(function ($) {
    /** Notifications */
    if (typeof window.notifications.toast === 'object') {
        window.notifications.toast.hideAfter = 6000;
    }

    $(document).ready(function () {
        window.jsInits.initMarkup();
        window.jsInits.initSelectAll();
    });
})(window.jQuery);

//----------------------------------------------------------------------------------------------------------------------

window.jsInits = {
    initMarkup: function () {
        retinajs();

        $('.auto-height').matchHeight();

        $(".custom-select").select2({
            minimumResultsForSearch: Infinity
        });
        $(".custom-select-template").select2({
            placeholder: "Select template",
            minimumResultsForSearch: Infinity
        });
        $(".custom-select-smpt").select2({
            placeholder: "Select SMTP Server",
            minimumResultsForSearch: Infinity
        });
        $(".custom-select-contact-list").select2({
            placeholder: "Select Contacts List",
            minimumResultsForSearch: Infinity
        });

        /** @todo need to fix it */
        var oldJqTrigger = jQuery.fn.trigger;
        jQuery.fn.trigger = function () {
            if (arguments && arguments.length > 0) {
                if (typeof arguments[0] == "object") {
                    if (typeof arguments[0].type == "string") {
                        if (arguments[0].type == "show.bs.modal") {
                            var ret = oldJqTrigger.apply(this, arguments);
                            if ($('.modal:visible').length) {
                                $('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
                                $(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
                            }
                            return ret;
                        }
                    }
                }
                else if (typeof arguments[0] == "string") {
                    if (arguments[0] == "hidden.bs.modal") {
                        if ($('.modal:visible').length) {
                            $('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
                            $('body').addClass('modal-open');
                        }
                    }
                }
            }
            return oldJqTrigger.apply(this, arguments);
        };

    },
    initSelectAll(){
        $(document).on('click', '[data-toggle-all-checkboxes]', function (e) {
            $('[data-select-item-checkbox]').prop('checked', this.checked).trigger('change');
            e.stopPropagation();
        });
    },
};

