export default {
    data() {
        return {
            state: {
                form: {
                    fields: [],
                    choices: {},
                }
            }
        }
    },
    computed: {},
    methods: {
        run(listId){
            let _this = this;
            let dataUrl = window.appConfig.routes['lists.ajax-form-load'];
            axios.post(dataUrl, {id: listId}).then(response => {
                if (response.data.success) {
                    _this.state.form = response.data.data.item;
                    _this.show();
                } else {
                    window.showActionResultMessages(response.data);
                }
            });
        },
        show() {
            $(this.$refs.modal).modal('show');
        },
        hide() {
            $(this.$refs.modal).modal('hide');
        },
        onSubmit(){
            let _this = this;
            let dataUrl = window.appConfig.routes['lists.ajax-form-post'];
            axios.post(dataUrl, {data: _this.state.form.fields}).then(response => {
                showActionResultMessages(response.data, 1);
                if (response.data.success) {
                    setTimeout(function () {
                        _this.hide();
                        window.showLoading();

                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }, 1000);
                }
            });
        },
    },
    mounted() {
        let _this = this;
        $(document).on('click', '[data-handle-edit-list]', function (e) {
            e.preventDefault();
            let id = $(this).data('id');
            _this.run(id);
        });
    }
}
