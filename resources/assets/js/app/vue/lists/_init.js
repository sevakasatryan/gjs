(function ($) {
    const selector = '#lists-page';

    $(document).ready(function () {
        if ($(selector).length) {
            new Vue({
                el: selector,
                components: {
                    'vue-edit-list-component': require('./modal-edit-list/component.vue'),
                    'vue-delete-button-component': require('./../common/delete-button/component.vue'),

                },
            });
        }
    });
})(jQuery);
