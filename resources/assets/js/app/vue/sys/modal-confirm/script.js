export default {
    props: {
        config: {
            type: Object,
            required: true
        }
    },
    data() {
        return {
            state: {
                callback: null,
                config: {
                    title: 'Warning!',
                    message: 'Are you sure?',
                    cancel: 'Discard',
                    submit: 'Submit',
                    appendTo: false,
                }
            }
        }
    },
    computed: {},
    methods: {
        onSubmit(){
            this.$emit('submit');

            if (this.state.callback) {
                this.state.callback();
            }

            this.hide();
        },
        show(callback) {
            let _this = this;

            if (typeof callback === 'function') {
                _this.state.callback = callback;
            } else {
                _this.state.callback = null;
            }

            $(_this.$refs.modal).modal('show');
        },
        hide() {
            $(this.$refs.modal).modal('hide');
        }
    },
    mounted(){
        let _this = this;

        _this.state.config = $.extend({}, _this.state.config, _this.config);

        if (_this.state.config.appendTo) {
            cons(_this.state.config.appendTo, 'AppendTo');
            
            _this.$nextTick(function () {
                $(_this.$refs.modal).appendTo(_this.state.config.appendTo);
            });
        }
    }
}
