export default {
    props: ['formData'],
    data() {
        return {
            state: {
                form: this.formData
            }
        }
    },
    computed: {},
    methods: {
        onCancel(){
            window.showLoading();
            window.location.href = window.appConfig.routes['root'];
        },
        onSubmit(){
            let _this = this;
            let dataUrl = window.appConfig.routes['settings.ajax-change-password'];
            axios.post(dataUrl, _this.state.form.fields).then(response => {
                showActionResultMessages(response.data, 1);
            });
        },
    },
    mounted() {
    }
}
