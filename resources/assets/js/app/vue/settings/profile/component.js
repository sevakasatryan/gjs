const loadAvatarComponent = require('./loadAvatarComponent.vue');

export default {
    components: {'load-avatar-component': loadAvatarComponent},
    props: ['formData'],
    data() {
        return {
            state: {
                form: this.formData,
                avatarImage: this.formData.choices.avatar,
                showRemove: this.formData.choices.has_avatar,
            }
        }
    },
    methods: {
        avatarLoaded(result){
            if (result.success) {
                this.state.form.fields.avatar_mode = 'set';
                this.state.avatarImage = result.data.thumb;
                this.state.showRemove = true;
            }
        },
        onCancel(){
            window.showLoading();
            window.location.href = window.appConfig.routes['root'];
        },
        onSubmit(){
            let _this = this;
            window.showLoading();
            let dataUrl = window.appConfig.routes['settings.ajax-profile-save'];
            axios.post(dataUrl, _this.state.form.fields).then(response => {
                window.hideLoading();
                showActionResultMessages(response.data, 1);
                if (response.data.success) {
                    _this.state.form = response.data.data.formData;
                }
            }).catch(e => window.hideLoading());
        },
        removeImage(){
            this.state.form.fields.avatar_mode = 'remove';
            this.state.avatarImage = this.state.form.choices.no_avatar;
            this.state.showRemove = false;
        },
    },
    mounted() {
        cons(this.formData, 'SSS');
    }
}
