(function ($) {
    const selector1 = '#vue-change-password-page';
    const selector2 = '#vue-profile-page';

    $(document).ready(function () {
        if ($(selector1).length) {
            new Vue({
                el: selector1,
                components: {
                    'vue-change-password-component': require('./password/component.vue')
                },
            });
        }

        if ($(selector2).length) {
            new Vue({
                el: selector2,
                components: {
                    'vue-profile-component': require('./profile/component.vue')
                },
            });
        }
    });
})(jQuery);
