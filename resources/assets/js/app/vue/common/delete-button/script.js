import confirmModal from './../../sys/modal-confirm/component';

export default {
    components: {'confirm-modal': confirmModal},
    props: ['ajaxDeleteUrl'],
    //------------------------------------------------------------------------------------------------------------------
    data() {
        return {
            state: {
                showButtons: false,
            }
        }
    },
    //------------------------------------------------------------------------------------------------------------------
    computed: {
        buttonClass(){
            return this.state.showButtons ? '' : 'n-a';
        },
        modalConfig(){
            return {
                'title': 'Warning',
                'message': 'Are you sure you want to delete selected items?',
                'cancel': 'Discard',
                'submit': 'Delete',
                'appendTo': 'body',
            }
        }
    },
    //------------------------------------------------------------------------------------------------------------------
    methods: {
        getSelectedIds(){
            let ids = [];
            $('[type=checkbox][data-id]:checked').each(function (i, item) {
                ids.push($(item).data('id'));
            });
            return ids;
        },
        deleteSelected(){
            if (this.getSelectedIds().length) {
                this.$refs.modal.show();
            }
        },
        onSubmit(){
            let _this = this;
            let dataUrl = _this.ajaxDeleteUrl;
            window.showLoading();
            axios.post(dataUrl, {ids: _this.getSelectedIds()}).then(response => {
                window.hideLoading();
                showActionResultMessages(response.data, 1);
                if (response.data.success) {
                    window.location.reload();
                }
            });
        }
    },
    //------------------------------------------------------------------------------------------------------------------
    mounted(){
        let _this = this;

        if (_this.ajaxDeleteUrl) {
            $(document).on('change', '[data-select-item-checkbox]', function () {
                let selected = _this.getSelectedIds();
                _this.state.showButtons = selected.length > 0;
            });
        } else {
            cons('ERROR AJAX URL');
        }
    }
    //------------------------------------------------------------------------------------------------------------------
}
