//------------------------------------------------------------------------------------------------------------------------------------------
window.jQuery = window.$ = require('jquery');
require('popper.js');
require('bootstrap');
require('../../html/app/js/select2.min');
require('../../html/app/js/jquery.matchHeight');
require('../../html/app/js/retina.min');
require('../../html/app/js/tempalte-scripts');
//------------------------------------------------------------------------------------------------------------------------------------------
import './../../../app_modules/AiApp/Resources/Assets/js/main';
import './app/main';
//------------------------------------------------------------------------------------------------------------------------------------------
// Axios, Vue:
window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
window.Vue = require('vue');
window.Vue.prototype.image_path = window.imagePath;
//------------------------------------------------------------------------------------------------------------------------------------------
// APP FILES:
import './app/vue/settings/_init';
import './app/vue/lists/_init';
//------------------------------------------------------------------------------------------------------------------------------------------