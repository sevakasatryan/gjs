@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <div id="lists-page">

        <section class="clearfix px-1 py-2 pl-3 grey-bg">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-7">
                    <span class="checkbox">
                        <label>
                            <input type="checkbox" name="" data-toggle-all-checkboxes="">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span><strong>All Lists</strong>
                        </label>
                    </span>
                    </div>
                    <div class="col text-right">
                        {{--<a href="#" readonly="true" class="btn danger" data-toggle="modal" data-target="#delete-contacts-modal">Delete Selected</a>--}}

                        <vue-delete-button-component ajax-delete-url="{{ route('lists.ajax-delete') }}"></vue-delete-button-component>

                        <a data-handle-edit-list data-id="" href="#" class="btn">Create List</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="clearfix px-1 pt-4 pb-0">
            <div class="container-fluid">

                <table class="table table-hover table-striped">
                    <thead>
                    <tr>
                        <th scope="col">List Name</th>
                        <th scope="col">Date Created</th>
                        <th scope="col">Emails</th>
                        <th scope="col">Opens</th>
                        <th scope="col">Clicks</th>
                        <th scope="col">Rating</th>
                        <th scope="col">Trigger</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($items as $item)
                        <tr>
                            <td>
                                <ul class="list-unstyled list-inline">
                                    <li class="list-inline-item mr-0">
                                    <span class="checkbox">
                                        <label>
                                            <input type="checkbox" data-select-item-checkbox="" data-id="{{$item->id}}" name="">
                                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                        </label>
                                    </span>
                                    </li>
                                    <li class="list-inline-item">
                                        <a data-handle-edit-list data-id="{{$item->id}}" href="#">
                                            <strong>{{$item->name}}</strong>
                                        </a>
                                    </li>
                                </ul>
                            </td>
                            <td>{{$item->created_at->format('M j, Y')}}</td>
                            <td><strong>{{$item->emails}}</strong></td>
                            <td><strong>{{$item->opens}}%</strong></td>
                            <td><strong>{{$item->clicks}}%</strong></td>
                            <td>
                                @include('application.lists.parts.rating', ['rating' => $item->rating])
                            </td>
                            <td>
                                {{--<a href="#">What's This?</a>--}}
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>

                {{ $items->links() }}

            </div>
        </section>

        <vue-edit-list-component></vue-edit-list-component>

    </div>

@endsection
