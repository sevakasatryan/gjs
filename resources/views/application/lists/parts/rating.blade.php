<ul class="stars-rating list-unstyled" title="rating: {{$rating}}">
    @for($j=1; $j<=5;$j++)
        <li class="{{$rating >= $j ? 'active' : ''}}">
            <i class="fa fa-star" aria-hidden="true"></i>
        </li>
    @endfor
</ul>
