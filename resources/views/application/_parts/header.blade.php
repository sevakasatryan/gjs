<?php $routeName = Route::currentRouteName() ?>

<header class="clearfix px-1">
    <div class="container-fluid">
        <div class="row align-items-center h-100">
            <div class="col-9">
                <div class="logo h-100 d-inline-block mr-5">
                    <a href="{{route('root')}}" class="align-middle">
                        <img src="{{ asset('images/gojeo-send-logo.png') }}" data-rjs="2">
                    </a>
                </div>
                <ul class="list-unstyled d-inline-block menu">
                    <li class="d-inline-block">
                        <a class="{{preg_match('|^dashboard|', $routeName) ? 'active' : ''}}"
                           href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="d-inline-block">
                        <a class="{{preg_match('|^lists|', $routeName) ? 'active' : ''}}"
                           href="{{ route('lists') }}">Lists & Contacts</a>
                    </li>
                    <li class="d-inline-block">
                        <a class="{{preg_match('|^templates|', $routeName) ? 'active' : ''}}"
                           href="{{ route('templates') }}">Email Templates</a>
                    </li>
                    <li class="d-inline-block">
                        <a class="{{preg_match('|^servers|', $routeName) ? 'active' : ''}}"
                           href="{{ route('servers') }}">SMTP Servers</a>
                    </li>
                </ul>
            </div>

            <div class="col text-right">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hello, {{$user->first_name}} <i class="fa fa-caret-down" aria-hidden="true"></i>
                        <span class="avatar" style="background-image: url({{$user->thumb}});"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('settings.profile') }}">Profile Settings</a>
                        <a class="dropdown-item" href="{{ route('settings.password') }}">Change Password</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="w-100 h50 d-block clearfix"></div>