<div>
    <div id="spin" style="position: absolute;"></div>
</div>

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <script> showNotification('{{ $error }}', 'error'); </script>
    @endforeach
@endif