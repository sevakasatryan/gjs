@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <section class="clearfix px-1 pt-4 pb-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <figure class="single-stats d-flex align-items-center">
                        <div class="icon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 25.002 30.003" enable-background="new 0 0 25.002 30.003" xml:space="preserve">
									<g>
                                        <g>
                                            <path fill="#FFFFFF" d="M12.505,13.913c3.809,0,6.897-3.114,6.897-6.957C19.402,3.115,16.314,0,12.505,0
												C8.696,0,5.608,3.115,5.608,6.956C5.608,10.799,8.696,13.913,12.505,13.913z M12.505,17.391C5.601,17.391,0.004,23.036,0.004,30
												h25.003C25.006,23.036,19.409,17.391,12.505,17.391z"/>
                                        </g>
                                    </g>
								</svg>
                        </div>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><strong>5</strong> Lists</li>
                            <li class="list-inline-item"><strong>3,324</strong> Contacts</li>
                        </ul>
                    </figure>
                </div>

                <div class="col">
                    <figure class="single-stats d-flex align-items-center">
                        <div class="icon violet"><i class="fa fa-file-text" aria-hidden="true"></i></div>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><strong>5</strong> Projects</li>
                            <li class="list-inline-item"><strong>21</strong> Email Templates</li>
                        </ul>
                    </figure>
                </div>

                <div class="col">
                    <figure class="single-stats d-flex align-items-center">
                        <div class="icon orange"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item"><strong>15</strong> Emails Sent</li>
                        </ul>
                    </figure>
                </div>
            </div>
            <p class="mt-4"><strong>Activity Stream</strong></p>
            <table class="table table-hover table-striped">
                <thead>
                <tbody>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon orange"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Email sent: <a href="#"><strong>Welcome to Affinity</strong></a> to <a href="#"><strong>Affinity Designers (355)</strong></a> contact list using the <a href="#"><strong>mail@gojeo.com</strong></a> SMTP server.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon violet"><i class="fa fa-file-text" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Template created <a href="#"><strong>Did you like Affinity?</strong></a></td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon">
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 25.002 30.003" enable-background="new 0 0 25.002 30.003" xml:space="preserve">
											<g>
												<g>
													<path fill="#FFFFFF" d="M12.505,13.913c3.809,0,6.897-3.114,6.897-6.957C19.402,3.115,16.314,0,12.505,0
														C8.696,0,5.608,3.115,5.608,6.956C5.608,10.799,8.696,13.913,12.505,13.913z M12.505,17.391C5.601,17.391,0.004,23.036,0.004,30
														h25.003C25.006,23.036,19.409,17.391,12.505,17.391z"/>
												</g>
											</g>
										</svg>
									</span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Added <strong>458</strong> contacts to the <a href="#"><strong>Affinity Designers (355) list</strong>.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon orange"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Email sent: <a href="#"><strong>Welcome to Affinity</strong></a> to <a href="#"><strong>Affinity Designers (355)</strong></a> contact list using the <a href="#"><strong>mail@gojeo.com</strong></a> SMTP server.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon violet"><i class="fa fa-file-text" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Template created <a href="#"><strong>Did you like Affinity?</strong></a></td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon">
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 25.002 30.003" enable-background="new 0 0 25.002 30.003" xml:space="preserve">
											<g>
												<g>
													<path fill="#FFFFFF" d="M12.505,13.913c3.809,0,6.897-3.114,6.897-6.957C19.402,3.115,16.314,0,12.505,0
														C8.696,0,5.608,3.115,5.608,6.956C5.608,10.799,8.696,13.913,12.505,13.913z M12.505,17.391C5.601,17.391,0.004,23.036,0.004,30
														h25.003C25.006,23.036,19.409,17.391,12.505,17.391z"/>
												</g>
											</g>
										</svg>
									</span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Added <strong>458</strong> contacts to the <a href="#"><strong>Affinity Designers (355) list</strong>.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon orange"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Email sent: <a href="#"><strong>Welcome to Affinity</strong></a> to <a href="#"><strong>Affinity Designers (355)</strong></a> contact list using the <a href="#"><strong>mail@gojeo.com</strong></a> SMTP server.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon violet"><i class="fa fa-file-text" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Template created <a href="#"><strong>Did you like Affinity?</strong></a></td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon">
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 25.002 30.003" enable-background="new 0 0 25.002 30.003" xml:space="preserve">
											<g>
												<g>
													<path fill="#FFFFFF" d="M12.505,13.913c3.809,0,6.897-3.114,6.897-6.957C19.402,3.115,16.314,0,12.505,0
														C8.696,0,5.608,3.115,5.608,6.956C5.608,10.799,8.696,13.913,12.505,13.913z M12.505,17.391C5.601,17.391,0.004,23.036,0.004,30
														h25.003C25.006,23.036,19.409,17.391,12.505,17.391z"/>
												</g>
											</g>
										</svg>
									</span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Added <strong>458</strong> contacts to the <a href="#"><strong>Affinity Designers (355) list</strong>.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon orange"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Email sent: <a href="#"><strong>Welcome to Affinity</strong></a> to <a href="#"><strong>Affinity Designers (355)</strong></a> contact list using the <a href="#"><strong>mail@gojeo.com</strong></a> SMTP server.</td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon violet"><i class="fa fa-file-text" aria-hidden="true"></i></span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Template created <a href="#"><strong>Did you like Affinity?</strong></a></td>
                </tr>
                <tr>
                    <td>
				      			<span class="box-widt-icon">
				      				<span class="icon">
										<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 25.002 30.003" enable-background="new 0 0 25.002 30.003" xml:space="preserve">
											<g>
												<g>
													<path fill="#FFFFFF" d="M12.505,13.913c3.809,0,6.897-3.114,6.897-6.957C19.402,3.115,16.314,0,12.505,0
														C8.696,0,5.608,3.115,5.608,6.956C5.608,10.799,8.696,13.913,12.505,13.913z M12.505,17.391C5.601,17.391,0.004,23.036,0.004,30
														h25.003C25.006,23.036,19.409,17.391,12.505,17.391z"/>
												</g>
											</g>
										</svg>
									</span>
				      				<strong>Mar 27 2018</strong>, 23:15
				      			</span>
                    </td>
                    <td>Added <strong>458</strong> contacts to the <a href="#"><strong>Affinity Designers (355) list</strong>.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
@endsection
