@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <section class="clearfix px-1 py-2 pl-3 grey-bg">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-7">
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
								<span class="checkbox">
									<label>
										<input type="checkbox" name="" checked>
										<span class="cr"><i class="cr-icon fa fa-check"></i></span><strong>All Email Templates</strong>
									</label>
								</span>
                        </li>
                    </ul>

                </div>
                <div class="col text-right">
                    <a href="#" class="btn blue" data-toggle="modal" data-target="#create-project-modal">Create Project</a>
                </div>
            </div>
        </div>
    </section>

    <section class="clearfix px-1 pt-4 pb-0">
        <div class="container-fluid">


            <table class="table table-hover table-striped">
                <thead>
                <thead>
                <tr>
                    <th scope="col">List Name</th>
                    <th scope="col">Date Created</th>
                    <th scope="col">Templates</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#"><strong>Mono Name</strong></a></li>
                        </ul>
                    </td>
                    <td>Mar 27, 2018</td>
                    <td><strong>2</strong></td>
                </tr>

                </tbody>
            </table>
        </div>
    </section>

@endsection
