@extends('layouts.app')

@push('head_assets')
<link rel="stylesheet" href="{{ mix('/build/app.css') }}">
<script src="{{ mix('/build/vendor.js') }}"></script>
<script src="{{ mix('/build/app.js') }}"></script>
@endpush

@section('body_content')
    @include('application._parts.common')
    @yield('content')
@endsection