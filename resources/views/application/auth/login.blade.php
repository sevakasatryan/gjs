@extends('application.layouts.layout_app')

@section('content')

    <div class="container">

        <form class="form-signin" id="login" method="post" action="{{ route('login') }}">

            {{ csrf_field() }}

            <img src="/images/login-logo.png" alt="">

            <div class="form-group">
                <label for="inputEmail">Email
                <input name="email" type="email" id="inputEmail" placeholder="Email address"
                       class="form-control @if($errors->has('email'))is-invalid @endif"
                       value="{{ old('email') }}" required autofocus>
                </label>

                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form-group">
                <label for="inputPassword">Password
                <input name="password" type="password" id="inputPassword"
                       class="form-control @if($errors->has('password'))is-invalid @endif"
                       placeholder="Password" required>
                </label>

                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>

            {{--<div class="checkbox">--}}
                {{--<label>--}}
                    {{--<input type="checkbox" value="remember-me"> Remember me--}}
                {{--</label>--}}
            {{--</div>--}}
            <button class="btn btn-lg btn-primary btn-block" type="submit">log in</button>
        </form>

    </div> <!-- /container -->

    @if ($errors->has('csrf'))
        <script> showNotification('{{ $errors->first('csrf') }}', 'error'); </script>
    @endif
@endsection