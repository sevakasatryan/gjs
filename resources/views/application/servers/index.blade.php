@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <section class="clearfix px-1 py-2 pl-3 grey-bg">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-7">
						<span class="checkbox">
							<label>
								<input type="checkbox" name="" checked>
								<span class="cr"><i class="cr-icon fa fa-check"></i></span><strong>All SMTP Servers</strong>
							</label>
						</span>
                </div>
                <div class="col text-right">
                    <a href="#" class="btn" data-toggle="modal" data-target="#add-server-modal">Add Server</a>
                </div>
            </div>
        </div>
    </section>
    <section class="clearfix px-1 pt-4 pb-0">
        <div class="container-fluid">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    {!! \Session::get('success') !!}
                </div>
            @endif

            <table class="table table-hover table-striped">
                <thead>
                <thead>
                <tr>
                    <th scope="col">Username</th>
                    <th scope="col">Date Added</th>
                    <th scope="col">Host</th>
                    <th scope="col">Port</th>
                    <th scope="col">From Name</th>
                    <th scope="col">From Email</th>
                    <th scope="col">Reply-to Email</th>
                </tr>
                </thead>
                <tbody>
                @foreach($servers as $server)
                <tr>
                    <td>
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item mr-0">
				      					<span class="checkbox">
											<label>
												<input type="checkbox" name="" checked>
												<span class="cr"><i class="cr-icon fa fa-check"></i></span>
											</label>
										</span>
                            </li>
                            <li class="list-inline-item"><a href="#">{{$server['username']}}</a></li>
                        </ul>
                    </td>
                    <td>{{date('M d, Y',strtotime($server['created_at']))}}</td>
                    <td>{{$server['host']}}</td>
                    <td>{{$server['port']}}</td>
                    <td>{{$server['from_name']}}</td>
                    <td>{{$server['from_email']}}</td>
                    <td>{{$server['reply_to_email']}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="add-server-modal" tabindex="-1" role="dialog" aria-labelledby="add-server-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="add-server-modalLabel">Add Server</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="grey-box form-group">
                        <div class="row row-small">
                            <div class="col col-small"><a href="#" class="btn btn-block btn-lg green-light">How to add AWS SES <i class="fa fa-external-link-square" aria-hidden="true"></i></a></div>
                            <div class="col col-small"><a href="#" class="btn btn-block btn-lg green-light">How to add SendGrid <i class="fa fa-external-link-square" aria-hidden="true"></i></a></div>
                        </div>
                    </div>
                    <form method="post" action="/add/server" >
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Host</label>
                                    <input type="text" name="host" class="form-control" placeholder="Host">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Port</label>
                                    <input type="number" name="port" class="form-control" placeholder="Port">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control" placeholder="Username">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group mb-5">
                                    <label>Password</label>
                                    <input type="text" name="password" class="form-control" placeholder="Password">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label>From Name</label>
                                    <input type="text" name="from_name" class="form-control" placeholder="From Name">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label>From Email</label>
                                    <input type="email" name="from_email" class="form-control" placeholder="From Email">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group mb-5">
                                    <label>Reply-to Email</label>
                                    <input type="email" name="reply_to_email" class="form-control" placeholder="Reply-to Email">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col"><a href="#" class="btn btn-lg w110 discard" data-dismiss="modal">Discard</a></div>
                            <div class="col text-right"><button class="btn btn-lg w110 green">Save</button></div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
