@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <div id="vue-profile-page">
        <vue-profile-component :form-data="{{ json_encode($formData) }}"></vue-profile-component>

    </div>

@endsection
