@extends('application.layouts.layout_app')

@section('content')

    @include('application._parts.header')

    <div id="vue-change-password-page">
        <vue-change-password-component :form-data="{{ json_encode($formData) }}"></vue-change-password-component>
    </div>

@endsection
