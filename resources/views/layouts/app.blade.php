<!DOCTYPE html>
<html dir="ltr" lang="en" @yield('html-attributes')>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google" content="notranslate">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{url('/favicon.ico')}}" type="image/x-icon">
    <title>{{config('app.name')}} @yield('title')</title>
    <script> window.appConfig = {!! ViewHelper::getJsConfig() !!}; </script>
    @stack('head_assets')
    @stack('head_addon')
</head>

<body class="@stack('body_class')" @stack('body_attributes')>

@yield('body_content')

@stack('bottom_assets')
@stack('bottom_addon')
</body>
</html>
