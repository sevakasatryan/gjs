<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/test', 'Application\UserController@test');

Route::post('/login', 'Application\UserController@attempLogin');

Route::group(['namespace' => 'Application', 'middleware' => ['apiAuth']], function () {
    Route::post('/get_user', 'UserController@getUser');
    Route::post('/save_user_settings', 'UserController@saveUserSettings');
    Route::post('/chenge_password', 'UserController@chengePassword');

    Route::post('/lists', 'ListsController@getIndex');
    Route::post('/list/create', 'ListsController@createList');
    Route::post('/list/edit', 'ListsController@editList');
    Route::post('/list/delete', 'ListsController@deleteList');
    Route::post('/list/contacts', 'ListsController@getContacts');

    Route::post('/contact/add', 'ListsController@addContact');
    Route::post('/contact/delete', 'ListsController@deleteContact');
    Route::post('/contact/get', 'ListsController@getContact');
    Route::post('/contact/edit', 'ListsController@editContact');

    Route::post('/smtp/add', 'SmtpController@addSmtp');
    Route::post('/smtp/get', 'SmtpController@getSmtp');
    Route::post('/smtp/sendTest', 'SmtpController@sendTest');

    Route::post('/projects/get', 'TemplatesController@getProjects');
    Route::post('/projects/add', 'TemplatesController@addProjects');
    Route::post('/project/get', 'TemplatesController@getProject');

    Route::post('/templates/get', 'TemplatesController@getTemplates');
    Route::post('/template/get', 'TemplatesController@getTemplate');
    Route::post('/template/add', 'TemplatesController@addTemplate');
    Route::post('/template/edit', 'TemplatesController@editTemplate');
    Route::post('/template/delete', 'TemplatesController@deleteTemplate');

    Route::post('/dashboard/get', 'DashboardController@getDashboard');



//
//    Route::post('/lists/ajax-form-post', 'ListsController@ajaxFormPost');
//    Route::post('/lists/ajax-delete', 'ListsController@ajaxDelete');
//    Route::post('/add/server', 'ServersController@addServer');
//    Route::get('/', ['as' => 'root', 'uses' => 'HomeController@getIndex']);
//    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@getIndex']);
//    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@getIndex']);
//    Route::get('/templates', ['as' => 'templates', 'uses' => 'TemplatesController@getIndex']);
//    Route::get('/servers', ['as' => 'servers', 'uses' => 'ServersController@getIndex']);
//    Route::get('/settings/profile', ['as' => 'settings.profile', 'uses' => 'SettingsController@getProfile']);
//    Route::get('/settings/password', ['as' => 'settings.password', 'uses' => 'SettingsController@getPassword']);
//    Route::post('/settings/ajax-change-password', ['as' => 'settings.ajax-change-password', 'uses' => 'SettingsController@ajaxChangePassword']);
//    Route::post('/settings/ajax-avatar', ['as' => 'settings.ajax-avatar', 'uses' => 'SettingsController@ajaxAvatar']);
//    Route::post('/settings/ajax-profile-save', ['as' => 'settings.ajax-profile-save', 'uses' => 'SettingsController@ajaxProfileSave']);
});


///*
//|--------------------------------------------------------------------------
//| Web Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register web routes for your application. These
//| routes are loaded by the RouteServiceProvider within a group which
//| contains the "web" middleware group. Now create something great!
//|
//*/
//
//// [APP]
//Auth::routes();
//Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
//
//Route::group(['namespace' => 'Application', 'middleware' => ['auth:web']], function () {
//    Route::get('/', ['as' => 'root', 'uses' => 'HomeController@getIndex']);
//    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@getIndex']);
//    Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@getIndex']);
//
//    Route::get('/lists', ['as' => 'lists', 'uses' => 'ListsController@getIndex']);
//    Route::post('/lists/ajax-form-load', ['as' => 'lists.ajax-form-load', 'uses' => 'ListsController@ajaxFormLoad']);
//    Route::post('/lists/ajax-form-post', ['as' => 'lists.ajax-form-post', 'uses' => 'ListsController@ajaxFormPost']);
//    Route::post('/lists/ajax-delete', ['as' => 'lists.ajax-delete', 'uses' => 'ListsController@ajaxDelete']);
//    Route::post('/add/server', 'ServersController@addServer');
//
//    Route::get('/templates', ['as' => 'templates', 'uses' => 'TemplatesController@getIndex']);
//    Route::get('/servers', ['as' => 'servers', 'uses' => 'ServersController@getIndex']);
//
//    Route::get('/settings/profile', ['as' => 'settings.profile', 'uses' => 'SettingsController@getProfile']);
//    Route::get('/settings/password', ['as' => 'settings.password', 'uses' => 'SettingsController@getPassword']);
//    Route::post('/settings/ajax-change-password', ['as' => 'settings.ajax-change-password', 'uses' => 'SettingsController@ajaxChangePassword']);
//    Route::post('/settings/ajax-avatar', ['as' => 'settings.ajax-avatar', 'uses' => 'SettingsController@ajaxAvatar']);
//    Route::post('/settings/ajax-profile-save', ['as' => 'settings.ajax-profile-save', 'uses' => 'SettingsController@ajaxProfileSave']);
//});
