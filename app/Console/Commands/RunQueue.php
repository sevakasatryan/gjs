<?php namespace App\Console\Commands;

use Modules\AiApp\Base\BaseCommands\BaseRunQueueCommand;

class RunQueue extends BaseRunQueueCommand
{
    protected function getQueueConfig()
    {
        $high = [
            'queues'  => ['high', 'medium'],
            'timeout' => 60,
            'memory'  => 200,
            'tries'   => 5,
        ];

        $default = [
            'queues'  => ['low', 'emails', 'default'],
            'timeout' => 60,
            'memory'  => 100,
            'tries'   => 10,
        ];

        if (is_local()) {
            $conf = [$high, $default];
        } else {
            $conf = [$high, $high, $default];
        }

        return $conf;
    }

}
