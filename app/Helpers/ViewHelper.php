<?php namespace App\Helpers;

use Illuminate\Support\Collection;
use Auth, Route;

class ViewHelper
{
    public static function getJsConfig()
    {
        if (Auth::check()) {
            $routes = Collection::make();
            $routeCollection = Route::getRoutes();
            foreach ($routeCollection as $item) {
                if (empty($item->parameterNames()) && $item->getName()) {
                    $routes->push($item->getName());
                }
            };
            $config = [
                'js_debug'         => config('app.debug') ? true : false,
                'max_uploads_size' => config('settings.max_uploads_size'),
                'token'            => csrf_token(),
                'build'            => config('settings.build'),
                'routes'           => $routes->mapWithKeys(function ($routeName) {
                    return [$routeName => route($routeName)];
                }),
                'images_path'       => asset('/images/'),
            ];
        } else {
            $config = [];
        }

        return json_encode($config);
    }
}
