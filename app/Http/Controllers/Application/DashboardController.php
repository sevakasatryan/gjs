<?php namespace App\Http\Controllers\Application;


use App\contact;
use App\lists;
use App\Project;
use App\Template;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;

class DashboardController extends BaseAdminController
{
    private $current_user = '';

    public function __construct(Request $request)
    {
        if (isset($request->token)){
            $this->current_user = User::where('email_api_token', $request->token)->first();
        }

        if(!$this->current_user){
            return json_encode(['success' => false, 'message' => 'Not user']);
        }
    }

    public function getDashboard(Request $request)
    {


        $lists_count = 0;
        $contacts_count = 0;
        $projects_count = 0;
        $templates_count = 0;
        $sentEmails_count = 0;
        $actifity = [];

        //Get lists
        $list_ids = Lists::where('user_id', $this->current_user->id)->select(['id'])->get()->toArray();
        $lists_count = count($list_ids);
        $tmp_list_ids = [];
        foreach ($list_ids as $list_id){
            array_push($tmp_list_ids, $list_id['id']);
        }

        //Get contacts
        if($lists_count){
            $contacts_count = Contact::whereIn('list_id', $tmp_list_ids)->count();
        }


        //Get Projects
        $projects = Project::select('id')->where('user_id', $this->current_user->id)->get();

        $projects_count = count($projects);
        $tmp_project_ids = [];
        foreach ($projects as $project){
            array_push($tmp_project_ids, $project->id);
        }

        //Get templates
        if($projects_count){
            $templates_count = Template::whereIn('project_id', $tmp_project_ids)->count();
        }

        $actifity['templates'] = Template::whereIn('project_id', $tmp_project_ids)->limit(10)->get();



        return json_encode([
            'success' => true,
            'lists' => $lists_count,
            'contacts' => $contacts_count,
            'projects' => $projects_count,
            'templates' => $templates_count,
            'emails' => $sentEmails_count,
            'activity' => $actifity
        ]);

    }
}