<?php namespace App\Http\Controllers\Application;

use Modules\AiApp\Base\BaseControllers\BaseAdminController;

class HomeController extends BaseAdminController
{
    public function getIndex()
    {
        return redirect(route('dashboard'));
    }
}