<?php namespace App\Http\Controllers\Application;

use App\Http\Requests\CommonRequest;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;
use App\Area\User\Forms\ProfileUserForm;
use App\Area\User\Forms\ProfilePasswordForm;
use App\Http\Requests\AjaxRequest;
use App\Area\User\Services\UserService;
use Auth;

class SettingsController extends BaseAdminController
{
    public function getProfile()
    {
        $form = new ProfileUserForm($this->user);

        return view('application.settings.profile', ['formData' => $form->getData()]);
    }

    public function getPassword()
    {
        $form = new ProfilePasswordForm($this->user);

        return view('application.settings.password', ['formData' => $form->getData()]);
    }

    public function ajaxChangePassword(AjaxRequest $request, UserService $userService)
    {
        $data = $request->all();
        $result = $userService->changePassword($this->user, $data);

        return response()->json($result->toArray());
    }

    public function ajaxAvatar(CommonRequest $request, UserService $userService)
    {
        $file = $request->file('file');
        $result = $userService->uploadTempAvatar($this->user, $file);

        return response()->json($result->toArray());
    }

    public function ajaxProfileSave(AjaxRequest $request, UserService $userService)
    {
        $data = $request->all();
        $result = $userService->editProfile($this->user, $data);

        if ($result->success) {
            Auth::setUser($result->getData('item'));
            $form = new ProfileUserForm($result->getData('item'));
            $result->setData('formData', $form->getData());
        }

        return response()->json($result->toArray());
    }

}