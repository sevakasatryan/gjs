<?php

namespace App\Http\Controllers\Application;

use App\SmtpConnection;
use App\Template;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SmtpController extends BaseAdminController
{
    private $current_user = '';

    public function __construct(Request $request)
    {
        if (isset($request->token)){
            $this->current_user = User::where('email_api_token', $request->token)->first();
        }

        if(!$this->current_user){
            return json_encode(['success' => false, 'message' => 'Not user']);
        }
    }

    public function addSmtp(Request $request)
    {
        ini_set('max_execution_time', 300);
        $smtp_host = $request->smtp_host;
        $smtp_port = $request->smtp_port;
        $smtp_username = $request->smtp_username;
        $smtp_password = $request->smtp_password;
        $from_name = $request->from_name;
        $from_email = $request->from_email;
        $reply_to_email = $request->reply_to_email;

        if(!$smtp_host){
            return json_encode(['success' => false, 'message' => 'SMTP Host is required']);
        }
        if(!$smtp_port){
            return json_encode(['success' => false, 'message' => 'SMTP Port is required']);
        }
        if(!$smtp_username){
            return json_encode(['success' => false, 'message' => 'Username is required']);
        }
        if(!$smtp_password){
            return json_encode(['success' => false, 'message' => 'Password is required']);
        }
        if(!$from_name){
            return json_encode(['success' => false, 'message' => 'From Name is required']);
        }
        if(!$from_email){
            return json_encode(['success' => false, 'message' => 'From email is required']);
        }

        if($reply_to_email){
            if (!filter_var($reply_to_email, FILTER_VALIDATE_EMAIL)) {
                return json_encode(['success' => false, 'message' => 'Incorrect Replay To Email Address']);
            }
        }

        if (!filter_var($from_email, FILTER_VALIDATE_EMAIL)) {
            return json_encode(['success' => false, 'message' => 'Incorrect From Email Address']);
        }

        $mail = new PHPMailer(true); // notice the \  you have to use root namespace here

        try {
            $mail->isSMTP();
            $mail->CharSet = "utf-8";
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "tls";
            $mail->Host = $smtp_host;
            $mail->Port = $smtp_port;
            $mail->Username = $smtp_username;
            $mail->Password = $smtp_password;
            $mail->setFrom($from_email, $from_name);
            $mail->Subject = "SMTP Test";
            $mail->MsgHTML("SMTP Test");
            $mail->addAddress("test@gjs.com", "gjs");
            $mail->send();
        } catch (phpmailerException $e) {
            return json_encode(['success' => false, 'message' => 'The system failed to connect with your SMTP credentials. Please double check and try again.']);
        } catch (Exception $e) {
            return json_encode(['success' => false, 'message' => 'The system failed to connect with your SMTP credentials. Please double check and try again.']);
        }

        $smtpConnection = new SmtpConnection();
        $smtpConnection->user_id = $this->current_user->id;
        $smtpConnection->host = $smtp_host;
        $smtpConnection->port = $smtp_port;
        $smtpConnection->username = $smtp_username;
        $smtpConnection->password = $smtp_password;
        $smtpConnection->from_name = $from_name;
        $smtpConnection->from_email = $from_email;
        $smtpConnection->reply_to_email = $reply_to_email;
        $smtpConnection->save();

        return json_encode(['success' => true, 'message' => 'New SMTP Connection successfully added']);
    }

    public function getSmtp(Request $request)
    {
        $smtp_servers = SmtpConnection::where('user_id', $this->current_user->id)->get();
        return json_encode(['success' => true, 'smtp_servers' => $smtp_servers]);
    }

    public function sendTest(Request $request)
    {
        $smtp_id = $request->smtp_id;
        $send_to_email = $request->send_to_email;
        $template_id = $request->template_id;
        $smtp_connection = SmtpConnection::where('user_id', $this->current_user->id)->where('id', $smtp_id)->first();

        if (!filter_var($send_to_email, FILTER_VALIDATE_EMAIL)) {
            return json_encode(['success' => false, 'message' => 'Incorrect New Email Address']);
        }

        if(!$smtp_connection){
            return json_encode(['success' => true, 'message' => 'Incorrect SMTP server id']);
        }

        if(!$template_id) {
            return json_encode(['success' => false, 'message' => 'Template Id Is Required']);
        }


        $template = Template::where('id', $template_id)->first();
        if(!$template) {
            return json_encode(['success' => false, 'message' => 'Incorrect Template Id']);
        }

        \Config::set('mail.host', $smtp_connection->host);
        \Config::set('mail.port', $smtp_connection->port);
        \Config::set('mail.username', $smtp_connection->username);
        \Config::set('mail.password', $smtp_connection->password);
        \Config::set('mail.from.address', $smtp_connection->from_email);
        \Config::set('mail.from.name', $smtp_connection->from_name);

        $transport = Mail::getSwiftMailer()->getTransport();
        Mail::alwaysFrom(\Config::get('mail.from.address'), \Config::get('mail.from.name'));
        $transport->setHost(\Config::get('mail.host'));
        $transport->setPort(\Config::get('mail.port'));
        $transport->setUsername(\Config::get('mail.username'));
        $transport->setPassword(\Config::get('mail.password'));

        $transport->setPort(\Config::get('mail.port'));
        $transport->setUsername('argisht.amiryan@gmail.com');
        $transport->setPassword('anvcrhdbgoaucgpk');

        $data = ['template' => $template->body];

        Mail::send('mailTemplate', $data, function ($message) use ($send_to_email, $smtp_connection, $template){
            $message->to($send_to_email)->subject($template->subject);
            if($smtp_connection->reply_to_email){
                $message->replyTo($smtp_connection->reply_to_email);
            }
        });

        if (Mail::failures()) {
            return json_encode(['success' => false, 'message' => Mail::failures()]);
        }else{
            return json_encode(['success' => true, 'message' => 'Test message successfully sent']);
        }
    }
}
