<?php namespace App\Http\Controllers\Application;

use App\Project;
use App\Template;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;

class TemplatesController extends BaseAdminController
{

    public function __construct(Request $request)
    {
        if (isset($request->token)){
            $this->current_user = User::where('email_api_token', $request->token)->first();
        }

        if(!$this->current_user){
            return json_encode(['success' => false, 'message' => 'Not user']);
        }
    }

    public function getProjects(Request $request)
    {
        $projects = Project::where('user_id', $this->current_user->id)->get();
        return json_encode(['success' => true, 'projects' => $projects]);
    }

    public function getProject(Request $request)
    {
        $project = Project::where('id', $request->project_id)->where('user_id', $this->current_user->id)->first();
        return json_encode(['success' => true, 'project' => $project]);
    }

    public function addProjects(Request $request)
    {
        $projectName = $request->name;
        if($projectName){
            if(!Project::where('user_id', $this->current_user->id)->where('name', $projectName)->count()){
                $project = new Project();
                $project->name = $projectName;
                $project->user_id = $this->current_user->id;
                $project->save();
                return json_encode(['success' => true, 'message' => 'Project Successfully Added']);
            }else{
                return json_encode(['success' => false, 'message' => 'Project Name already exists']);
            }

        }else{
            return json_encode(['success' => false, 'message' => 'Project Name Is Required']);
        }
    }

    public function getTemplates(Request $request)
    {
        $project_id = $request->project_id;
        if($project_id){
            if(Project::where('user_id', $this->current_user->id)->where('id', $project_id)->count()){
                $templates = Template::with('project')->with('follow_template')->where('project_id', $project_id)->get();
                return json_encode(['success' => true, 'templates' => $templates]);
            }else{
                return json_encode(['success' => false, 'message' => 'Incorrect Project Id']);
            }
        }else{
            return json_encode(['success' => false, 'message' => 'Project Id Is Required']);
        }
    }

    public function getTemplate(Request $request)
    {
        $template_id = $request->template_id;
        if($template_id){
            $template = Template::with('project')->with('follow_template')->where('id', $template_id)->first();
            if($template){
                if(Project::where('user_id', $this->current_user->id)->where('id', $template->project_id)->count()){
                    return json_encode(['success' => true, 'template' => $template]);
                }else{
                    return json_encode(['success' => false, 'message' => 'Incorrect Template Id']);
                }
            }else{
                return json_encode(['success' => false, 'message' => 'Incorrect Template Id']);
            }

        }else{
            return json_encode(['success' => false, 'message' => 'Template Id Is Required']);
        }
    }

    public function addTemplate(Request $request)
    {
        $project_id = $request->project_id;
        $template_name = $request->template_name;
        $template_subject = $request->template_subject;
        $template_body = $request->template_body;
        $follow_template_id = $request->follow_template_id;
        $after_day = $request->after_day;
        $after_hours = $request->after_hours;

        if(!$project_id){
            return json_encode(['success' => false, 'message' => 'Project Id Is Required']);
        }
        if(!$template_name){
            return json_encode(['success' => false, 'message' => 'Template Name Is Required']);
        }
        if(!$template_subject){
            return json_encode(['success' => false, 'message' => 'Template Subject Is Required']);
        }
        if(!$template_body){
            return json_encode(['success' => false, 'message' => 'Template Body Is Required']);
        }

        $project = Project::where('id', $project_id)->where('user_id', $this->current_user->id)->first();

        if(!$project){
            return json_encode(['success' => false, 'message' => 'Incorrect Project Id']);
        }

        $template = new Template();

        $template->name        = $template_name;
        $template->subject     = $template_subject;
        $template->body        = $template_body;
        $template->project_id  = $project_id;

        if($follow_template_id && ($after_day ||$after_hours)){
            $template->follow_template_id  = $follow_template_id;
            $template->after_day           = $after_day;
            $template->after_hours         = $after_hours;
        }

        $template->save();
        return json_encode(['success' => true, 'message' => 'Template Successfully Added']);
    }


    public function editTemplate(Request $request)
    {
        $project_id = $request->project_id;
        $template_id = $request->template_id;
        $template_name = $request->template_name;
        $template_subject = $request->template_subject;
        $template_body = $request->template_body;
        $follow_template_id = $request->follow_template_id;
        $after_day = $request->after_day;
        $after_hours = $request->after_hours;

        if(!$project_id){
            return json_encode(['success' => false, 'message' => 'Project Id Is Required']);
        }
        if(!$template_name){
            return json_encode(['success' => false, 'message' => 'Template Name Is Required']);
        }
        if(!$template_subject){
            return json_encode(['success' => false, 'message' => 'Template Subject Is Required']);
        }
        if(!$template_body){
            return json_encode(['success' => false, 'message' => 'Template Body Is Required']);
        }

        $project = Project::where('id', $project_id)->where('user_id', $this->current_user->id)->first();

        if(!$project){
            return json_encode(['success' => false, 'message' => 'Incorrect Project Id']);
        }

        $template = Template::where('id', $template_id)->first();

        if(!$template){
            return json_encode(['success' => false, 'message' => 'Incorrect Template Id']);
        }

        $template->name                 = $template_name;
        $template->subject              = $template_subject;
        $template->body                 = $template_body;
        $template->follow_template_id   = $follow_template_id;
        $template->after_day            = $after_day;
        $template->after_hours          = $after_hours;


        $template->save();
        return json_encode(['success' => true, 'message' => 'Template Successfully Edited']);
    }

    public function deleteTemplate(Request $request)
    {
        $projects = Project::select('id')->where('user_id', $this->current_user->id)->get();
        $tmp_project_ids = [];
        foreach ($projects as $project){
            array_push($tmp_project_ids, $project->id);
        }

        $template_id = $request->template_id;

        $template = Template::whereIn('project_id', $tmp_project_ids)->where('id', $template_id)->first();

        if(!$template){
            return json_encode(['success' => false, 'message' => 'Incorrect Template Id']);
        }

        Template::where('follow_template_id', $template_id)->update(['follow_template_id' => null, 'after_day' => null, 'after_hours' => null]);

        Template::where('id', $template_id)->delete();
        return json_encode(['success' => true, 'message' => 'Template Successfully Deleted']);
    }
}













