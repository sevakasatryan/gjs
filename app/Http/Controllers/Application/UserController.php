<?php

namespace App\Http\Controllers\Application;

use App\Area\User\Models\User;
use App\SmtpConnection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\Config;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;
use Swift_SmtpTransport;

class UserController extends BaseAdminController
{
    private $current_user = '';

    public function __construct(Request $request)
    {
        if (isset($request->token)){
            $this->current_user = User::where('email_api_token', $request->token)->first();
        }
    }

    public function attempLogin(Request $request){
        $email = $request->email;
        $password= $request->password;

        $res = User::where('email', $email)->first();
        if($res){
            if(Hash::check($password, $res->password)){
                $api_token = $this->generate_api_token(30);
                $res->email_api_token = $api_token;
                $res->save();
                return json_encode(['success' => true, 'token' => $api_token]);
            }else{
                return json_encode(['success' => false, 'message' => 'Incorrect Email Or Password']);
            }
        }else{
            return json_encode(['success' => false, 'message' => 'Incorrect Email Or Password']);
        }
    }

    public function generate_api_token($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz____';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getUser(Request $request)
    {
        $user = User::where('email_api_token', $request->token)->first();
        if($user){
            return json_encode(['success' => true, 'user' => $user]);
        }else{
            return json_encode(['success' => false, 'message' => 'Not user']);
        }
    }

    public function saveUserSettings(Request $request)
    {

        $first_name = $request->first_name;
        $last_name = $request->last_name;

        if($this->current_user){
            if($first_name){
                if($last_name){
                    $this->current_user->first_name = $first_name;
                    $this->current_user->last_name = $last_name;
                    $this->current_user->save();
                    return json_encode(['success' => true, 'message' => "username successfully changed to %$first_name%"]);
                }else{
                    return json_encode(['success' => false, 'message' => 'last name is required']);
                }
            }else{
                return json_encode(['success' => false, 'message' => 'first name is required']);
            }
        }else{
            return json_encode(['success' => false, 'message' => 'Not user']);
        }

    }

    public function chengePassword(Request $request)
    {
        $oldPassword = $request->old_password;
        $newPassword = $request->new_password;

        if($this->current_user){

            if(Hash::check($oldPassword, $this->current_user->password)){
                if(strlen($newPassword) >= 6){
                    $this->current_user->password = Hash::make($newPassword);
                    $this->current_user->save();
                    return json_encode(['success' => true, 'message' => 'Password successfully changed']);
                }else{
                    return json_encode(['success' => false, 'message' => 'Incorrect new password']);
                }
            }else{
                return json_encode(['success' => false, 'message' => 'Incorrect old password']);
            }

        }else{
            return json_encode(['success' => false, 'message' => 'Not user']);
        }

    }

    public function test()
    {



//
//
//
//
//
//
//
//
//
//        dd('-----------');
//
//
//
//        'The system failed to connect with your SMTP credentials. Please double check and try again.';
//
//        ini_set('max_execution_time', 300);
//
//
//
//
//
//        try{
//            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', '587', 'ssl');
//            $transport->setUsername('test960699@gmail.com');
//            $transport->setPassword('oy');
//            $mailer = new \Swift_Mailer($transport);
//            $mailer->getTransport()->start();
//            return 'ok';
//        } catch (\Swift_TransportException $e) {
//            return $e->getMessage();
//        } catch (\Exception $e) {
//            return $e->getMessage();
//        }
//
//
//
//
//
//
//
//
//
//        dd('--------');
//
//        $smtp_connection = SmtpConnection::where('user_id', $this->current_user->id)->first();
//        \Config::set('mail.host', $smtp_connection->host);
//        \Config::set('mail.port', $smtp_connection->port);
//        \Config::set('mail.username', $smtp_connection->username);
//        \Config::set('mail.password', $smtp_connection->password);
//        \Config::set('mail.from.address', $smtp_connection->from_email);
//        \Config::set('mail.from.name', $smtp_connection->from_name);
//
//        $transport = Mail::getSwiftMailer()->getTransport();
//        Mail::alwaysFrom(\Config::get('mail.from.address'), \Config::get('mail.from.name'));
//        $transport->setHost(\Config::get('mail.host'));
//        $transport->setPort(\Config::get('mail.port'));
//        $transport->setUsername(\Config::get('mail.username'));
//        $transport->setPassword(\Config::get('mail.password'));
//
//        $transport->setPort(\Config::get('mail.port'));
//        $transport->setUsername('argisht.amiryan@gmail.com');
//        $transport->setPassword('anvcrhdbgoaucgpk');
//        $data = array(
//            'name' => "Learning Laravel",
//        );
//
//        Mail::send('mail-test', $data, function ($message) use ($smtp_connection){
//            $message->to('sevakasatryan706@gmail.com')->subject('Learning Laravel test email');
//            if($smtp_connection->reply_to_email){
//                $message->replyTo('test1234567@gmail.com');
//            }
//        });
//


    }
}
