<?php namespace App\Http\Controllers\Application;

use App\Area\User\Models\Smtpservers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;
use Illuminate\Http\Response;

class ServersController extends BaseAdminController
{
    public function getIndex()
    {
        $servers = Smtpservers::all();

        $data = array(
            'servers' => $servers
        );
        return view('application.servers.index',$data);
    }

    public function addServer(Request $request){

        $input = $request->all();

        $input['user_id'] = Auth::id();
        Smtpservers::create($input);
        return redirect('/servers')->with('success', 'your message,here');
    }

    public static function sendMail(){

            Config::set('mail.host', 'smtp1.sample.com');
//
//            Mail::to($request->user())->send(new OrderShipped($order));
//
//            Config::set('mail.host', 'smtp2.sample.com');
//
//            Mail::to($request->user())->send(new OrderShipped($order));
    }
}