<?php namespace App\Http\Controllers\Application;

use App\contact;
use App\Http\Requests\AjaxRequest;
use App\Http\Requests\CommonRequest;
use App\lists;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Modules\AiApp\Base\BaseControllers\BaseAdminController;

class ListsController extends BaseAdminController
{
    private $current_user = '';

    public function __construct(Request $request)
    {
        if (isset($request->token)){
            $this->current_user = User::where('email_api_token', $request->token)->first();
        }

        if(!$this->current_user){
            return json_encode(['success' => false, 'message' => 'Not user']);
        }
    }

    public function getIndex(CommonRequest $request)
    {
        $lists = Lists::where('user_id', $this->current_user->id)->get();
        return json_encode(['success' => true, 'lists' => $lists]);
    }

    public function createList(Request $request)
    {
        $list_name = $request->list_name;
        if(Lists::where('name', $list_name)->where('user_id', $this->current_user->id)->count()){
            return json_encode(['success' => false, 'message' => 'List Name already exists']);
        }
        if(strlen($list_name)){
            $list = new Lists();
            $list->name = $list_name;
            $list->user_id = $this->current_user->id;
            $list->save();
            return json_encode(['success' => true, 'message' => 'List successfully created']);
        }else{
            return json_encode(['success' => false, 'message' => 'List Name Is Required']);
        }
    }

    public function editList(Request $request)
    {

        $list_name = $request->list_name;
        $listId = $request->list_id;
        $list = Lists::where('id', $listId)->where('user_id', $this->current_user->id)->first();
        if(!$list){
            return json_encode(['success' => false, 'message' => 'List does not exist']);
        }
        if(strlen($list_name)){
            if(!Lists::where('name', $list_name)->where('user_id', $this->current_user->id)->count()){
                $list->name = $list_name;
                $list->save();
                return json_encode(['success' => true, 'message' => 'List name successfully updated']);
            }else{
                return json_encode(['success' => false, 'message' => 'List name already exists']);
            }
        }else{
            return json_encode(['success' => false, 'message' => 'List Name Is Required']);
        }
    }

    public function deleteList(Request $request)
    {
        $list_id = $request->list_id;
        $res = Lists::where('id', $list_id)->where('user_id', $this->current_user->id)->delete();
        if($res){
            return json_encode(['success' => true, 'message' => 'List Successfully Deleted']);
        }else{
            return json_encode(['success' => false, 'message' => 'Fail to delete list']);
        }
    }

    public function addContact(Request $request)
    {
        $contact_email = $request->contact_email;
        $listId = $request->list_id;

        if(!Lists::where('user_id', $this->current_user->id)->where('id', $listId)->count()){
            return json_encode(['success' => false, 'message' => 'Invalid List id']);
        }

        if(Contact::where('email', $contact_email)->where('list_id', $listId)->count()){
            return json_encode(['success' => false, 'message' => 'You already have this contact in this list']);
        }

        $contact = new Contact();
        $contact->email = $contact_email;
        $contact->list_id = $listId;
        $contact->save();

        return json_encode(['success' => true, 'message' => 'Contact successfully added to the list']);

    }

    public function getContacts(Request $request)
    {
        $list_id = $request->list_id;

        if($list_id){
            $list = Lists::with('contacts')->where('user_id', $this->current_user->id)->where('id', $list_id)->first();
            return json_encode(['success' => true, 'list' => $list]);
        }else{
            return json_encode(['success' => false, 'message' => 'List id is required']);
        }

    }

    public function deleteContact(Request $request)
    {

        try{
            $emails = json_decode($request->emails);
        }catch (\Exception $e){
            $emails = $request->emails;
        }

        $list_id = $request->has('list_id')? $request->list_id: null;
        if(!count($emails)){
            return json_encode(['success' => false, 'message' => 'Contact email is required']);
        }
        if($list_id){
            $tmp_contacts_count = Contact::where('list_id', $list_id)->whereIn('email', $emails)->count();
            if(!$tmp_contacts_count){
                return json_encode(['success' => false, 'message' => 'Contact(s) not exists in this list']);
            }else{
                $del_res = Contact::where('list_id', $list_id)->whereIn('email', $emails)->delete();
                if($del_res){
                    return json_encode(['success' => true, 'message' => 'Contact(s) successfully deleted']);
                }else{
                    return json_encode(['success' => true, 'message' => 'Contact(s) is not deleted for some reasons']);
                }
            }
        }else{
            $list_ids = Lists::where('user_id', $this->current_user->id)->select(['id'])->get()->toArray();
            $tmp_list_ids = [];
            foreach ($list_ids as $list_id){
                array_push($tmp_list_ids, $list_id['id']);
            }
            if(!count($tmp_list_ids)){
                return json_encode(['success' => false, 'message' => 'you don\'t have list in your account']);
            }else{
                if(!Contact::whereIn('list_id', $tmp_list_ids)->whereIn('email', $emails)->count()){
                    return json_encode(['success' => false, 'message' => 'Contact(s) not exists in your account']);
                }
                $del_res = Contact::whereIn('list_id', $tmp_list_ids)->whereIn('email', $emails)->delete();
                if($del_res){
                    return json_encode(['success' => true, 'message' => 'Contact(s) successfully deleted']);
                }else{
                    return json_encode(['success' => true, 'message' => 'Contact(s) is not deleted for some reasons']);
                }
            }
        }
    }

    public function getContact(Request $request)
    {

        $email = $request->email;

        $list_ids = Lists::where('user_id', $this->current_user->id)->select(['id'])->get()->toArray();
        $tmp_list_ids = [];
        foreach ($list_ids as $list_id){
            array_push($tmp_list_ids, $list_id['id']);
        }
        if(!count($tmp_list_ids)){
            return json_encode(['success' => false, 'message' => 'you don\'t have list in your account']);
        }else{
            $get_res = Contact::whereIn('list_id', $tmp_list_ids)->where('email', $email)->first();
            if($get_res){
                return json_encode(['success' => true, 'contact' => $get_res]);
            }else{
                return json_encode(['success' => true, 'message' => 'Contact does not exist']);
            }
        }

    }

    public function editContact(Request $request)
    {
        $email = $request->email;
        $new_email = $request->new_email;
        $list_id = $request->has('list_id')? $request->list_id: null;
        if (!filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
            return json_encode(['success' => false, 'message' => 'Incorrect New Email Address']);
        }
        if($list_id){
            if(!Contact::where('list_id', $list_id)->where('email', $email)->count()){
                return json_encode(['success' => false, 'message' => 'Contact does not exists in this list']);
            }else{
                $res = Contact::where('list_id', $list_id)->where('email', $email)->first();
                if($res){
                    if(Contact::where('list_id', $list_id)->where('email', $new_email)->count()){
                        return json_encode(['success' => false, 'message' => 'new contact already exists in this list']);
                    }
                    $res->email = $new_email;
                    $res->save();
                    return json_encode(['success' => true, 'message' => 'Contact  successfully edited']);
                }else{
                    return json_encode(['success' => false, 'message' => 'Contact is not edited for some reasons']);
                }
            }
        }else{
            $list_ids = Lists::where('user_id', $this->current_user->id)->select(['id'])->get()->toArray();
            $tmp_list_ids = [];
            foreach ($list_ids as $list_id){
                array_push($tmp_list_ids, $list_id['id']);
            }
            if(!count($tmp_list_ids)){
                return json_encode(['success' => false, 'message' => 'you don\'t have list in your account']);
            }else{
                if(!Contact::whereIn('list_id', $tmp_list_ids)->where('email', $email)->count()){
                    return json_encode(['success' => false, 'message' => 'Contact not exists in your account']);
                }
                $res = Contact::whereIn('list_id', $tmp_list_ids)->where('email', $email)->get();
                if($res){
                    $count_edit = 0;
                    foreach($res as $usemail){
                        if(!Contact::where('list_id', $usemail->list_id)->where('email', $new_email)->count()){
                            $count_edit++;
                            $usemail->email = $new_email;
                            $usemail->save();
                        }
                    }
                    if($count_edit){
                        return json_encode(['success' => true, 'message' => "Contact successfully edited in $count_edit lists"]);
                    }else{
                        return json_encode(['success' => false, 'message' => 'new contact already exists in your lists']);
                    }
                }else{
                    return json_encode(['success' => false, 'message' => 'Contact is not edited for some reasons']);
                }
            }
        }
    }

    public function ajaxFormLoad(AjaxRequest $request)
    {
        $id = $request->get('id');
        $path = $id ? "lists/$id/edit" : 'lists/create';
        $result = $this->apiService->getMailApi()->get($path, [], $this->user->email_api_token);
        $form = $result->getData('item');

        return response()->json($this->answerOneItem($form));
    }

    public function ajaxFormPost(AjaxRequest $request)
    {
        $data = $request->all();

        if (empty($data['id'])) {
            $result = $this->apiService->getMailApi()->post('lists', $data, $this->user->email_api_token);
        } else {
            $result = $this->apiService->getMailApi()->put('lists/'.$data['id'], $data, $this->user->email_api_token);
        }

        if ($result->success) {
            /* all ok, load form */
            $item = $result->getData('item');
            $result = $this->apiService->getMailApi()->get('lists/'.$item['id'].'/edit', [], $this->user->email_api_token);
        }

        return response()->json($result);
    }

    public function ajaxDelete(AjaxRequest $request)
    {
        $ids = $request->get('ids');
        $result = $this->apiService->getMailApi()->post('lists/delete', ['data' => $ids], $this->user->email_api_token);

        return response()->json($result->toArray());
    }
}