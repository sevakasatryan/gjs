<?php

namespace App\Http\Middleware;

use App\Area\User\Models\User;
use Closure;
use Illuminate\Support\Facades\Response;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(User::where('email_api_token', $request->token)->count()){
            return $next($request);
        }else{
            //abort(401, 'unauthorised');
            return Response::json(['success' => false]);
        }
    }
}
