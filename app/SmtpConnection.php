<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmtpConnection extends Model
{
    protected $table = 'smtp_connections';
}
