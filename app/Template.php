<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    public function project(){
        return $this->hasOne('App\Project', 'id','project_id');
    }

    public function follow_template(){
        return $this->hasOne('App\Template', 'id','follow_template_id');
    }
}
