<?php namespace App\Area\Api\Models;

use ArrayObject;

class ApiItem extends ArrayObject
{
    public function __get($name)
    {
        return $this[$name] ?? null;
    }
}
