<?php namespace App\Area\Api\Services;

use App\Area\Api\Models\ApiItem;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\AiApp\Helpers\ActionResult;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class ApiHelper
{
    public $dates = ['updated_at', 'created_at'];

    public function wrapResult(ActionResult $actionResult, $path = null)
    {
        if ($actionResult->hasData('items')) {
            $items = $actionResult->getData('items');
            $items = $this->wrapItems($items);

            if ($actionResult->hasData('pagination')) {
                $pagination = $actionResult->getData('pagination');

                $items = new LengthAwarePaginator($items, $pagination['total'], $pagination['per_page'], $pagination['current_page']);
                //$items->hasMorePagesWhen($pagination['current_page'] <= $pagination['last_page']);
                $items->withPath($path);
            }

            $actionResult->setData('items', $items);
        }

        return $actionResult;
    }

    public function wrapItems(array $items): Collection
    {
        $res = Collection::make();
        foreach ($items as $item) {
            $apiItem = new ApiItem($item);

            foreach ($this->dates as $dateField) {
                if (isset($item[$dateField])) {
                    $apiItem[$dateField] = Carbon::parse($item[$dateField]);
                }
            }

            $res->push($apiItem);
        }

        return $res;
    }
}

