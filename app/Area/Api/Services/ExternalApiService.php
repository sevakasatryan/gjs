<?php namespace App\Area\Api\Services;

use App\Area\User\Models\User;
use App\Area\Api\Connector\EmailApi;
use App\Area\User\Services\UserService;
use Modules\AiApp\Helpers\ActionResult;
use AppLog;

class ExternalApiService
{
    protected $mailApi, $userService, $helper;


    public function __construct(EmailApi $emailApi, UserService $userService, ApiHelper $apiHelper)
    {
        $this->mailApi = $emailApi;
        $this->userService = $userService;
        $this->helper = $apiHelper;
    }

    public function getMailApi()
    {
        return $this->mailApi;
    }

    public function getHelper()
    {
        return $this->helper;
    }

    public function syncUser(User $user)
    {
        if (is_local()) {
            /** do not locate user by email in production!!! */
            $userRes = $this->mailApi->get('users/by_email/'.$user->email);
        } else {
            $userRes = $this->mailApi->get('users/me', [], $user->email_api_token);
        }

        if ($userRes instanceof ActionResult) {
            if ($userRes->code === 404) {
                /** ApiUser not found, create */
                $res = $this->mailApi->post('users', [
                    'id'         => $user->id,
                    'email'      => $user->email,
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                ]);

                if ($res->success) {
                    $apiToken = $res->getData('api_token');
                }
            } elseif ($userRes->success) {
                /** ApiUser already exists */
                $apiUser = $userRes->getData('item');
                $apiToken = $apiUser['api_token'] ?? null;

                /** Update */
                $this->mailApi->put('users/'.$apiUser['id'], [
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                ]);
            } else {
                AppLog::syncUserError($userRes);
            }

            if (!empty($apiToken)) {
                $user->email_api_token = $apiToken;
                $user = $this->userService->getBox()->saveAndFresh($user);
            }
        }

        return $user;
    }
}

