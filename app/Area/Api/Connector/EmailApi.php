<?php namespace App\Area\Api\Connector;

use Modules\AiApp\ApiConnect\Connector;
use AppLog;

class EmailApi extends Connector
{
    public function __construct()
    {
        $fullConfig = config('api-config');
        $provider = $fullConfig['provider'] ?? '';
        $config = $fullConfig['providers'][$provider] ?? [];

        parent::__construct($config);
    }

    protected function exception(\Exception $e)
    {
        throw new \Exception($e);
    }
}