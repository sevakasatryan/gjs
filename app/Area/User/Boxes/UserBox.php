<?php namespace App\Area\User\Boxes;

use Modules\AiApp\Base\BaseBoxes\BaseBox;
use App\Area\User\Models\User;
use Illuminate\Support\Collection;
use Hash, DB;

class UserBox extends BaseBox
{
    protected $with = ['media'];

    public static function className()
    {
        return User::class;
    }

    public function getTotals()
    {
        $res = $this->newQuery()
                    ->select('role', DB::raw('count(*) as total'))
                    ->groupBy('role')
                    ->pluck('total', 'role')
                    ->all();

        return $res;
    }

    public function getByEmail($email)
    {
        /** @var User $user */
        $user = $this->newQuery()->where('email', $email)->first();

        return $user;
    }
}