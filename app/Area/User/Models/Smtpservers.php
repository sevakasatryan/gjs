<?php namespace App\Area\User\Models;

use Illuminate\Database\Eloquent\Model;
class Smtpservers extends Model
{
    protected $table = 'smtp_servers';
    protected $fillable = ['id', 'user_id', 'host', 'username', 'password', 'port', 'from_name','from_email','reply_to_email'];


}
