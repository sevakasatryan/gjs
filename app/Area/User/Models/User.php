<?php namespace App\Area\User\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Modules\AiApp\Base\BaseModels\Traits\MediaMethodsTrait;
use Modules\AiApp\Base\BaseModels\BaseUser;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

class User extends BaseUser implements HasMedia, HasMediaConversions
{
    use Notifiable, HasMediaTrait, MediaMethodsTrait;

    const ROLE_USER  = 'user';
    const ROLE_ADMIN = 'admin';

    protected $fillable = ['first_name', 'last_name', 'email'];
    protected $visible = ['id', 'first_name', 'last_name', 'email', 'created_at'];
    protected $hidden = ['role', 'is_active', 'api_token', 'password', 'remember_token'];
    protected $casts = [
        'meta' => 'array',
    ];

    //------------------------------------------------------------------------------------------------------------------
    // Relations

    //------------------------------------------------------------------------------------------------------------------

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
             ->width(500)
             ->height(500)
             ->background('transparent')
             ->keepOriginalImageFormat()
             ->nonQueued()
             ->performOnCollections('src', 'tmp');
    }

    //------------------------------------------------------------------------------------------------------------------
    public function getNoThumbImage()
    {
        return asset('images/no-avatar.png');
    }

    public function isAdmin()
    {
        return ($this->role == self::ROLE_ADMIN);
    }
}
