<?php namespace App\Area\User\Services;

use App\Area\User\Models\User;
use Modules\AiApp\Helpers\ActionResult;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use App\Events\UserRegistered;
use App, Auth, Hash, Validator, Password;

class AdminUserService extends UserService
{
    public static function getRoles()
    {
        return [
            User::ROLE_USER,
            User::ROLE_ADMIN,
        ];
    }

    public function registration(array $data)
    {
        $data['role'] = User::ROLE_USER;
        $data['is_active'] = true;

        return $this->editUser(null, $data);
    }

    public function autoRegistration($email, $password = null)
    {
        if (empty($password)) {
            $password = str_shuffle(uniqid());
        }

        $parts = explode('@', $email);
        $data = [
            'first_name'            => empty($parts[0]) ? 'FirstName' : $parts[0],
            'last_name'             => '',
            'email'                 => $email,
            'role'                  => User::ROLE_USER,
            'password'              => $password,
            'password_confirmation' => $password,
            'is_active'             => true,
            'is_auto'               => true,
        ];

        return $this->editUser(null, $data);
    }

    public function editUser($id, $data): ActionResult
    {
        $result = new ActionResult();
        $data['is_active'] = !empty($data['is_active']);

        $isNew = empty($id);
        $rawPassword = $data['password'] ?? null;

        if (empty($data['password'])) {
            unset($data['password'], $data['password_confirmation']);
        }

        if ($isNew || $this->getBox()->find($id)) {
            $rules = [
                'first_name' => 'string|required',
                'last_name'  => 'string|sometimes|nullable',
                'email'      => ['email', 'required', Rule::unique('users')->ignore($id)],
                'role'       => 'in:'.join(',', self::getRoles()),
                'is_active'  => 'boolean',
                'is_auto'    => 'boolean|sometimes',
            ];

            if ($isNew) {
                $rules['password'] = 'required|min:6|confirmed';
                $rules['password_confirmation'] = 'required';
            } else {
                $rules['password'] = 'sometimes|min:6|confirmed';
                $rules['password_confirmation'] = 'sometimes';
            }

            $data = Collection::make($data)->only(array_keys($rules))->toArray();
            $validator = Validator::make($data, $rules);

            if ($validator->passes()) {
                if (!empty($data['password'])) {
                    $data['password'] = Hash::make($data['password']);
                }
                unset($data['password_confirmation']);

                User::unguard();
                /** @var User $user */
                $user = $this->getBox()->updateOrCreate(['id' => $id], $data);
                User::reguard();

                if ($user) {
                    if ($isNew) {
                        event(new UserRegistered($user, $rawPassword));
                    }
                    $result->setData('item', $user);
                    $result->allOK();
                } else {
                    $result->errorDB();
                }
            } else {
                $result->errorValidation($validator->messages()->toArray());
            }
        } else {
            $result->error404();
        }

        return $result;
    }

}
