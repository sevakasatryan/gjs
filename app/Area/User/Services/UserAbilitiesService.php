<?php namespace App\Area\User\Services;

use App\Area\File\Services\FileService;
use App\Area\User\Models\User;
use App\Area\Video\Services\VideoService;
use Carbon\Carbon;
use Modules\AiApp\Helpers\ActionResult;

class UserAbilitiesService
{
    protected $userService, $videoService, $fileService;

    public function __construct(UserService $userService, VideoService $videoService, FileService $fileService)
    {
        $this->userService = $userService;
        $this->videoService = $videoService;
        $this->fileService = $fileService;
    }

    public function updateStats($userId)
    {
        $result = new ActionResult();

        /** @var User $user */
        $user = $this->userService->getBox()->getById($userId);

        if ($user) {
            $fileStorage = $this->fileService->getBox()->countUserMediaStorage($user->id);
            $videoStorage = $this->videoService->getBox()->countUserMediaStorage($user->id);
            $totalUsed = $fileStorage + $videoStorage;

            $stats = [
                'files_count'          => $this->fileService->getBox()->countUserItems($user->id),
                'files_storage'        => $fileStorage,
                'videos_count'         => $this->videoService->getBox()->countUserItems($user->id),
                'videos_storage'       => $videoStorage,
                'total_storage'        => $totalUsed,
                'total_storage_human'  => format_bytes($totalUsed),
                'storage_used_percent' => $user->calculateStorageUsage($totalUsed),
                'date'                 => Carbon::now()->toDateTimeString(),
            ];

            $user->setMeta('stats', $stats);
            $this->userService->getBox()->save($user);
            $result->setData('stats', $stats);
            foreach ($stats as $k => $v) {
                $result->mylogOk($k.' ===> '.$v);
            }
            $result->allOK();

        } else {
            $result->error404();
        }

        return $result;
    }
}
