<?php namespace App\Area\User\Services;

use App\Area\User\Models\User;
use Modules\AiApp\Base\BaseServices\BaseModelService;
use Modules\AiApp\Helpers\ActionResult;
use Modules\AiApp\Avatar\AvatarServiceTrait;
use App\Area\User\Boxes\UserBox;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use App, Auth, Hash, Validator, Password;

/**
 * @method UserBox getBox()
 */
class UserService extends BaseModelService
{
    use AvatarServiceTrait;

    public function __construct(UserBox $userBox)
    {
        $this->setBox($userBox);
    }

    public function passwordForgot($email)
    {
        $result = new ActionResult();
        $credentials = ['email' => $email];
        $validator = Validator::make($credentials, ['email' => 'required|email']);
        if ($validator->passes()) {
            $broker = Password::broker();
            $response = $broker->sendResetLink($credentials);
            if (Password::RESET_LINK_SENT == $response) {
                $result->success(trans($response));
            } else {
                $result->error(trans($response));
            }
        } else {
            $result->errorValidation($validator->messages()->toArray());
        }

        return $result;
    }

    public function editProfileById($userId, array $data): ActionResult
    {
        $user = $this->getBox()->getById($userId);
        if ($user) {
            $result = $this->editProfile($user, $data);
        } else {
            $result = (new ActionResult())->error404();
        }

        return $result;
    }

    public function editProfile(User $user, array $data): ActionResult
    {
        $result = new ActionResult();
        $rules = [
            'first_name'  => 'sometimes|string|required',
            'last_name'   => 'sometimes|string|required',
            'email'       => ['sometimes', 'email', 'required', Rule::unique('users')->ignore($user->id)],
            'avatar_mode' => 'sometimes|nullable|in:set,remove',
        ];

        $data = Collection::make($data)->only(array_keys($rules))->toArray();
        if (count($data)) {
            $validator = Validator::make($data, $rules);
            if ($validator->passes()) {
                /** @todo email disabled */
                unset($data['email']);
                $this->checkAvatar($user, $data['avatar_mode'] ?? false);
                $this->getBox()->update($user, $data);
                $user = $this->getBox()->fresh($user);
                $result->setData('item', $user);
                $result->allOK();
            } else {
                $result->errorValidation($validator->messages()->toArray());
            }
        } else {
            $result->errorIncomingData();
        }

        return $result;
    }

    public function changeUserPassword($userId, $old, $new): ActionResult
    {
        $user = $this->getBox()->getById($userId);
        if ($user) {
            $data = [
                'old_password'          => $old,
                'password'              => $new,
                'password_confirmation' => $new,
            ];
            $result = $this->changePassword($user, $data);
        } else {
            $result = (new ActionResult())->error404();
        }

        return $result;
    }

    public function changePassword(User $user, array $data): ActionResult
    {
        $result = new ActionResult();
        $rules = [
            'old_password'          => 'required|old_password:'.$user->password,
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ];

        $data = Collection::make($data)->only(array_keys($rules))->toArray();

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        }, 'Invalid Old Password');

        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            $user->password = Hash::make($data['password']);
            $this->getBox()->save($user);
            $result->allOK();
        } else {
            $result->errorValidation($validator->messages()->toArray());
        }

        return $result;
    }
}
