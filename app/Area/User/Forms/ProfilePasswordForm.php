<?php namespace App\Area\User\Forms;

use App\Area\User\Models\User;
use Modules\AiApp\Base\BaseForms\BaseForm;

class ProfilePasswordForm extends BaseForm
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getFields(): array
    {
        return [
            'id'                    => $this->user->id,
            'old_password'          => '',
            'password'              => '',
            'password_confirmation' => '',
        ];
    }
}
