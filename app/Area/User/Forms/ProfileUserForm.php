<?php namespace App\Area\User\Forms;

use App\Area\User\Models\User;
use Modules\AiApp\Base\BaseForms\BaseForm;

class ProfileUserForm extends BaseForm
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getFields(): array
    {
        return [
            'id'          => $this->user->id,
            'first_name'  => $this->user->first_name,
            'last_name'   => $this->user->last_name,
            'email'       => $this->user->email,
            'avatar_mode' => '',
        ];
    }

    public function getChoices(): array
    {
        return [
            'avatar'      => $this->user->thumb,
            'avatar_mode' => ['', 'set', 'remove'],
            'has_avatar'  => boolval($this->user->getSrc()),
            'no_avatar'   => $this->user->getNoThumbImage(),
        ];
    }

}
