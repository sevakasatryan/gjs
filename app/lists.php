<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lists extends Model
{
    protected $table = 'lists';

    public function contacts(){
        return $this->hasMany('App\contact', 'list_id','id');
    }
}
