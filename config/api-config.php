<?php

return [
    'provider'  => env('MAIL_API_PROVIDER', ''),
    'log'       => env('MAIL_API_LOG_QUERY', false),
    'providers' => [
        'local' => [
            'adapter'    => 'http',
            'api_token'  => 'c15877d3ad985759e196dbb55cf3a4e3',
            'api_url'    => 'http://gjs-api.loc/api/v1/',
            'basic_auth' => [
                'username' => '',
                'password' => '',
            ],
        ],
    ],
];
