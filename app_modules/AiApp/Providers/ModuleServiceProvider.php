<?php namespace Modules\AiApp\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    use CustomSetupTrait;

    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'ai-app');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'ai-app');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'ai-app');

        $this->customBoot();
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        //$this->app->register(RouteServiceProvider::class);

        $this->customRegister();
    }
}
