<?php namespace Modules\AiApp\Providers;

use Modules\AiApp\AppLog\AppLogService;
use Modules\AiApp\Console;
use Spatie\MediaLibrary\Filesystem\Filesystem as MediaFilesystem;
use Modules\AiApp\Extend\FileSystem\CustomFilesystem;
use Modules\AiApp\Extend\Console\ExtVendorPublishCommand;
use Modules\AiApp\FileSystem\Adapter\MyLocalAdapter;
use Modules\AiApp\FileSystem\Adapter\MyAzureAdapter;
//use MicrosoftAzure\Storage\Common\ServicesBuilder;
use League\Flysystem\Filesystem;
use Storage, File;

trait CustomSetupTrait
{
    public function customBoot()
    {
        $this->extendStorage();
    }

    public function customRegister()
    {
        /**
         * AppLog
         */
        $this->app->singleton('app.log', AppLogService::class);

        /**
         * Console commands
         */
        $this->commands([
            Console\NodeStatus::class,
            Console\RemoveExpiredFiles::class,
        ]);

        /**
         * Custom MediaLibrary Filesystem
         * @see https://docs.spatie.be/laravel-medialibrary/v6/advanced-usage/overriding-the-default-filesystem-behaviour
         */
        $this->app->bind(MediaFilesystem::class, CustomFilesystem::class);

        /**
         * Custom vendor:publish command - symlink instead of copy
         */
        $this->app->extend('command.vendor.publish', function () {
            return new ExtVendorPublishCommand($this->app['files']);
        });

        /**
         * Publishes
         */
        $this->publishes([
            //__DIR__.'/../Config/config.php'        => config_path('modules/aiapp.php'),
            __DIR__.'/../Publish/.htpasswd'     => base_path('.htpasswd'),
            __DIR__.'/../Publish/bin/'          => base_path('bin'),
            __DIR__.'/../Publish/public/db.php' => public_path('db.php'),
        ], 'config');

        /**
         * Config
         */
        $this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'modules.aiapp');
    }

    protected function extendStorage()
    {
        /**
         * Local + ttl symlinks
         */
        Storage::extend('my-local', function ($app, $config) {
            $opts = ['public_disk', 'ttl_folder', 'ttl_minutes'];
            self::checkConfig($opts, $config);

            $permissions = isset($config['permissions']) ? $config['permissions'] : [];
            $links = ($config['links'] ?? '') === 'skip' ?
                MyLocalAdapter::SKIP_LINKS :
                MyLocalAdapter::DISALLOW_LINKS;

            $adapter = new MyLocalAdapter($config['root'], LOCK_EX, $links, $permissions);
            $adapter->setConfig($config);

            return new Filesystem($adapter, $config);
        });

        /**
         * Azure
         */
        Storage::extend('my-azure', function ($app, $config) {
            $opts = ['ttl_minutes'];
            self::checkConfig($opts, $config);

            $endpoint = sprintf(
                'DefaultEndpointsProtocol=https;AccountName=%s;AccountKey=%s',
                $config['name'],
                $config['key']
            );
            $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($endpoint);

            $adapter = new MyAzureAdapter($blobRestProxy, $config['container']);
            $adapter->setConfig($config);

            return new Filesystem($adapter, $config);
        });
    }

    protected static function checkConfig(array $opts, $config)
    {
        foreach ($opts as $opt) {
            if (empty($config[$opt])) {
                throw new \Exception('Config option "'.$opt.'" need to be defined!');
            }
        }
    }
}
