<?php namespace Modules\AiApp\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\FilesystemAdapter;
use Storage;

class TestTableSeeder extends Seeder
{
    public function run()
    {
        echo 'Cleaning storage..'.PHP_EOL;
        foreach (['media', 'tmp'] as $disk) {
            echo 'DISK: '.$disk.PHP_EOL;

            /** @var FilesystemAdapter $storage */
            $storage = Storage::disk($disk);
            $prefix = $storage->getDriver()->getAdapter()->getPathPrefix();

            if (preg_match('|\/'.$disk.'\/$|', $prefix)) {
                echo 'REMOVE SYMLINKS:'.PHP_EOL;
                $cmd = sprintf('find %s -type l -print -delete', $prefix);
                exec($cmd, $output);
                var_dump($output);
            } else {
                throw new \Exception('Invalid PATH:'.$prefix);
            }

            foreach ($storage->directories() as $dir) {
                $fullPath = $prefix.$dir;
                echo 'DIR: '.$fullPath.PHP_EOL;

                $storage->deleteDirectory($dir);
            }
        }
        echo 'OK'.PHP_EOL;
    }
}