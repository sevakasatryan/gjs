<?php namespace Modules\AiApp\Extend\Console;

use Illuminate\Foundation\Console\VendorPublishCommand;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\Adapter\Local as LocalAdapter;

class ExtVendorPublishCommand extends VendorPublishCommand
{
    protected function buildRelativeSymlink($from, $link)
    {
        $fromPath = realpath($from);
        $relativePath = ltrim(str_replace(base_path(), '', $fromPath), '/');
        $relativeLink = ltrim(str_replace(base_path(), '', $link), '/');
        $level = substr_count($relativeLink, '/');
        for ($j = 0; $j < $level; $j++) {
            $relativePath = '../'.$relativePath;
        }

        if (!file_exists($link)) {
            symlink($relativePath, $link);
        }

        return $relativePath;
    }

    protected function isSuitablePath($path)
    {
        return (strpos($path, 'app_modules') > 0);
    }

    /**
     * Publish the file to the given path.
     *
     * @param  string $from
     * @param  string $to
     * @return void
     */
    protected function publishFile($from, $to)
    {
        if ($this->isSuitablePath($from)) {
            if (!$this->files->exists($to) || $this->option('force')) {
                $this->createParentDirectory(dirname($to));
                $this->buildRelativeSymlink($from, $to);
                $this->status($from, $to, 'File');
            }
        } else {
            parent::publishFile($from, $to);
        }
    }

    /**
     * Publish the directory to the given directory.
     *
     * @param  string $from
     * @param  string $to
     * @return void
     */
    protected function publishDirectory($from, $to)
    {
        if ($this->isSuitablePath($from)) {
            $to = rtrim($to, '/').'/';
            $fromFs = new Flysystem(new LocalAdapter($from));
            $this->createParentDirectory($to);

            foreach ($fromFs->listContents() as $item) {
                $itemPath = trim($item['path'] ?? '-', '/');
                $fromPath = $fromFs->getAdapter()->getPathPrefix().$itemPath;
                $toPath = rtrim($to, '/').'/'.$itemPath;
                $this->buildRelativeSymlink($fromPath, $toPath);
            }

            $this->status($from, $to, 'Directory');
        } else {
            parent::publishDirectory($from, $to);
        }
    }
}
