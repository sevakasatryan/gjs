<?php namespace Modules\AiApp\Extend\FileSystem;

use Spatie\MediaLibrary\Filesystem\DefaultFilesystem;
use Spatie\MediaLibrary\Media;

class CustomFilesystem extends DefaultFilesystem
{

    /*
 * Copy a file to the medialibrary for the given $media.
 */
    public function copyToMediaLibrary(string $pathToFile, Media $media, bool $conversions = false, string $targetFileName = '')
    {
        $destination = $this->getMediaDirectory($media, $conversions).
            ($targetFileName == '' ? pathinfo($pathToFile, PATHINFO_BASENAME) : $targetFileName);


        if (!$conversions && $this->needSymlink($media)) {
            $fs = $this->filesystem->disk($media->disk);
            $targetPath = $fs->path($destination);
            //ddd($targetPath);
            symlink($pathToFile, $targetPath);

            return;
        }

        parent::copyToMediaLibrary($pathToFile, $media, $conversions, $targetFileName);
    }

    protected function needSymlink(Media $media)
    {
        $need = false;
        $model = $media->model;
        if ($model && method_exists($model, 'symlinkInsteadOfCopy') && $model->symlinkInsteadOfCopy()) {
            $need = true;
        }

        return $need;
    }
}