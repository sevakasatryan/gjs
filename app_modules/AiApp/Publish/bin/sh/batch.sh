#!/bin/bash

for i in {1..30}
do
    echo "-----------------------------------------------------------------------------------------------"
    echo "> $i"

    php artisan x:process-task --schedule $i

    echo
done
