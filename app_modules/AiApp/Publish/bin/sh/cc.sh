#!/usr/bin/env bash

php artisan clear-compiled
php artisan config:clear
php artisan cache:clear
php artisan view:clear

php composer.phar dump-autoload
php artisan optimize

php artisan queue:restart
#-----------------------------------------------------------------------------------------------------------------------

for folder in bootstrap/cache storage public/test
do
    echo '>>>' $folder

    if [ -d "$folder" ]; then
        sudo chgrp -R www-data $folder
        sudo find $folder -type d -exec chmod 0775 '{}' \;
        sudo find $folder -type f -exec chmod 0664 '{}' \;
    fi
done
