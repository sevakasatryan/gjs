#!/usr/bin/env bash

clear

if [ -z "$1" ]; then
    echo "Specify evironment as 1 argumant (dev / prod)"
    echo "press any key..."
    read;
    exit;
fi

function echoo {
  bold=`tput bold`
  col=`tput setaf 3`
  reset=`tput sgr0`

  echo "${bold}${col}$1${reset}"
}

read -r -p "Run update script? [y/N] " response
if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
then
    php artisan down

    echoo "[updating sources]"
    #git reset --hard HEAD
    git reset
    git clean -f
    git checkout .
    git pull

    echoo "[clear cache]"
    sudo php artisan clear-compiled
    php artisan config:clear
    php artisan view:clear

    echoo "[replace .env file]"
    yes | cp -rf ".env_$1" .env

    echoo "[cache config]"
    php artisan config:cache
    php artisan optimize

    php artisan up

    echoo "[done]"
else
    echoo "[skip]"
fi
