<?php namespace Modules\AiApp\Area\Worker\Models;

use Modules\AiApp\Base\BaseModels\BaseModel;
use Carbon\Carbon;

class WorkerStats extends BaseModel
{
    protected $table = 'worker_stats';
    protected $guarded = ['id'];
    protected $casts = [
        'info' => 'array',
    ];

    public function getHumanDateAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

//    protected function isOk(){
//        Carbon::now()->diffForHumans();
//        //return ($this->count && $this->updated_at->diff())
//    }
}
