<?php namespace Modules\AiApp\Area\Worker\Boxes;

use Modules\AiApp\Area\Worker\Models\WorkerStats;
use Modules\AiApp\Base\BaseBoxes\BaseBox;

class WorkerStatsBox extends BaseBox
{
    public static function className()
    {
        return WorkerStats::class;
    }

    public function getStats()
    {
        return $this->newQuery()
                    ->orderBy('node', 'asc')
                    ->get();
    }
}