<?php namespace Modules\AiApp\Area\Worker\Services;

use Modules\AiApp\Area\Worker\Boxes\WorkerStatsBox;
use Modules\AiApp\Helpers\ActionResult;
use Modules\AiApp\Base\BaseServices\BaseModelService;
use Carbon\Carbon;

class WorkerStatsService extends BaseModelService
{
    public function __construct(WorkerStatsBox $box)
    {
        $this->setBox($box);
    }

    public function updateStats()
    {
        $result = ActionResult::getInstance()->forLogs(__METHOD__);

        //$cmd = "/bin/ps axfww | grep queue:work | grep -v 'sh -c' | grep ".strtoupper(config('app.name'));
        //$cmd = "/bin/ps aux | grep queue:work | grep -v 'sh -c' | grep ".strtoupper(config('app.name'));

        $cmd = "/bin/ps axfww | grep [q]ueue:work | grep =".app_code();

        $result->mylogOk($cmd);

        exec($cmd, $out);
        $out = (array)$out;

        foreach ($out as $str) {
            $result->mylogOk($str);
        }

        $nodeName = gethostname();

        $this->getBox()->updateOrCreate(['node' => $nodeName], [
            'node'       => $nodeName,
            'info'       => $out,
            'count'      => count($out),
            'updated_at' => Carbon::now(),
        ]);

        return $result;
    }

}

