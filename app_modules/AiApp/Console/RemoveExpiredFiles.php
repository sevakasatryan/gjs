<?php namespace Modules\AiApp\Console;

use Modules\AiApp\Base\BaseCommands\BaseCommand;
use App\Area\Common\Services\StorageService;

class RemoveExpiredFiles extends BaseCommand
{
    const COMMAND_NAME = 'x:remove-expired-files {minutes?}';

    protected $signature = self::COMMAND_NAME;
    protected $description = 'Remove expired symlinks from public folder; Remove temporary files (argument: minutes, default: 1440);';

    public function handle(StorageService $storageService)
    {
        $minutesAgo = intval($this->argument('minutes'));

        $result = $storageService->removeExpiredSymlinks();
        $this->printActionResult($result);

        $result = $storageService->cleanTempStorage($minutesAgo);
        $this->printActionResult($result);
    }
}
