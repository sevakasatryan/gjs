<?php namespace Modules\AiApp\Console;

use Modules\AiApp\Area\Worker\Services\WorkerStatsService;
use Modules\AiApp\Base\BaseCommands\BaseCommand;

class NodeStatus extends BaseCommand
{
    const COMMAND_NAME = 'x:node-status';

    protected $signature = self::COMMAND_NAME;
    protected $description = 'Node Status';

    public function handle(WorkerStatsService $workerStatsService)
    {
        $result = $workerStatsService->updateStats();
        $this->printActionResult($result);
    }
}
