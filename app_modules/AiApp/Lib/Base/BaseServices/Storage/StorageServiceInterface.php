<?php namespace Modules\AiApp\Base\BaseServices\Storage;

use Modules\AiApp\Helpers\TempStorage;

interface StorageServiceInterface
{
    public function getTempDisk();

    public function getPublicDisk();

    public function getTempStorage($dir): TempStorage;
}