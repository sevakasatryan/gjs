<?php namespace Modules\AiApp\Base\BaseServices\Storage;

use Modules\AiApp\Helpers\TempStorage;
use Modules\AiApp\Helpers\ActionResult;
use Illuminate\Database\Eloquent\Model;
use Storage;

abstract class BaseStorageService implements StorageServiceInterface
{
    /******************************************************************************************************************/
    public function getTempDisk()
    {
        throw new \Exception('Not defined');
    }

    public function getPublicDisk()
    {
        throw new \Exception('Not defined');
    }

    public function getTempStorage($dir): TempStorage
    {
        $storage = Storage::disk($this->getTempDisk());

        return new TempStorage($this->getTempDisk(), $storage, $dir);
    }

    public function cleanTempStorage($minutesAgo = null): ActionResult
    {
        if (empty($minutesAgo)) {
            $minutesAgo = 60 * 24;
        }

        $result = ActionResult::getInstance()->forLogs(__METHOD__);

        $storage = Storage::disk($this->getTempDisk());

        $dir = $storage->getDriver()->getAdapter()->getPathPrefix();

        if (file_exists($dir)) {
            $result->mylogOk(sprintf('-> Find files older and newer than %s minutes:', $minutesAgo));
            $cmd = sprintf('find %s -type f -mmin +$((%s)) \( ! -iname ".gitignore" \) -print -delete', $dir, $minutesAgo);
            exec($cmd, $output);

            $result->mylogOk($cmd);
            $result->mylogOk($output);
            unset($output);

            $result->mylogOk('-> Remove empty folders:');
            $cmd = sprintf('find %s -mindepth 1 -type d -empty -print -delete', $dir, $minutesAgo);
            exec($cmd, $output);
            $result->mylogOk($cmd);
            $result->mylogOk($output);
            unset($output);
        } else {
            $result->mylogError('TMP dir does not exists.');
        }

        return $result;
    }

    public function copyMediaFromStorage(Model $media, $localDir)
    {
        $target = null;
        if (method_exists($media, 'getContent') && property_exists($media, 'file_name')) {
            if (is_dir($localDir) && is_writable($localDir)) {
                $target = rtrim($localDir, '/').'/'.$media->file_name;
                file_put_contents($target, $media->getContent());
            } else {
                throw new \Exception('Invalid local dir : '.$localDir);
            }
        } else {
            throw new \Exception('Invalid object: '.get_class($media));
        }

        return $target;
    }


//    public function copyFromStorage(FileUploadInterface $model, $localFilePath = false)
//    {
//        $content = $model->getStorage()->get($model->file_path);
//
//        if (empty($localFilePath)) {
//            $tmpDir = $this->getTempStorage(sprintf('/copy/%s/%s', class_basename($model), $model->id));
//            $tmpDir->doNotRemove();
//            $path = $tmpDir->put(basename($model->getFileDataObj()->source), $content);
//
//            $localFilePath = $tmpDir->getFullPath(basename($path));
//        } else {
//            file_put_contents($localFilePath, $content);
//        }
//
//        return $localFilePath;
//    }


    /******************************************************************************************************************/

//
//    public function restoreFilesFolder()
//    {
//        try {
//            $storage = Storage::disk($this->getDisk());
//            if ($storage->exists('trash/'.$this->getFilesFolder())) {
//                copyStorageFilesFolderToFolder($storage, 'trash/'.$this->getFilesFolder(), $this->getFilesFolder(), true);
//            }
//        } catch (\Exception $e) {
//            Log::error($e->getMessage());
//        }
//    }
//
//    public function copyFilesToFolder($destFolderPath, $move = false)
//    {
//        try {
//            $storage = Storage::disk($this->getDisk());
//            if ($storage->exists($this->getFilesFolder()) && $destFolderPath) {
//                copyStorageFilesFolderToFolder($storage, $this->getFilesFolder(), $destFolderPath, (bool)$move);
//            }
//        } catch (\Exception $e) {
//            Log::error($e->getMessage());
//        }
//    }
//

}