<?php namespace Modules\AiApp\Base\BaseServices;

use Modules\AiApp\Base\BaseBoxes\BaseBox;

interface BaseModelServiceInterface
{
    public function getBox();

    public function setBox(BaseBox $box);
}