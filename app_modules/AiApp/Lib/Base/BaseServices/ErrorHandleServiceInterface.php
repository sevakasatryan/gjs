<?php namespace Modules\AiApp\Base\BaseServices;

interface ErrorHandleServiceInterface
{
    public function handleException(\Exception $exception);

    public function handleFailedJobException(array $data = []);

    public static function prepareErrorData($in): array;
}