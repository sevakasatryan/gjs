<?php namespace Modules\AiApp\Base\BaseServices;

use Exception;

abstract class BaseErrorHandleService implements ErrorHandleServiceInterface
{
    public function handleException(\Exception $exception)
    {
    }

    public function handleFailedJobException(array $data = [])
    {
    }

    public static function prepareErrorData($in): array
    {
        if (!is_array($in)) {
            $in = [$in];
        }

        $data = [];
        foreach ($in as $item) {
            if ($item instanceof \Exception) {
                $data[] = $item->getMessage();
                $data[] = sprintf('%s : %s', $item->getFile(), $item->getLine());
            } else {
                $data[] = strval($item);
            }
        }

        return $data;
    }
}