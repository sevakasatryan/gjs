<?php namespace Modules\AiApp\Base\BaseServices;

use Modules\AiApp\Base\BaseBoxes\BaseBox;
use Modules\AiApp\Helpers\ActionResult;

abstract class BaseModelService implements BaseModelServiceInterface
{
    private $box;

    /**
     * @return BaseBox
     */
    public function getBox()
    {
        return $this->box;
    }

    public function setBox(BaseBox $box)
    {
        $this->box = $box;
    }

    public function getItemParam($userId, $itemId, $key, $default = null): ActionResult
    {
        $result = new ActionResult();
        $item = $this->getBox()->getUserItemById($userId, $itemId);

        if ($item) {
            $data = $item->getParam($key, $default);
            $result->setData('item', $data);
            $result->allOK();
        } else {
            $result->error404();
        }

        return $result;
    }

    public function setItemParam($userId, $itemId, $key, $value): ActionResult
    {
        $result = new ActionResult();
        $item = $this->getBox()->getUserItemById($userId, $itemId);

        if ($item) {
            $item->setParam($key, $value);
            $item = $this->getBox()->saveAndFresh($item);
            $data = $item->getParam($key);
            $result->setData('item', $data);
            $result->allOK();
        } else {
            $result->error404();
        }

        return $result;
    }

}