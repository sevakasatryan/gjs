<?php namespace Modules\AiApp\Base;

abstract class BaseConstants
{
    Const SORT_ASC  = 'asc';
    Const SORT_DESC = 'desc';
}