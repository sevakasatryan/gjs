<?php namespace Modules\AiApp\Base\BaseForms;

abstract class BaseForm implements BaseFormInterface
{
    public function getField($fieldName)
    {
        $fields = $this->getFields();

        return $fields[$fieldName] ?? null;
    }

    public function getChoice($fieldName): array
    {
        $choices = $this->getChoices();

        return $choices[$fieldName] ?? [];
    }

    public function getFields(): array
    {
        return [];
    }

    public function getChoices(): array
    {
        return [];
    }

    public function getData(): array
    {
        return [
            'fields'  => $this->getFields(),
            'choices' => $this->getChoices(),
        ];
    }

    public function toArray()
    {
        return $this->getData();
    }

    public function getValue($fieldName)
    {
        return old($fieldName, $this->getField($fieldName));
    }
}
