<?php namespace Modules\AiApp\Base\BaseForms;

interface BaseFormInterface
{
    public function getFields();

    public function getChoices();

    public function getData();
}