<?php namespace Modules\AiApp\Base\BaseHelpers;

use Illuminate\Support\Collection;
use Auth, Route;

abstract class BaseViewHelper
{
    public static function getJsConfig()
    {
        $config = [
            'js_debug'    => config('app.debug') ? true : false,
            'token'       => csrf_token(),
            'images_path' => asset('/images/'),
            'public'      => asset('/'),
        ];

        if (Auth::check()) {
            $config = array_merge($config, [
                'routes' => self::getRoutes(),
            ]);
        }

        return json_encode($config);
    }

    //------------------------------------------------------------------------------------------------------------------

    protected static function getRoutes()
    {
        $routes = Collection::make();
        $routeCollection = Route::getRoutes();
        foreach ($routeCollection as $item) {
            if (empty($item->parameterNames()) && $item->getName()) {
                $routes->push($item->getName());
            }
        };
        $ret = $routes->mapWithKeys(function ($routeName) {
            return [$routeName => route($routeName)];
        });

        return $ret;
    }
}
