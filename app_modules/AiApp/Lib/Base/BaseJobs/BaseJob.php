<?php namespace Modules\AiApp\Base\BaseJobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\AiApp\Base\BaseServices\ErrorHandleServiceInterface;
use Modules\AiApp\Base\BaseServices\BaseErrorHandleService;
use Modules\AiApp\Helpers\ActionResult;
use Exception, AppLog;

abstract class BaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "onQueue" and "delay" queue helper methods.
    |
    */

    protected function logActionResult(ActionResult $actionResult = null)
    {
        if ($actionResult && $actionResult->error) {
            AppLog::jobs($actionResult);
        }
    }

    protected function getErrorHandleService(): ErrorHandleServiceInterface
    {
        return new class() extends BaseErrorHandleService
        {
        };
    }

    /**
     * The job failed to process.
     *
     * @param  Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        $data = [
            get_called_class(),
            $exception,
        ];
        $errorHandleService = $this->getErrorHandleService();
        $errorHandleService->handleFailedJobException($data);
    }
}
