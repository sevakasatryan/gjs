<?php namespace Modules\AiApp\Base\BaseModels\Traits;

trait UserTrait
{
    public function getFullNameAttribute()
    {
        return $this->getFullName();
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function isAdmin()
    {
        return (bool)$this->is_admin;
    }
}