<?php namespace Modules\AiApp\Base\BaseModels\Traits;

use Illuminate\Support\Collection;
use Modules\AiApp\Media\Media;

trait MediaMethodsTrait
{
    //------------------------------------------------------------------------------------------------------------------
    /* Attributes */
    public function getSrcAttribute()
    {
        return $this->getSrc();
    }

    public function getThumbAttribute()
    {
        return $this->getThumb();
    }

    public function getPropsAttribute()
    {
        return $this->getProps();
    }

    //------------------------------------------------------------------------------------------------------------------

    /**
     * @return Media
     */
    public function getSrc()
    {
        return $this->getMedia('src')->first();
    }

    public function getThumb()
    {
        if ($this->getSrc()) {
            $relUrl = $this->getSrc()->getUrl('thumb');
            $tu = $relUrl ? $this->getSrc()->getFullUrl('thumb') : '';
        } else {
            $tu = $this->getNoThumbImage();
        }

        return $tu;
    }

    public function getSources(): Collection
    {
        return $this->getMedia('src');
    }

    public function getSourcesArray(): array
    {
        $sources = $this->getSources();
        $ret = $sources->map(function (Media $item) {
            return [
                'name'  => $item->name,
                'file'  => $item->getUrl(),
                'thumb' => $item->getUrl('thumb'),
                'mime'  => $item->mime_type,
            ];
        })->toArray();

        return $ret;
    }

    public function getNoThumbImage()
    {
        return '';
    }

    public function getProps()
    {
        if ($this->getSrc()) {
            $props = $this->getSrc()->getProps();
        } else {
            $props = null;
        }

        return $props;
    }

    public function getMediaInfo()
    {
        if ($this->getSrc()) {
            $info = $this->getSrc()->getMediaInfo();
        } else {
            $info = '';
        }

        return $info;
    }

    public function symlinkInsteadOfCopy()
    {
        return false;
    }

    public function clearAllCollectionsExcept(array $exceptCollections = [])
    {
        $allCollections = $this->media->pluck('collection_name')->unique();
        $diff = $allCollections->diff($exceptCollections);
        $diff->each(function ($collectionName) {
            $this->clearMediaCollection($collectionName);
        });

        return $diff;
    }
}
