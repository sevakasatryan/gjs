<?php namespace Modules\AiApp\Base\BaseModels;

interface BaseUserInterface
{
    public function getFullNameAttribute();

    public function getFullName();

    public function isAdmin();
}
