<?php namespace Modules\AiApp\Base\BaseModels;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

abstract class BaseMongoModel extends Eloquent implements BaseModelInterface
{
    protected $connection = 'mongodb';
    protected $guarded = ['id', '_id'];
    protected $hidden = ['_id'];
    protected $appends = ['id'];

    public function getCollectionName()
    {
        return $this->collection;
    }

    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            $value = '';
        }

        if ($this->getCastType($key) == 'array') {
            return (array)$value;
        } else {
            return parent::castAttribute($key, $value);
        }
    }

    protected function isJsonCastable($key)
    {
        return $this->hasCast($key) && in_array($this->getCastType($key), ['json', 'object', 'collection'], true);
    }

    public function forceAttributesCasts()
    {
        foreach ($this->casts as $attr => $cast) {
            $value = $this->getAttributeValue($attr);
            $this->{$attr} = $this->castAttribute($attr, $value);
        }
    }


    public function originalIsNumericallyEquivalent($key)
    {
        return false;
    }
}