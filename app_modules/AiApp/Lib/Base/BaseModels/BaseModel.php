<?php namespace Modules\AiApp\Base\BaseModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model implements BaseModelInterface
{
    protected $perPage = 20;

    public function getCreatedAtTsAttribute()
    {
        if ($this->created_at instanceof Carbon) {
            return $this->created_at->getTimeStamp();
        }

        return null;
    }

    public function getMeta($key, $default = null)
    {
        return $this->getArr('meta', $key, $default);
    }

    public function setMeta($key, $value = null)
    {
        return $this->setArr('meta', $key, $value);
    }

    public function getParam($key, $default = null)
    {
        return $this->getArr('params', $key, $default);
    }

    public function setParam($key, $value = null)
    {
        return $this->setArr('params', $key, $value);
    }

    public function getArr($field, $key, $default = null)
    {
        return $this->{$field}[$key] ?? $default;
    }

    public function setArr($field, $key, $value)
    {
        if (is_array($key)) {
            $this->{$field} = $key;
        } else {
            $arr = (array)$this->{$field};
            $arr[$key] = $value;
            $this->{$field} = $arr;
        }
    }
}
