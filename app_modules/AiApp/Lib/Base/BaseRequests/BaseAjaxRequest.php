<?php namespace Modules\AiApp\Base\BaseRequests;

class BaseAjaxRequest extends BaseRequest
{
    public function authorize()
    {
        return $this->ajax();
    }
}