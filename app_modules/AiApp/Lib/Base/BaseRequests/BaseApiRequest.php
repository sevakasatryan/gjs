<?php namespace Modules\AiApp\Base\BaseRequests;

class BaseApiRequest extends BaseRequest
{
    public function authorize()
    {
        $userId = $this->get('user_id');

        return !empty($userId);
    }
}