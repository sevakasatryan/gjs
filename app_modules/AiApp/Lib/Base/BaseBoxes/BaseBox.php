<?php namespace Modules\AiApp\Base\BaseBoxes;

use Illuminate\Support\Collection;
use Modules\AiApp\Base\BaseModels\BaseModel;
use Modules\AiApp\Base\BaseModels\BaseModelInterface;
use Modules\AiApp\Helpers\MultiActionResult;
use MongoDB\BSON\UTCDateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Modules\AiApp\Helpers\ActionResult;
use Ramsey\Uuid\Uuid;

abstract class BaseBox
{
    public static $maxLimit = 1000;
    protected $with = [];
    protected $sortableFields = ['id', 'created_at', 'updated_at'];

    abstract public static function className();

    public function checkLimit(Builder $query)
    {
        $limit = $query->getQuery()->limit;
        if ($limit && ($limit > self::$maxLimit)) {
            throw new \Exception('Invalid limit: '.$limit.'. Maximum: '.self::$maxLimit);
        }
    }

    public static function getWith()
    {
        static $with = null;
        if (is_null($with)) {
            $class = get_called_class();
            $with = (new $class)->with;
        }

        return $with;
    }

    /**
     * @return Builder
     */
    public function newQuery()
    {
        return static::query();
    }

    /**
     * @return BaseModel
     */
    public function newInstance($attrs = [])
    {
        return static::instance($attrs);
    }

    /**
     * @return Builder
     */
    public static function query()
    {
        $className = static::className();

        return $className::query();
    }

    /**
     * @return BaseModel
     */
    public static function instance($attrs = [])
    {
        $className = static::className();

        return new $className($attrs);
    }

    public function save(BaseModelInterface $model)
    {
        return $model->save();
    }

    public function saveAndFresh(BaseModelInterface $model)
    {
        $model->save();
        $model = $this->fresh($model);

        return $model;
    }

    public function update(BaseModelInterface $model, $attributes = [])
    {
        return $model->update($attributes);
    }

    public function findWrap($id)
    {
        $result = new ActionResult();
        $model = $this->find($id);
        if ($model) {
            $result->setData('item', $model);
            $result->allOK();
        } else {
            $result->error404();
        }

        return $result;
    }

    public function find($id)
    {
        return $this->newQuery()->find($id);
    }

    public function getById($id)
    {
        return $this->newQuery()
                    ->with($this->with)
                    ->where(self::getIdColumn($id), $id)
                    ->first();
    }

    public function getByCode($code)
    {
        return $this->newQuery()
                    ->with($this->with)
                    ->where('code', strval($code))
                    ->first();
    }

    public function getByPackageCode($code)
    {
        return $this->newQuery()
                    ->with($this->with)
                    ->where('package_code', strval($code))
                    ->first();
    }

    public function getByIds(array $ids)
    {
        return $this->newQuery()
                    ->with($this->with)
                    ->whereIn(self::getIdColumn($ids[0] ?? 0), $ids)
                    ->get();
    }

    public function getUserItemById($userId, $id)
    {
        if (empty($userId)) {
            throw new \Exception('Invalid UserId');
        }

        $item = $this->newQuery()
                     ->with($this->with)
                     ->where('user_id', $userId)
                     ->where(self::getIdColumn($id), $id)
                     ->first();

        return $item;
    }

    protected static function getIdColumn($id)
    {
        return is_string($id) && Uuid::isValid($id) ? 'uuid' : 'id';
    }

    public function deleteItemById($id): ActionResult
    {
        $result = new ActionResult();
        $item = $this->getById($id);

        if ($item) {
            $item->delete();
            $result->allOK();
        } else {
            $result->error404();
        }

        return $result;
    }

    public function deleteUserItemById($userId, $id, $alwaysOk = false): ActionResult
    {
        $result = new ActionResult();
        $item = $this->getUserItemById($userId, $id);
        if ($item) {
            $item->delete();
            $result->allOK();
        } else {
            $result->error404();
        }

        if ($alwaysOk) {
            $result->allOK();
        }

        return $result;
    }

    public function deleteUserItemsByIds($userId, array $ids, $alwaysOk = false): ActionResult
    {
        $result = new ActionResult();
        $result->allOK();

        foreach ($ids as $id) {
            $res = $this->deleteUserItemById($userId, $id);
            if ($res->error && empty($alwaysOk)) {
                $result = $res;
            }
        }

        return $result;
    }


    public function destroy($ids)
    {
        $model = $this->newQuery()->getModel();

        return $model::destroy($ids);
    }

    /**
     * @param array $attributes
     * @return BaseModelInterface
     */
    public function create($attributes = [])
    {
        return $this->newQuery()->create($attributes);
    }

    public function firstOrCreate(array $attributes, array $values = [])
    {
        return $this->newQuery()->firstOrCreate($attributes, $values);
    }

    public function firstOrNew(array $attributes, array $values = [])
    {
        return $this->newQuery()->firstOrNew($attributes, $values);
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        return $this->newQuery()->updateOrCreate($attributes, $values);
    }

    public function findOrNew($id, $columns = ['*'])
    {
        return $this->newQuery()->findOrNew($id, $columns);
    }

    public function carbonToUTC(Carbon $carbon): UTCDateTime
    {
        return new UTCDateTime($carbon->timestamp * 1000);
    }

    public function fresh(BaseModelInterface $model)
    {
        return $model->fresh($this->with);
    }

    public function findWithTrashed($id)
    {
        return $this->newQuery()->withTrashed()->find($id);
    }

    public function getPagination($limit = 20, array $opts = [])
    {
        return $this->getItemsPagination($opts, $limit);
    }

    public function applyFilterQuery($query, array $filter)
    {
        return $query;
    }

    public function getUserItems($userId, array $opts, $limit = 10, $skip = 0)
    {
        $query = $this->getItemsQuery(intval($userId), $opts)
                      ->limit($limit)
                      ->skip($skip);

        $this->checkLimit($query);

        return $query->get();
    }

    /**
     * @param array $opts
     * @return array
     *
     * @see http://jsonapi.org/format/#fetching-sorting
     */
    protected function buildOrder(array $opts): array
    {
        $res = [];

        if (!empty($opts['sort'])) {
            $ar = explode(',', $opts['sort']);
            foreach ($ar as $field) {
                $field = strval($field);
                if ($field[0] == '-') {
                    $field = substr($field, 1);
                    $order = 'desc';
                } else {
                    $order = 'asc';
                }
                if (in_array($field, $this->sortableFields)) {
                    $res[] = [$field, $order];
                }
            }
        }

        if (empty($res)) {
            $res[] = ['created_at', 'desc'];
        }

        return $res;
    }

    protected function getItemsQuery($userId, array $opts)
    {
        $filter = is_array($opts['filter'] ?? null) ? $opts['filter'] : $opts;
        $order = $this->buildOrder($opts);

        $query = $this->newQuery()
                      ->with($this->with)
                      ->where(function ($query) use ($userId) {
                          if (!is_null($userId)) {
                              $query->where('user_id', $userId);
                          }
                      })
                      ->where(function ($query) use ($filter) {
                          $this->applyFilterQuery($query, $filter);
                      });

        foreach ($order as $orderItem) {
            $query->orderBy($orderItem[0], $orderItem[1]);
        }

        return $query;
    }

    public function getUserItemsPagination($userId, array $opts, $limit = 10, $json = false)
    {
        $query = $this->getItemsQuery(intval($userId), $opts);
        if ($json) {
            $paginator = $query->jsonPaginate($limit);
        } else {
            $paginator = $query->paginate($limit);
        }

        $this->checkLimit($query);

        return $paginator;
    }

    public function getAllRecords($columns = null, array $order = null): Collection
    {
        if (is_null($columns)) {
            $columns = ['*'];
        }

        if (is_null($order)) {
            $order = [['id', 'asc']];
        }

        $query = $this->newQuery()
                      ->with($this->with)
                      ->select($columns);

        foreach ($order as $or) {
            $query->orderBy($or[0], $or[1]);
        }

        return $query->get();
    }

    public function getItemsPagination(array $opts, $limit = 10, $json = false)
    {
        $query = $this->getItemsQuery(null, $opts);

        if ($json) {
            $paginator = $query->jsonPaginate($limit);
        } else {
            $paginator = $query->paginate($limit);
        }

        $this->checkLimit($query);

        return $paginator;
    }

    public function countUserItems($userId)
    {
        return $this->newQuery()->where('user_id', $userId)->count();
    }
}