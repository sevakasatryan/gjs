<?php namespace Modules\AiApp\Base\BaseExceptions;

use \Exception;
use Throwable;

class SafeException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message instanceof Exception) {

            $class = class_basename($message);
            $code = $message->getCode();

            $message = sprintf('Exception class: %s (code: %s)', $class, $code);
        }

        parent::__construct($message, $code, $previous);
    }
}
