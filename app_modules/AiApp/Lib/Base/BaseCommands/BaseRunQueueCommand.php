<?php namespace Modules\AiApp\Base\BaseCommands;

use File;

abstract class BaseRunQueueCommand extends BaseCommand
{
    const COMMAND_NAME = 'x:run-queue';

    protected $signature = self::COMMAND_NAME;
    protected $description = 'Run queue listener (runs automatically via CRON)';

    protected function getQueueConfig()
    {
        return [
            [
                'queues'  => ['high'],
                'timeout' => 60,
                'memory'  => 100,
                'tries'   => 10,
            ],
            [
                'queues'  => ['high', 'medium'],
                'timeout' => 60,
                'memory'  => 100,
                'tries'   => 10,
            ],
            [
                'queues'  => ['high', 'medium', 'low', 'emails', 'default'],
                'timeout' => 60,
                'memory'  => 100,
                'tries'   => 10,
            ],
            [
                'queues'  => ['high', 'medium', 'low', 'emails', 'default'],
                'timeout' => 60,
                'memory'  => 100,
                'tries'   => 10,
            ],
        ];
    }

    public function handle()
    {
        $this->checkLocks();

        $conf = $this->getQueueConfig();
        foreach ($conf as $idx => $queueConf) {
            $this->runQueue($idx + 1, $queueConf);
        }
    }

    /**
     * Check/create locks folder
     */
    protected function checkLocks()
    {
        $locksDir = storage_path('locks');
        if (!file_exists($locksDir)) {
            File::makeDirectory($locksDir);
            file_put_contents($locksDir.'/.gitignore', '*'.PHP_EOL.'!.gitignore');
        }
    }

    protected function runQueue(int $id, array $queueConf)
    {
        $queues = $queueConf['queues'];
        array_unshift($queues, app_code());

        $runCommand = false;
        $monitorFilePath = storage_path('locks/run_queue_'.intval($id).'.pid');

        if (file_exists($monitorFilePath)) {
            $pid = file_get_contents($monitorFilePath);
            $result = exec("ps -p $pid --no-heading | awk '{print $1}'");

            if ($result == '') {
                $runCommand = true;
            }
        } else {
            $runCommand = true;
        }

        if ($runCommand) {
            $cm = sprintf(
                'queue:listen --queue=%s --timeout=%d --memory=%d --tries=%d',
                join(',', $queues), $queueConf['timeout'], $queueConf['memory'], $queueConf['tries']
            );
            $command = 'php '.base_path('artisan').' '.$cm.' > /dev/null & echo $!';

            $number = exec($command);
            file_put_contents($monitorFilePath, $number);
        }
    }
}
