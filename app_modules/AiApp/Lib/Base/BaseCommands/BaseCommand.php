<?php namespace Modules\AiApp\Base\BaseCommands;

use Modules\AiApp\Helpers\ActionResult;
use Illuminate\Console\Command;
use Carbon\Carbon;

abstract class BaseCommand extends Command
{
    public function printActionResult(ActionResult $actionResult)
    {
        $prefix = '['.Carbon::now()->toDateTimeString().'] ';
        $type = $actionResult->type == 'success' ? 'info' : 'error';
        $this->line($prefix.$actionResult->message, $type);

        foreach ((array)$actionResult->getData('mylog') as $log) {
            $this->line($prefix.$log['message'], $log['type']);
        }

        var_dump($actionResult->getData());
    }
}
