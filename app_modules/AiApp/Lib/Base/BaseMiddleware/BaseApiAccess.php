<?php namespace Modules\AiApp\Base\BaseMiddleware;

use Modules\AiApp\Helpers\ActionResult;
use Closure, Auth;

class BaseApiAccess
{
    protected function beforeNext()
    {
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string $paramString
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUser = Auth::guard('api')->user();

        if (empty($currentUser)) {
            return $this->unauth();
        }

        if (empty($currentUser->is_active)) {
            return $this->unauth('Inactive User');
        }

        if (empty($request->get('user_id'))) {
            $request->merge(['user_id' => $currentUser->id]);
        }

        if ($request->route('user') == 'me') {
            $request->route()->setParameter('user', $currentUser->id);
        }

        /**
         * Check Access
         */
        if (empty($currentUser->isAdmin())) {
            $routeUserId = $request->route('user');
            $requestUserId = $request->get('user_id');

            if ($routeUserId && ($routeUserId != $currentUser->id)) {
                return $this->unauth('Unauthorized Access (RTU)');
            }

            if ($requestUserId && ($requestUserId != $currentUser->id)) {
                return $this->unauth('Unauthorized Access (RQU)');
            }
        }

        $this->beforeNext();

        return $next($request);
    }

    protected function unauth($msg = 'Unauthorized Request')
    {
        $answer = ActionResult::getInstance()->errorUnauthorized($msg);

        return response()->json($answer);
    }
}