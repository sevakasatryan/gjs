<?php namespace Modules\AiApp\Base\BaseControllers;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Pagination\Paginator;
use Modules\AiApp\Helpers\ActionResult;
use App;

abstract class BaseController extends Controller
{
    use ValidatesRequests, DispatchesJobs;

    protected function failIfEmpty($smth)
    {
        if (empty($smth)) {
            App::abort(404);
        }
    }

    protected function failIf($smth)
    {
        if (!empty($smth)) {
            App::abort(404);
        }
    }

    protected function apiResponse(ActionResult $answer)
    {
        return response()->json($answer->toArray());
    }

    protected function answerData($data, $dataKey, $allowEmpty = false): ActionResult
    {
        $answer = new ActionResult();
        if ($data || $allowEmpty) {
            $answer->setData(strval($dataKey), $data);
            $answer->allOK();
        } else {
            $answer->error404();
        }

        return $answer;
    }

    protected function answerOneItem($item, $allowEmpty = false): ActionResult
    {
        return $this->answerData($item, 'item', $allowEmpty);
    }

    protected function answerOk()
    {
        return ActionResult::getInstance()->allOK();
    }

    protected function answerManyItems($items): ActionResult
    {
        $answer = new ActionResult();
        $answer->allOK();

        if ($items instanceof Paginator) {
            $answer->setPagination($items);
        } else {
            if ($items instanceof Collection) {
                $items = $items->toArray();
            } elseif (is_object($items)) {
                if (method_exists($items, 'toArray')) {
                    $items = $items->toArray();
                }
            }
            $answer->setData('items', (array)$items);
        }

        return $answer;
    }
}
