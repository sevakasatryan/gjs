<?php namespace Modules\AiApp\Base\BaseControllers;

use Auth;

abstract class BaseAdminController extends BaseController
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            view()->share([
                'user' => $this->user,
            ]);

            return $next($request);
        });
    }
}