<?php namespace Modules\AiApp\Base\BaseControllers;

use Modules\AiApp\Base\BaseModels\BaseUser;
use Modules\AiApp\Helpers\ActionResult;
use Modules\AiApp\Base\BaseRequests\BaseApiRequest;
use Modules\AiApp\Base\BaseRequests\BaseRequest;
use Modules\AiApp\Base\BaseExceptions\SafeException;
use Modules\AiApp\Helpers\PaginationParams;
use Illuminate\Http\Request;
use Auth, App, AppLog;

abstract class BaseApiController extends BaseController
{
    protected function doNotCheckAuth()
    {
        return false;
    }

    public function callAction($method, $parameters)
    {
        $request = $this->getRequest();

        try {
            if (empty($this->doNotCheckAuth())) {
                /**
                 * Check ApiAccess middleware
                 *
                 * ["api", "App\Http\Middleware\ApiAccess"]
                 */
                $middleWares = (array)$request->route()->computedMiddleware;

                $ok = false;

                if (count($middleWares) >= 2) {
                    foreach ($middleWares as $m) {
                        if (strpos($m, 'ApiAccess') !== false) {
                            $ok = true;
                            break;
                        }
                    }
                }

                if (empty($ok)) {
                    throw new SafeException('Invalid server-side ApiAccess configuration');
                }
            }

            /**
             * call method
             */
            $jsonResponse = parent::callAction($method, $parameters);
        } catch (\Exception $e) {
            AppLog::baseControllerError($e);

            if ($e instanceof SafeException) {
                $errorAnswer = ActionResult::getInstance()->error($e->getMessage());
            } else {
                $errorAnswer = ActionResult::getInstance()->errorRequest();
            }
            $jsonResponse = $this->apiResponse($errorAnswer);
        }

        return $jsonResponse;
    }

    protected function getUser(): BaseUser
    {
        $user = Auth::guard('api')->user();
        if ($user instanceof BaseUser) {
            return $user;
        } else {
            throw new SafeException('Invalid Access (NO USER)');
        }
    }

    protected function getUserId()
    {
        return $this->getUser()->id;
    }

    protected function adminOnly()
    {
        $isAdmin = $this->getUser()->isAdmin();
        if (empty($isAdmin)) {
            throw new SafeException('Admin-only Access');
        }
    }

    protected function getRequest(): Request
    {
        if ($this->doNotCheckAuth()) {
            $request = App::make(BaseRequest::class);
        } else {
            $request = App::make(BaseApiRequest::class);
        }

        return $request;
    }


    protected function getPaginationParams($default = 20, $maxLimit = 1000): PaginationParams
    {
        $rp = $this->getRequest()->get('page');
        $pp = new PaginationParams($rp, $default, $maxLimit);

        return $pp;
    }
}