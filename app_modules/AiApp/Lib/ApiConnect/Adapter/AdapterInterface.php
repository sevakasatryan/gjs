<?php namespace Modules\AiApp\ApiConnect\Adapter;

interface AdapterInterface
{
    public function __construct(array $config = []);

    public function makeRequest($method, $path, $query = [], array $params = []): array;

    public function makeRequestWithToken($apiToken, $method, $path, $query = [], array $params = []): array;
}