<?php namespace Modules\AiApp\ApiConnect\Adapter;

abstract class AbstractAdapter implements AdapterInterface
{
    const AUTH_BASIC  = 'basic';
    const AUTH_BEARER = 'bearer';
    const AUTH_QUERY  = 'query';

    const METHOD_GET    = 'GET';
    const METHOD_POST   = 'POST';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PUT    = 'PUT';

    protected $apiToken, $apiUrl, $apiAuthMode, $basicAuthUser, $basicAuthPass, $timeout;

    public function __construct(array $config = [])
    {
        foreach (['api_url', 'api_token'] as $param) {
            if (empty($config[$param])) {
                throw new \Exception('Missing required parameter: '.$param);
            }
        }

        $this->apiUrl = rtrim($config['api_url'], '/').'/';
        $this->apiToken = $config['api_token'];
        $this->apiAuthMode = $config['api_auth_mode'] ?? self::AUTH_BEARER;
        $this->basicAuthUser = $config['basic_auth_user'] ?? null;
        $this->basicAuthPass = $config['basic_auth_pass'] ?? null;
        $this->timeout = $config['timeout'] ?? 30;

        if ($this->basicAuthUser && ($this->apiAuthMode == self::AUTH_BASIC)) {
            throw new \Exception('Cannot use AUTH_BASIC because basic auth credentials already configured for connection to api endpoint.');
        }
    }

    public function get($path, $params = [])
    {
        return $this->makeRequest(self::METHOD_GET, $path, $params);
    }

    public function delete($path, $params = [])
    {
        return $this->makeRequest(self::METHOD_DELETE, $path, $params);
    }

    public function post($path, $params = [])
    {
        return $this->makeRequest(self::METHOD_POST, $path, [], $params);
    }

    public function makeRequest($method, $path, $query = [], array $formParams = []): array
    {
        return $this->makeRequestWithToken($this->apiToken, $method, $path, $query, $formParams);
    }
}