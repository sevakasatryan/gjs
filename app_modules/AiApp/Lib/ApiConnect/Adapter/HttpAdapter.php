<?php namespace Modules\AiApp\ApiConnect\Adapter;

use GuzzleHttp\Client;
use Illuminate\Http\UploadedFile;

class HttpAdapter extends AbstractAdapter
{
    public function makeRequestWithToken($apiToken, $method, $path, $query = [], array $formParams = []): array
    {
        $method = strtoupper($method);

        $client = new Client([
            'timeout'         => $this->timeout,
            'allow_redirects' => false,
            'verify'          => false,
        ]);

        $ep = $this->apiUrl.$path;

        if ($method == self::METHOD_GET) {
            $query = array_merge($query, $formParams);
            $formParams = [];
        }

        $options = [
            'query'     => $query,
            //'form_params' => $formParams,
            'multipart' => self::flatten($formParams),
            'auth'      => [],
            'headers'   => [],
        ];

        if ($this->apiAuthMode == self::AUTH_BASIC) {
            $options['auth'] = ['', $apiToken];
        } elseif ($this->apiAuthMode == self::AUTH_BEARER) {
            $options['headers']['Authorization'] = "Bearer {$apiToken}";
        } elseif ($this->apiAuthMode == self::AUTH_QUERY) {
            $options['query']['api_token'] = $apiToken;
        }

        if (!empty($this->basicAuthUser)) {
            $options['auth'] = [$this->basicAuthUser, $this->basicAuthPass];
        }

        $res = $client->request($method, $ep, $options);

        //dd($method, $ep, $query, $options, strval($res->getBody()));

        return (array)json_decode(strval($res->getBody()), 1);
    }

    protected static function flatten($formData, $originalKey = '')
    {
        $output = [];

        foreach ($formData as $key => $value) {
            $newKey = $originalKey;

            if (empty($originalKey)) {
                $newKey .= $key;
            } else {
                $newKey .= '['.$key.']';
            }

            if ($value instanceof UploadedFile) {
                $output[] = [
                    'name'      => $newKey,
                    'filename'  => $value->getClientOriginalName(),
                    'Mime-Type' => $value->getMimeType(),
                    'contents'  => fopen($value->getPathname(), "r"),
                ];
            } elseif (is_array($value)) {
                $output = array_merge($output, self::flatten($value, $newKey));
            } else {
                $output[] = [
                    'name'     => $newKey,
                    'contents' => $value,
                ];
            }
        }

        return $output;
    }
}