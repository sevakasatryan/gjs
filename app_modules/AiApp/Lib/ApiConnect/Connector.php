<?php namespace Modules\AiApp\ApiConnect;

use Modules\AiApp\ApiConnect\Adapter\AbstractAdapter;
use Modules\AiApp\ApiConnect\Adapter\AdapterInterface;
use Modules\AiApp\ApiConnect\Adapter\HttpAdapter;
use Modules\AiApp\Helpers\ActionResult;

abstract class Connector
{
    const TOKEN_SYSTEM = 'system';

    protected $config;
    /** @var  HttpAdapter */
    protected $adapter;

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->setupAdapter();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function get($path, $params = [], $apiToken = self::TOKEN_SYSTEM)
    {
        return $this->request(AbstractAdapter::METHOD_GET, $path, $params, $apiToken);
    }

    public function delete($path, $params = [], $apiToken = self::TOKEN_SYSTEM)
    {
        return $this->request(AbstractAdapter::METHOD_DELETE, $path, $params, $apiToken);
    }

    public function post($path, $params = [], $apiToken = self::TOKEN_SYSTEM)
    {
        return $this->request(AbstractAdapter::METHOD_POST, $path, $params, $apiToken);
    }

    public function put($path, $params = [], $apiToken = self::TOKEN_SYSTEM)
    {
        $params['_method'] = AbstractAdapter::METHOD_PUT;

        return $this->request(AbstractAdapter::METHOD_POST, $path, $params, $apiToken);
    }

    //------------------------------------------------------------------------------------------------------------------

    protected function setupAdapter(): AdapterInterface
    {
        if ($this->config && !empty($this->config['adapter'])) {
            if ($this->config['adapter'] == 'http') {
                $this->adapter = new HttpAdapter($this->config);
            } else {
                throw new \Exception('Adapter "%s" is not implemented yet', $this->config['adapter']);
            }
        } else {
            throw new \Exception('Invalid api config');
        }

        return $this->adapter;
    }

    protected function request($method, $path, $params = [], $apiToken): ActionResult
    {
        $path = trim(strtolower($path), '/');

        $this->addLog('_');
        $this->addLog(sprintf('%s : %s', $method, $path));
        $this->addLog($params);

        try {
            if ($apiToken == self::TOKEN_SYSTEM) {
                $answer = $this->adapter->makeRequest($method, $path, [], $params);
            } else {
                $answer = $this->adapter->makeRequestWithToken($apiToken, $method, $path, [], $params);
            }
            $result = ActionResult::fromArray($answer);
        } catch (\Exception $e) {
            $result = ActionResult::getInstance()->error($e->getMessage());
            $this->exception($e);
        }

        $this->addLog('[RESULT]: '.$result);

        return $result;
    }

    private function addLog($msg)
    {
        if ($this->config['log'] ?? false) {
            $this->log($msg);
        }
    }

    protected function log($msg)
    {
        // Extend this
    }

    protected function exception(\Exception $e)
    {
        // Extend this
    }
}