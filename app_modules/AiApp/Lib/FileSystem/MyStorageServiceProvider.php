<?php namespace Modules\AiApp\FileSystem;

use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Modules\AiApp\FileSystem\Adapter\MyAzureAdapter;
use Modules\AiApp\FileSystem\Adapter\MyLocalAdapter;
use MicrosoftAzure\Storage\Common\ServicesBuilder;
use Illuminate\Support\Arr;
use Storage;

class MyStorageServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Local + ttl symlinks
         */
        Storage::extend('my-local', function ($app, $config) {
            $opts = ['public_disk', 'ttl_folder', 'ttl_minutes'];
            $this->checkConfig($opts, $config);

            $permissions = isset($config['permissions']) ? $config['permissions'] : [];
            $links = Arr::get($config, 'links') === 'skip' ?
                MyLocalAdapter::SKIP_LINKS :
                MyLocalAdapter::DISALLOW_LINKS;

            $adapter = new MyLocalAdapter($config['root'], LOCK_EX, $links, $permissions);
            $adapter->setConfig($config);

            return new Filesystem($adapter, $config);
        });

        /**
         * Azure
         */
        Storage::extend('my-azure', function ($app, $config) {
            $opts = ['ttl_minutes'];
            $this->checkConfig($opts, $config);

            $endpoint = sprintf(
                'DefaultEndpointsProtocol=https;AccountName=%s;AccountKey=%s',
                $config['name'],
                $config['key']
            );
            $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($endpoint);

            $adapter = new MyAzureAdapter($blobRestProxy, $config['container']);
            $adapter->setConfig($config);

            return new Filesystem($adapter, $config);
        });
    }

    protected function checkConfig(array $opts, $config)
    {
        foreach ($opts as $opt) {
            if (empty($config[$opt])) {
                throw new \Exception('Config option "'.$opt.'" need to be defined!');
            }
        }
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}