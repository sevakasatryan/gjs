<?php namespace Modules\AiApp\FileSystem\Adapter;

use Cache;

trait MyAdapterTrait
{
    protected $config;

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function getPermanentUrl($path)
    {
        return $this->buildPermanentUrl($path);
    }

    public function getTtlUrl($path, $expireMinutes = null): string
    {
        $ttl = $this->getTtl($expireMinutes);
        $preTtl = $this->getPreTtl($expireMinutes);

        if ($this->config['use_cache'] ?? false) {
            $url = Cache::tags([$this->getCacheTag(), md5($path)])
                        ->remember($path, $preTtl, function () use ($path, $ttl) {
                            return $this->buildTtlUrl($path, $ttl);
                        });
        } else {
            $url = $this->buildTtlUrl($path, $ttl);
        }

        return $url;
    }

    protected function getCacheTag(): string
    {
        return $this->config['url_cache_tag'] ?? 'storage.urls';
    }

    protected function getPreTtl($minutes = null): int
    {
        $ttl = $this->getTtl($minutes);
        $preTtl = max(1, intval($ttl * 0.8));

        return $preTtl;
    }

    protected function getTtl($minutes = null): int
    {
        $ttl = intval($minutes ? $minutes : $this->config['ttl_minutes']);
        $ttl = max(1, $ttl);

        return $ttl;
    }

    protected function getPostTtl($minutes = null): int
    {
        $ttl = $this->getTtl($minutes);
        $postTtl = max(1, intval($ttl * 1.2));

        return $postTtl;
    }


    public function flushCache()
    {
        Cache::tags($this->getCacheTag())->flush();
    }
}