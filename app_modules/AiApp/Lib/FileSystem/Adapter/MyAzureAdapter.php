<?php namespace Modules\AiApp\FileSystem\Adapter;

use League\Flysystem\Azure\AzureAdapter;
use Carbon\Carbon;

class MyAzureAdapter extends AzureAdapter
{
    use MyAdapterTrait;

    protected function buildTtlUrl($path, int $expireMinutes)
    {
        $expiresTs = Carbon::now()->addMinutes($expireMinutes)->getTimestamp();
        $expires = gmdate("Y-m-d\TH:i:s\Z", $expiresTs);
        $sig = $this->getSASForBlob($this->config['name'], $this->container, $path, "b", "r", $expires, $this->config['key']);
        $url = $this->getBlobUrl($this->config['name'], $this->container, $path, "b", "r", $expires, $sig);

        return (string)$url;
    }

    protected function getSASForBlob($accountName, $container, $blob, $resourceType, $permissions, $expiry, $key)
    {
        /* Create the signature */
        $_arraysign = array();
        $_arraysign[] = $permissions;
        $_arraysign[] = '';
        $_arraysign[] = $expiry;
        $_arraysign[] = '/'.$accountName.'/'.$container.'/'.$blob;
        $_arraysign[] = '';
        $_arraysign[] = "2014-02-14"; //the API version is now required
        $_arraysign[] = '';
        $_arraysign[] = '';
        $_arraysign[] = '';
        $_arraysign[] = '';
        $_arraysign[] = '';

        $_str2sign = implode("\n", $_arraysign);

        return base64_encode(
            hash_hmac('sha256', urldecode(utf8_encode($_str2sign)), base64_decode($key), true)
        );
    }

    protected function getBlobUrl($accountName, $container, $blob, $resourceType, $permissions, $expiry, $_signature)
    {
        /* Create the signed query part */
        $_parts = array();
        $_parts[] = (!empty($expiry)) ? 'se='.urlencode($expiry) : '';
        $_parts[] = 'sr='.$resourceType;
        $_parts[] = (!empty($permissions)) ? 'sp='.$permissions : '';
        $_parts[] = 'sig='.urlencode($_signature);
        $_parts[] = 'sv=2014-02-14';

        /* Create the signed blob URL */
        $_url = 'https://'
            .$accountName.'.blob.core.windows.net/'
            .$container.'/'
            .$blob.'?'
            .implode('&', $_parts);

        return $_url;
    }
}