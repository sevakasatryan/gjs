<?php namespace Modules\AiApp\FileSystem\Adapter;

use Modules\AiApp\Helpers\ActionResult;
use League\Flysystem\Adapter\Local;
use Carbon\Carbon;
use Storage;

class MyLocalAdapter extends Local
{
    use MyAdapterTrait;

    protected function buildPermanentUrl($path): string
    {
        $fullPath = $this->getPathPrefix().$path;

        if (is_readable($fullPath)) {

            $extension = pathinfo($fullPath, PATHINFO_EXTENSION);
            $permanent = $this->config['permanent_folder'] ?? 'permanent';
            $fn = md5($fullPath).'.'.$extension;
            $publicFilePath = sprintf('%s/%s', $permanent, $fn);
            $publicStorage = $this->getPublicStorage();

            try {
                $publicStorage->makeDirectory(dirname($publicFilePath));
            } catch (\Exception $e) {

            }

            $prefix = $publicStorage->getDriver()->getAdapter()->getPathPrefix();
            if (!file_exists($prefix.$publicFilePath)) {
                @symlink($fullPath, $prefix.$publicFilePath);
            }
            $url = $publicStorage->url($publicFilePath);
        } else {
            $url = '';
        }

        return $url;
    }

    protected function buildTtlUrl($path, int $expireMinutes): string
    {
        $fullPath = $this->getPathPrefix().$path;

        if (is_readable($fullPath)) {
            $extension = pathinfo($fullPath, PATHINFO_EXTENSION);
            $folder = intval(time() / 60 * $expireMinutes); // each [TIME] (minute) - new value
            $fn = md5($fullPath).'.'.$extension;
            $publicFilePath = sprintf('%s/%s/%s', $this->config['ttl_folder'], $folder, $fn);
            $publicStorage = $this->getPublicStorage();

            try {
                $publicStorage->makeDirectory(dirname($publicFilePath));
            } catch (\Exception $e) {

            }

            $prefix = $publicStorage->getDriver()->getAdapter()->getPathPrefix();
            if (!file_exists($prefix.$publicFilePath)) {
                @symlink($fullPath, $prefix.$publicFilePath);
            }
            $url = $publicStorage->url($publicFilePath);
        } else {
            $url = '';
        }

        return $url;
    }

    protected function getPublicStorage()
    {
        return Storage::disk($this->config['public_disk']);
    }

    public function removeExpiredSymlinks(): ActionResult
    {
        $result = ActionResult::getInstance()->forLogs(__METHOD__);

        $minutesAgo = $this->getPostTtl();

        $publicStorage = $this->getPublicStorage();

        $prefix = $publicStorage->getDriver()->getAdapter()->getPathPrefix();
        $ttlFilesDirName = sprintf('%s/%s/', rtrim($prefix, '/'), $this->config['ttl_folder']);

        if (file_exists($ttlFilesDirName)) {
            $result->mylogOk(sprintf('-> Find files older and newer than %s minutes:', $minutesAgo));
            $cmd = sprintf('find %s -type l -mmin +$((%s)) -print -delete', $ttlFilesDirName, $minutesAgo);
            exec($cmd, $output);

            $result->mylogOk($cmd);
            $result->mylogOk($output);
            unset($output);

            $result->mylogOk('-> Remove empty folders:');
            $cmd = sprintf('find %s -mindepth 1 -type d -empty -print -delete', $ttlFilesDirName, $minutesAgo);
            exec($cmd, $output);
            $result->mylogOk($cmd);
            $result->mylogOk($output);
            unset($output);
        } else {
            $result->mylogError('TTL folder does not exists.');
        }

        return $result;
    }
}