<?php namespace Modules\AiApp\Avatar;

use Modules\AiApp\Helpers\ActionResult;
use Illuminate\Http\UploadedFile;
use App, Validator;
use Modules\AiApp\Media\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

trait AvatarServiceTrait
{
    public function uploadTempAvatarById($itemId, UploadedFile $file, $set = false, array $customProps = [])
    {
        $item = $this->getBox()->getById($itemId);
        $result = $this->uploadTempAvatar($item, $file, $set, $customProps);

        return $result;
    }

    public function uploadTempAvatar(HasMedia $item, UploadedFile $file, $set = false, array $customProps = [])
    {
        $result = new ActionResult();
        $rules = ['file' => 'file|required|mimes:jpg,jpeg,bmp,png,tiff'];
        $validator = Validator::make(['file' => $file], $rules);
        if ($validator->passes()) {
            $item->clearMediaCollection('tmp');
            $media = $item->addMedia($file)
                          ->preservingOriginal()
                          ->withCustomProperties($customProps)
                          ->toMediaCollection('tmp');

            $thumb = $media->getUrl('thumb');
            $result->setData('thumb', $thumb);

            if ($set) {
                $this->checkAvatar($item, 'set');
            }

            $result->allOK();
        } else {
            $result->errorValidation($validator->messages()->toArray());
        }

        return $result;
    }

    public function checkAvatar(HasMedia $item, $mode = false)
    {
        if ($mode === 'remove') {
            $item->clearMediaCollection('src');
        } elseif ($mode) {
            /** @var Media $tmpAva */
            $tmpAva = $item->getFirstMedia('tmp');
            if ($tmpAva) {
                $item->clearMediaCollection('src');
                $item->addMedia($tmpAva->getFullPath())
                     ->withCustomProperties((array)$tmpAva->custom_properties)
                     ->preservingOriginal()
                     ->toMediaCollection('src');
            }
        }
        $item->clearMediaCollection('tmp');
    }
}
