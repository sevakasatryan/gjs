<?php

use Carbon\Carbon;

function is_access_by_ip()
{
    $isIp = false;
    if (!empty($_SERVER['HTTP_HOST'])) {
        $isIp = filter_var($_SERVER['HTTP_HOST'], FILTER_VALIDATE_IP);
    }

    return $isIp;
}

function get_app_domain()
{
    $parts = parse_url(config('app.url'));
    $domain = $parts['host'] ?? '';

    return $domain;
}

function get_domain()
{
    if (empty($_SERVER['HTTP_HOST'])) {
        $domain = get_app_domain();
    } else {
        if (is_access_by_ip()) {
            $domain = $_SERVER['HTTP_HOST'];
        } else {
            $parts = explode('.', $_SERVER['HTTP_HOST']);
            $parts = array_reverse($parts);
            $domain = "$parts[1].$parts[0]";
        }
    }

    return $domain;
}

function is_production()
{
    return App::environment('prod');
}

function is_dev()
{
    return App::environment('dev');
}

function is_local()
{
    return App::environment('local');
}

function is_debug()
{
    return config('app.debug');
}

function not_production()
{
    return !is_production();
}

function get_last_commit_date()
{
    exec('git log -1 --pretty=format:%ct', $output, $ret);
    $ts = intval($output[0]);

    return Carbon::createFromTimestamp($ts)->format(config('settings.date_format.default_time'));
}

function sanitize($str, $leaveSpace = false)
{
    $str = strip_tags($str);
    $str = preg_replace('/[\r\n\t ]+/', ' ', $str);
    $str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
    $str = strtolower($str);
    $str = html_entity_decode($str, ENT_QUOTES, "utf-8");
    $str = htmlentities($str, ENT_QUOTES, "utf-8");
    $str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
    $str = str_replace('%', '-', $str);

    if (empty($leaveSpace)) {
        $str = str_replace(' ', '-', $str);
    }

    return $str;
}

function get_extension($path)
{
    $path_parts = pathinfo($path);

    return $path_parts['extension'];
}

function is_timestamp($val)
{
    return ((string)(int)$val === $val) && ($val <= PHP_INT_MAX) && ($val >= ~PHP_INT_MAX);
}

function parse_date($val)
{
    $date = null;

    if ($val) {
        if (is_timestamp($val)) {
            $date = Carbon::createFromTimestamp($val);
        } else {
            $date = Carbon::parse($val);
        }
    }

    return $date;
}

function get_build_version()
{
    static $countCommits = null;
    if (is_null($countCommits)) {
        if (env('APP_DEBUG', false)) {
            $countCommits = time();
        } else {
            $cmd = "git rev-list --all --count";
            $countCommits = exec($cmd);
        }
    }

    return sprintf('%s.%s', 1, $countCommits);
}

function get_first_bytes($filePath, $length = 10, $offset = 0)
{
    $fp = fopen($filePath, 'r');
    $data = '';
    if ($fp) {
        fseek($fp, (int)$offset);
        $data = fread($fp, (int)$length);
        fclose($fp);
    }

    return $data;
}

function array_arrvals(array $array): array
{
    if (is_array($array)) {
        foreach ($array as $k => $val) {
            if (is_object($val)) {
                if ($val instanceof \Carbon\Carbon) {
                    $val = $val->toW3cString();
                } elseif (method_exists($val, 'toArray')) {
                    $val = $val->toArray();
                } else {
                    $val = (array)$val;
                }
            }

            if (is_array($val)) {
                $val = array_arrvals($val);
            }

            $array[$k] = $val;
        }
    }

    return $array;
}

function get_pdf_dimensions($path, $box = 'MediaBox')
{
    //$box can be set to BleedBox, CropBox or MediaBox
    $stream = new SplFileObject($path);
    $result = false;

    $num = "[0-9\.]{1,}";

    $line = 0;
    while (!$stream->eof() && $line < 100) {
        $line++;
        $str = $stream->fgets();
        if (preg_match("/{$box} \[$num $num ($num) ($num)\]/", $str, $matches)) {
            $result['width'] = $matches[1];
            $result['height'] = $matches[2];
            break;
        }
    }
    $stream = null;

    return $result;
}

function get_string_between($string, $start, $end, $ignoreCase = false)
{
    $result = '';

    if ($start == '') {
        $startPos = 0;
    } else {
        $startPos = $ignoreCase ? mb_stripos($string, $start) : mb_strpos($string, $start);
    }

    if ($startPos !== false) {
        $startPos += mb_strlen($start);

        if ($end == '') {
            $endPos = mb_strlen($string);
        } else {
            $endPos = $ignoreCase ? mb_stripos($string, $end, $startPos) : mb_strpos($string, $end, $startPos);
        }
        if ($endPos !== false) {
            $result = mb_substr($string, $startPos, $endPos - $startPos);
        }
    }

    return $result;
}

function filter_bad_keywords($keywords)
{
    $stopWords = [
        'i', 'a', 'about', 'an', 'and', 'are', 'as', 'at', 'be', 'by', 'com', 'de', 'en', 'for', 'from', 'how', 'in',
        'is', 'it', 'la', 'of', 'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 'when', 'where', 'who', 'will',
        'with', 'und', 'the', 'our', 'any',
    ];

    $matchWords = array_filter((array)$keywords, function ($item) use ($stopWords) {
        $ok = !($item == '' || in_array($item, $stopWords) || mb_strlen($item) <= 2 || is_numeric($item));
        $ok = $ok && !preg_match('/^[\£\d\.\,]+$/', $item);
        $ok = $ok && preg_match('/[\pL]/i', $item);

        return $ok;
    });

    return $matchWords;
}

function extract_keywords($string, int $limit = 1000): array
{
    $string = preg_replace('/\s+/', ' ', $string);
    $string = mb_strtolower(trim($string));
    mb_internal_encoding('UTF-8');
    $string = preg_replace('/[\pP] /u', ' ', $string);
    $string = preg_replace('/[^\w\s\.\,\@\#\$\%\&\*\+\£]/u', ' ', $string);
    $matchWords = filter_bad_keywords(explode(' ', $string));
    $matchWords = sort_array_by_word_count($matchWords);

    return array_slice($matchWords, 0, $limit);
}

function sort_array_by_word_count($keywords)
{
    $wordCountArr = array_count_values($keywords);
    arsort($wordCountArr);
    $keywords = array_keys($wordCountArr);
    usort($keywords, function ($a, $b) {
        return preg_match('/\d/', $a) <=> preg_match('/\d/', $b);
    });

    return $keywords;
}

function is_cli()
{
    return php_sapi_name() == "cli";
}

if (!function_exists('mb_ucwords')) {
    function mb_ucwords($str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
}

//----------------------------------------------------------------------------------------------------------------------
// DUMP
if (!function_exists('d')) {
    function d(...$args)
    {
        $args[] = str_repeat('-', 80);

        return call_user_func_array('dump', $args);
    }
}

function lcd(...$args)
{
    if (is_cli() && !is_production()) {
        return d($args);
    }
}

if (!function_exists('ddd')) {
    function ddd(...$args)
    {
        var_dump(...$args);
        die();
    }
}

function mydump($arg)
{
    echo '<pre>';
    print_r($arg);
    echo '</pre>';
}

//----------------------------------------------------------------------------------------------------------------------

function to_numeric($val)
{
    return preg_replace('/[^\d\.\,]/', '', $val);
}

// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function file_upload_max_size()
{
    static $max_size = -1;

    if ($max_size < 0) {
        // Start with post_max_size.
        $max_size = parse_size(ini_get('post_max_size'));

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = parse_size(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }
    }

    return $max_size;
}

function parse_size($size)
{
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
        // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    } else {
        return round($size);
    }
}

function format_bytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision).' '.$suffixes[floor($base)];
}

function pipe_streams($in, $out)
{
    $size = 0;
    while (!feof($in)) {
        $size += fwrite($out, fread($in, 1000000));
    }

    return $size;
}

/**
 *  returns SQL with values in it
 */
function get_sql(\Illuminate\Database\Eloquent\Builder $query)
{
    $replace = function ($sql, $bindings) {
        $needle = '?';
        foreach ($bindings as $replace) {
            $pos = strpos($sql, $needle);
            if ($pos !== false) {
                if (gettype($replace) === "string" || gettype($replace) === "object") {
                    $replace = ' "'.addslashes($replace).'" ';
                }
                $sql = substr_replace($sql, $replace, $pos, strlen($needle));
            }
        }

        return $sql;
    };
    $sql = $replace($query->toSql(), $query->getBindings());

    return $sql;
}

function flatten(array $array)
{
    $return = array();
    array_walk_recursive($array, function ($a) use (&$return) {
        $return[] = $a;
    });

    return $return;
}

function smart_hash(array $data)
{
    $in = [];
    ksort($data);
    foreach ($data as $k => $datum) {
        if (is_array($datum)) {
            $v = smart_hash($datum);
        } elseif (is_object($datum)) {
            if (method_exists($datum, 'toArray')) {
                $v = smart_hash($datum->toArray());
            } else {
                $v = smart_hash((array)$datum);
            }
        } else {
            $v = trim(strval($datum));
        }

        $in[strval($k)] = $v;
    }

    $h = json_encode($in);
    if (false === $h) {
        throw new \Exception('Error hash calculating');
    }
    $hash = sha1($h);

    return $hash;
}

function http_local_url($url)
{
    if (is_local() && preg_match('/(\.loc|example\.com)/', $url)) {
        $url = str_replace('https://', 'http://', $url);
    }

    return $url;
}

if (!function_exists('millis')) {
    function millis()
    {
        return round(microtime(true) * 1000);
    }
}

function stringboolval($stringValue)
{
    $stringValue = strtolower($stringValue);

    if (in_array($stringValue, ['false', 'null', 'none'])) {
        $stringValue = 0;
    }

    return boolval($stringValue);
}

function null_to_empty($array)
{
    if (is_array($array)) {
        foreach ($array as $key => &$val) {
            if (is_array($val)) {
                $val = null_to_empty($val);
            } elseif (is_null($val)) {
                $val = '';
            }
        }
        unset($val);
    }

    return $array;
}

if (!function_exists('app_code')) {
    function app_code()
    {
        $name = sprintf('%s-%s', config('app.name'), config('app.env'));

        return strtoupper(str_slug($name));
    }
}

function absolute_size($size, $baseSize = null)
{
    $result = null;
    if (preg_match('|^([\d\.]+)%$|', $size, $m)) {
        $percent = $m[1];
        if (($percent >= 0 && $percent <= 100) && $baseSize) {
            $result = $percent / 100 * $baseSize;
        }
    } elseif (preg_match('|^[\d]+$|', $size)) {
        $result = intval($size);
    } else {
        $result = floatval($size);
    }

    if (is_null($result)) {
        throw new \Exception('Invalid incoming value');
    }

    return $result;
}


