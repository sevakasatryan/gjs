<?php namespace Modules\AiApp\Convert;

use Illuminate\Support\Collection;

/*
 * TEXT:
 * https://stackoverflow.com/questions/22710099/ffmpeg-create-blank-screen-with-text-video
 * https://stackoverflow.com/questions/17623676/text-on-video-ffmpeg
 *
 * ffmpeg -i INPUT1 -i INPUT2 -i INPUT3 -filter_complex amix=inputs=3:duration=first:dropout_transition=3 OUTPUT
 */

trait ConvertServiceMovieTrait
{
    //------------------------------------------------------------------------------------------------------------------
    // VIDEO

    /**
     * @see https://ffmpeg.org/pipermail/ffmpeg-user/2011-October/002602.html
     *
     * ffmpeg -t 60 -s qcif -f rawvideo -pix_fmt rgb24 -r 25 -i /dev/zero silence.mpeg
     *
     * ffmpeg -s 640x480 -f rawvideo -pix_fmt rgb24 -r 25 -i /dev/zero -i music.mp3 -vcodec libx264 -preset medium -tune stillimage -crf 24 -acodec copy -shortest output.mkv
     *
     * convert -size 320x240 xc:black black.png
     * ffmpeg -loop 1 -i black.png -i music.ogg -acodec copy -shortest output
     *
     */
    public static function createEmptyVideo($outputPath, $duration, array $opts = [])
    {
        if (empty($duration)) {
            throw new \Exception('Invalid duration : '.$duration);
        }

        $opts = array_merge([
            'resolution' => 'vga',
            'fps'        => 25,
        ], $opts);

        $resolution = self::getResolutionXY($opts['resolution']);

        //$cmdTpl = '/usr/bin/ffmpeg -y -t %s -s %s -f rawvideo -pix_fmt rgb24 -r 25 -i /dev/zero -vcodec libx264 -movflags +faststart %s';
        $cmdTpl = '/usr/bin/ffmpeg -y -t %s -s %s -f rawvideo -pix_fmt rgb24 -r %s -i /dev/zero -vcodec libx264 -pix_fmt yuv420p -preset ultrafast -tune stillimage -movflags +faststart %s';
        $cmd = sprintf($cmdTpl, $duration, $resolution, $opts['fps'], escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * x264 presets: placebo, veryslow, slower, slow, medium, fast, faster, veryfast, superfast, ultrafast
     * @see http://x264.janhum.alfahosting.org/fullhelp.txt
     * ffmpeg  -i rabbit.mp4 -vcodec mpeg2video -q:v 2 -s hd720 -acodec libmp3lame rabbit.avi
     */
    public static function convertMovie($filePath, $outputPath, $opts)
    {
        $opts = array_merge([
            'resolution'  => 'vga',
            'fps'         => 25,
            'preset'      => 'fast',
            'acodec'      => 'libmp3lame',
            'crf'         => 20,
            'skip'        => null,
            'duration'    => null,
            'mute'        => false,
            'keep_aspect' => false,
        ], $opts);

        $opts['resolution'] = self::getResolutionXY($opts['resolution']);

        $pre = [
            '-ss' => $opts['skip'],
            '-t'  => $opts['duration'],
        ];

        $centerOpts = [
            '-s'      => empty($opts['resolution']) ? null : $opts['resolution'],
            '-vcodec' => empty($opts['vcodec']) ? self::detectVideoCodec($outputPath) : $opts['vcodec'],
            '-preset' => $opts['preset'],
            '-crf'    => $opts['crf'],
            '-acodec' => $opts['acodec'],
            '-r'      => $opts['fps'],
            0         => $opts['mute'] ? '-an' : null,
        ];

        if (!empty($opts['keep_aspect'] && !empty($opts['resolution']))) {
            /*
             * https://stackoverflow.com/questions/8133242/ffmpeg-resize-down-larger-video-to-fit-desired-size-and-add-padding
             * https://trac.ffmpeg.org/wiki/Scaling%20(resizing)%20with%20ffmpeg
             */
            @list($w, $h) = explode('x', $opts['resolution']);
            if ($w && $h) {
                //$cmd = '-vf "scale=min(iw*TARGET_HEIGHT/ih\,TARGET_WIDTH):min(TARGET_HEIGHT\,ih*TARGET_WIDTH/iw),pad=TARGET_WIDTH:TARGET_HEIGHT:(TARGET_WIDTH-iw)/2:(TARGET_HEIGHT-ih)/2"';
                //$cmd = '-vf "scale=(iw*sar)*min(TARGET_WIDTH/(iw*sar)\,TARGET_HEIGHT/ih)‌​:ih*min(TARGET_WIDTH‌​/(iw*sar)\,TARGET_HEIGHT/ih), pad=TARGET_WIDTH:TARGET_HEIGHT:(TARGET_WIDTH-iw)/2:(TARGET_HEIGHT-ih)/2"';
                $cmd = '-vf "scale=TARGET_WIDTH:TARGET_HEIGHT:force_original_aspect_ratio=decrease,pad=TARGET_WIDTH:TARGET_HEIGHT:x=(TARGET_WIDTH-iw)/2:y=(TARGET_HEIGHT-ih)/2:color=black"';

                $cmd = str_replace(['TARGET_WIDTH', 'TARGET_HEIGHT'], [$w, $h], $cmd);
                $centerOpts[] = $cmd;
            } else {
                throw new \Exception('Invalid resolution value when using keep_aspect flag : '.$opts['resolution'].'; good example template: WIDTHxHEIGHT');
            }
        }

        $cmdTpl = '/usr/bin/ffmpeg -y %s -i %s %s -strict experimental %s';
        $cmd = sprintf($cmdTpl, self::buildOpts($pre), escapeshellarg($filePath), self::buildOpts($centerOpts), escapeshellarg($outputPath));

        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     *  copy codecs only
     */
    public static function videoToMo4($filePath, $outputPath)
    {
        $cmdTpl = '/usr/bin/ffmpeg -y -i %s -vcodec copy -fflags +genpts -strict experimental %s';
        $cmd = sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath));
        exec($cmd, $out);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * https://superuser.com/questions/133413/joining-h264-without-re-encoding
     */
    public static function joinVideos(array $files, $outputPath)
    {
        $tmp = [];
        $tmpDir = rtrim(dirname($outputPath), '/').'/';

        foreach ($files as $file) {
            $cmdTpl = '/usr/bin/ffmpeg -y -i %s -vcodec copy -vbsf h264_mp4toannexb %s';

            $tmpName = $tmpDir.basename($file).'.ts';
            $cmd = sprintf($cmdTpl, escapeshellarg($file), escapeshellarg($tmpName));
            exec($cmd);

            if (file_exists($tmpName)) {
                $tmp[] = $tmpName;
            }
        }
        //$cmdTpl = '/usr/bin/ffmpeg -y -i "concat:%s" -vcodec copy -acodec copy -bsf aac_adtstoasc %s';
        $cmdTpl = '/usr/bin/ffmpeg -y -i "concat:%s" -vcodec copy -acodec copy -movflags +faststart %s';

        $cmd = sprintf($cmdTpl, join('|', $tmp), escapeshellarg($outputPath));

        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    public static function detectVideoCodec($filePath)
    {
        $fn = basename($filePath);
        if (preg_match('/\.(mgp|mpeg2|avi)$/i', $fn)) {
            $codec = 'mpeg2video';
        } elseif (preg_match('/\.(mp4|mkv)$/i', $fn)) {
            $codec = 'libx264 -movflags +faststart';
        } else {
            throw new \Exception('Invalid video format: '.$fn);
        }

        return $codec;
    }

    public static function getVideoCodec($filePath)
    {
        $cmdTpl = '/usr/bin/ffprobe -v error -select_streams v:0 -show_entries stream=codec_name -of default=nokey=1:noprint_wrappers=1 %s';
        $cmd = sprintf($cmdTpl, escapeshellarg($filePath));
        $codec = (string)exec($cmd);

        return $codec;
    }
    //------------------------------------------------------------------------------------------------------------------
    // AUDIO

    public static function detectAudioCodec($filePath)
    {
        $fn = basename($filePath);

        if (preg_match('/\.(mp3|mpeg3)$/i', $fn)) {
            $codec = 'libmp3lame';
        } elseif (preg_match('/\.(aac|m4a)$/i', $fn)) {
            $codec = 'aac';
        } else {
            throw new \Exception('Invalid audio format: '.$fn);
        }

        return $codec;
    }

    /**
     * ffmpeg -f lavfi -i anullsrc -t 60 -c:a libmp3lame base.mp3
     */
    public static function createEmptyAudio($outputPath, $duration)
    {
        if (empty($duration)) {
            throw new \Exception('Invalid duration : '.$duration);
        }

        $codec = self::detectAudioCodec($outputPath);

        $cmdTpl = '/usr/bin/ffmpeg -y -f lavfi -i anullsrc -t %s -c:a %s -strict experimental %s';
        $cmd = sprintf($cmdTpl, $duration, $codec, escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * ffmpeg -i clock.wav -acodec libmp3lame -to 1 c.mp3
     */
    public static function convertAudio($filePath, $outputPath, $opts)
    {
        $opts = array_merge([
            'duration' => null,
        ], $opts);

        $addon = [];
        if (!empty($opts['duration'])) {
            $addon[] = '-to '.$opts['duration'];
        }

        $codec = self::detectAudioCodec($outputPath);

        $cmdTpl = '/usr/bin/ffmpeg -y -i %s -acodec %s %s -strict experimental %s';
        $cmd = sprintf($cmdTpl, escapeshellarg($filePath), $codec, join(' ', $addon), escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * WITH DELAY: ffmpeg -t 3 -f lavfi -i anullsrc -t 3 -i shadows.mp4 -filter_complex "concat=n=2:v=0:a=1" s.mp3
     * NORMAL: ffmpeg -t 3 -i rabbit.mp4 -strict -2 -vn 1.m4a
     */
    public static function extractSound($filePath, $outputPath, $opts)
    {
        $opts = array_merge([
            'skip'     => null,
            'duration' => null,
        ], $opts);

        $pre = [
            '-ss' => $opts['skip'],
            '-t'  => $opts['duration'],
        ];
        $codec = self::detectAudioCodec($outputPath);
        $cmdTpl = '/usr/bin/ffmpeg -y %s -i %s -vn -map a -acodec %s -ac 2 -strict experimental %s';
        $cmd = sprintf($cmdTpl, self::buildOpts($pre), escapeshellarg($filePath), $codec, escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * Repackage the files with: (Note that this won't transcode the files, just will copy them)
     * https://stackoverflow.com/questions/34118013/how-to-determine-webm-duration-using-ffprobe
     *
     * ffmpeg - i source.webm - vcodec copy - acodec copy new_source.webm
     */
    public static function fixMeta($filePath, $outputPath, array $opts = [])
    {
        $cmdTpl = '/usr/bin/ffmpeg -y -i %s -vcodec copy -acodec copy -fflags +genpts -strict experimental %s';
        $cmd = sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    /**
     * @see https://superuser.com/questions/650291/how-to-get-video-duration-in-seconds
     */
    public static function getDuration($filePath, $hms = false)
    {
        $cmdTpl = '/usr/bin/ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 %s %s';
        $cmd = sprintf($cmdTpl, $hms ? '-sexagesimal' : '', escapeshellarg($filePath));
        $res = exec($cmd);

        if ($hms) {
            $duration = strval($res);
        } else {
            $duration = $res * 1000;
        }

        return $duration;
    }

    public static function generateThumbnail($filePath, $outputPath, array $opts = [])
    {
        $ss = $opts['ss'] ?? 0;
        $cmdTpl = '/usr/bin/ffmpeg -ss %s -i %s -frames:v 1 %s';
        $cmd = sprintf($cmdTpl, $ss, escapeshellarg($filePath), escapeshellarg($outputPath));
        exec($cmd);

        return file_exists($outputPath) ? $outputPath : '';
    }

    //------------------------------------------------------------------------------------------------------------------
    // RESOLUTIONS

    public static function getResolutions($returnAllValues = false): Collection
    {
        static $col = null;
        if (is_null($col)) {
            $items = [
                'sqcif'  => '128x96',
                'qcif'   => '176x144',
                'cif'    => '352x288',
                '4cif'   => '704x576',
                '16cif'  => '1408x1152',
                'qqvga'  => '160x120',
                'qvga'   => '320x240',
                'vga'    => '640x480',
                'svga'   => '800x600',
                'xga'    => '1024x768',
                'uxga'   => '1600x1200',
                'qxga'   => '2048x1536',
                'sxga'   => '1280x1024',
                'qsxga'  => '2560x2048',
                'hsxga'  => '5120x4096',
                'wvga'   => '852x480',
                'wxga'   => '1366x768',
                'wsxga'  => '1600x1024',
                'wuxga'  => '1920x1200',
                'woxga'  => '2560x1600',
                'wqsxga' => '3200x2048',
                'wquxga' => '3840x2400',
                'whsxga' => '6400x4096',
                'whuxga' => '7680x4800',
                'cga'    => '320x200',
                'ega'    => '640x350',
                '240p'   => '352x240',
                '360p'   => '480x360',
                'hd480'  => '852x480',
                'hd720'  => '1280x720',
                'hd1080' => '1920x1080',
            ];
            $col = Collection::make();
            foreach ($items as $k => $v) {
                $col->push(['alias' => $k, 'value' => $v]);
            }
        }

        if ($returnAllValues) {
            $aliases = $col->pluck('alias');
            $vals = $col->pluck('value');
            $col = $aliases->merge($vals);
        }

        return $col;
    }

    public static function getResolutionXY($param, $strict = false)
    {
        $param = strval($param);
        $result = $strict ? '' : $param;
        $resCol = self::getResolutions();
        foreach ($resCol as $res) {
            if ($res['alias'] == $param || $res['value'] == $param) {
                $result = $res['value'];
                break;
            }
        }

        return $result;
    }
}

// https://stackoverflow.com/questions/11779490/how-to-add-a-new-audio-not-mixing-into-a-video-using-ffmpeg
// ffmpeg -i rabbit.mp4 -i wah_wah_wah.mp3 -filter_complex amix=inputs=2:duration=longest:dropout_transition=0 -vcodec copy -acodec copy out.avi

// ffmpeg -i out.avi -i image.png -filter_complex "[0:v][1:v] overlay=25:25:enable='between(t,0,20)'" -pix_fmt yuv420p -acodec copy output.mp4
// ffmpeg -i out.avi -i masha.png -i santa.png -filter_complex "[0:v][1:v] overlay=25:25:enable='between(t,1,3)'[a]; [2:v][a] overlay=25:25:enable='between(t,3,5)'[b];" -pix_fmt yuv420p -acodec copy out2.avi

// ffmpeg -i rabbit.mp4 -vcodec mpeg2video -q:v 2 -s hd720 -acodec libmp3lame rabbit.avi

// ffmpeg -i base.avi -i rabbit.avi -filter_complex "[0:v][1:v] overlay=25:25:enable='between(t,0,3)'" -vcodec mpeg2video -q:v 2 -s hd720 -acodec copy z.avi

// ffmpeg -i base.mp4 -itsoffset 1 -t 2 -i clock.mp3 -map 0:0 -map 1:a -vcodec copy -acodec copy z.mp4

// ffmpeg -i base.mp4 -itsoffset 1 -i clock.mp3 -itsoffset 3 -i wah.mp3 -filter_complex amix=inputs=3:duration=first:dropout_transition=3 -vcodec copy -acodec copy z.mp4

//ffmpeg -i outr20.wav -i outr20t03.wav -filter_complex "[0][1]amix=inputs=2[m];[m]alimiter[a]" -map [a] output.wav
// BASE SOUND:
// ffmpeg -f lavfi -i anullsrc -t 60 -c:a libmp3lame base.mp3
// SOUND WITH DELAY AND DURATION
// ffmpeg -t 3 -f lavfi -i anullsrc -t 3 -i clock.mp3 -filter_complex "[0:0] [1:0] concat=n=2:v=0:a=1" c.mp3
// ffmpeg -t 3 -f lavfi -i anullsrc -t 3 -i shadows.mp4 -filter_complex "concat=n=2:v=0:a=1" s.mp3
// MIX AUDIO:
// ffmpeg -i base.mp3 -i c.mp3 -i w.mp3 -filter_complex amix=inputs=3:duration=first:dropout_transition=0 z.mp3

// OVERLAY WITH OFFSET
// ffmpeg -i base.mp4 -itsoffset 4 -t 4 -i rabbit.mp4 -t 4 -i shadows.mp4 -i masha.png -filter_complex "overlay=eof_action=pass,overlay=eof_action=pass,overlay=0:0:enable='between(t,0,3)" -an test.mp4
// OVERLAY VIDEOS + PICTURES:
// ffmpeg -i base.mp4 -i rabbit.mp4 -i shadows.mp4 -i masha.png -filter_complex "overlay=25:25:enable='between(t,5,8)',overlay=25:25:enable='between(t,2,4)',overlay=25:25:enable='between(t,0,3)'" -an test.mp4
// + TEXT
// ffmpeg -i base.mp4 -i rabbit.mp4 -i shadows.mp4 -i masha.png -filter_complex "overlay=25:25:enable='between(t,5,8)',overlay=25:25:enable='between(t,2,4)',overlay=25:25:enable='between(t,0,3)',drawtext=enable='between(t,1,10)':fontsize=50:fontcolor=white:fontfile=/usr/share/fonts/truetype/freefont/FreeSerif.ttf:text='Test Text'" -an test.mp4


// ffmpeg -i base.mp4 -i shadows.mp4 -itsoffset 5 -i rabbit.mp4 -filter_complex "overlay=0:0:enable='between(t,0,20)',overlay=0:0:enable='between(t,5,9)'" -an video.mp4