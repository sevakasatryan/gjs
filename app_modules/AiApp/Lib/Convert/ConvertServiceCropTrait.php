<?php namespace Modules\AiApp\Convert;

use SplFileObject;

trait ConvertServiceCropTrait
{

//        ****************************************************
//    610 ----------------------X 420,610                    *
//        |                     |                            *
//        |                     |                            *
//        |                     |                            *
//        |                     |                            *
//        |                     |                            *
//        |                     |                            *
//        |                     |                            *
//        |0,315                |                            *
//    315 X----------------------                            *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//        *                                                  *
//      0 ****************************************************
    public static function pdfCrop($pdfFilePath, $outputPath, $cropData)
    {
        $ret = '';
        $dims = self::getPdfDimensions($pdfFilePath);
        $coords = self::percentCoordsToAbs($cropData, $dims['width'], $dims['height']);

        if ($pdfFilePath == $outputPath) {
            throw new \Exception('Input and output paths are the same');
        }

        if ($coords) {
//            $cmdTpl = "/usr/bin/pdfcrop --margins '-%s -%s -%s -%s' %s %s";
//            $cmd = sprintf($cmdTpl, $coords['w'], $coords['n'], $coords['e'], $coords['s'], escapeshellarg($pdfFilePath), escapeshellarg($outputPath));
//
//            $cmdTpl = "/usr/bin/podofobox %s %s trim %s %s %s %s";
//            $cmd = sprintf($cmdTpl, escapeshellarg($pdfFilePath), escapeshellarg($outputPath), $coords['x1'], $coords['iy1'], $coords['width'], $coords['height']);
//
            /*
            * "width" => "595"
            * "height" => "842"
            * 0, 0, 595, 842 - 100% doc
            */
            $cmdTpl = "/usr/bin/gs -o %s -sDEVICE=pdfwrite -c '[/CropBox [%s %s %s %s]' -c '/PAGES pdfmark' -f %s";
            $cmd = sprintf($cmdTpl, escapeshellarg($outputPath), $coords['x1'], $coords['iy1'], $coords['x2'], $coords['iy2'], escapeshellarg($pdfFilePath));
            exec($cmd, $output);

            if (file_exists($outputPath)) {
                $ret = $outputPath;
            }
        }

        return $ret;
    }

    public static function getPdfDimensions($pdfFilePath, $box = 'MediaBox')
    {
        //$box can be set to BleedBox, CropBox or MediaBox
        $stream = new SplFileObject($pdfFilePath);
        $result = false;

        $num = "[0-9\.]{1,}";

        $line = 0;
        while (!$stream->eof() && $line < 5000) {
            $line++;
            $str = $stream->fgets();
            if (preg_match("/{$box} \[$num $num ($num) ($num)\]/", $str, $matches)) {
                $result['width'] = $matches[1];
                $result['height'] = $matches[2];
                break;
            }
        }
        $stream = null;

        return $result;
    }

    /**
     * @param $filePath
     * @param $outputPath
     * @param $points
     * @return string
     *
     * @see http://www.imagemagick.org/Usage/distorts/#perspective
     */
    public static function cropFitImage($filePath, $outputPath, $points)
    {
        $ret = '';
        $validCropData = self::isValidCropFitData($points);
        if ($validCropData) {
            $size = getimagesize($filePath);
            $absPoints = self::percentPointsToAbs($points, $size[0], $size[1]);

//            list($xx, $yy) = explode('x', self::A4_300_DPI);
            $xx = $size[0];
            $yy = $size[1];

            //convert skar356.jpeg  -virtual-pixel white -distort Affine '50,200 0,0 300,3000 0,3500 2000,2800 2500 3500 2200,50 2500 0' zz.jpg
            $pointsString = sprintf("%s,%s 0,0 %s,%s 0,{$yy} %s,%s {$xx},{$yy} %s,%s {$xx},0",
                $absPoints[0][0], $absPoints[0][1],
                $absPoints[1][0], $absPoints[1][1],
                $absPoints[2][0], $absPoints[2][1],
                $absPoints[3][0], $absPoints[3][1]
            );
            //$cmdTpl = "/usr/bin/convert %s virtual-pixel white -distort Affine '%s' %s";
            $cmdTpl = "/usr/bin/convert %s -matte -virtual-pixel white -distort Perspective '%s' %s";

            $cmd = sprintf($cmdTpl, escapeshellarg($filePath), $pointsString, escapeshellarg($outputPath));
            exec($cmd);
        } else {
            if ($filePath != $outputPath) {
                $cmdTpl = "/usr/bin/convert %s %s";
                $cmd = sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath));
                exec($cmd);
            }
        }

        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function cropImage($filePath, $outputPath, $cropData, $leaveSize = false)
    {
        $ret = '';
        $validCropData = self::isValidCropData($cropData);

        if ($validCropData) {
            $size = getimagesize($filePath);
            $coords = self::percentCoordsToAbs($cropData, $size[0], $size[1]);
            //convert mouse.jpeg \( -clone 0 -fill white -draw 'color 0,0 reset' \) \( -clone 0 -crop 500x500+300+300 \) -flatten new.jpg
            if ($leaveSize) {
                $cmdTpl = '/usr/bin/convert %s \( -clone 0 -fill white -draw "color 0,0 reset" \) \( -clone 0 -crop %sx%s+%s+%s \) -flatten %s';
            } else {
                $cmdTpl = "/usr/bin/convert %s -crop %sx%s+%s+%s %s";
            }
            $cmd = sprintf($cmdTpl, escapeshellarg($filePath), $coords['width'], $coords['height'], $coords['x1'], $coords['y1'], escapeshellarg($outputPath));
            exec($cmd);
        } else {
            if ($filePath != $outputPath) {
                copy($filePath, $outputPath);
            }
        }

        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function isValidCropFitData($points)
    {
        $isValid = false;
        if (is_array($points) && count($points) == 4) {
            $isValid = true;
            foreach ($points as $point) {
                if (count($point) !== 2 || !isset($point[0]) || !isset($point[1])) {
                    $isValid = false;
                }
            }
            if (empty($points[1][1]) || empty($points[2][0]) || empty($points[2][1]) || empty($points[3][0])) {
                $isValid = false;
            }
        }

        return $isValid;
    }

    public static function isValidCropData($coords)
    {
        return isset($coords['x1']) && isset($coords['y1']) && !empty($coords['x2']) && !empty($coords['y2']);
    }

    public static function percentPointsToAbs($points, $imageWidth, $imageHeight)
    {
        $absPoints = [];
        $validCropData = self::isValidCropFitData($points) && $imageWidth && $imageHeight;
        if ($validCropData) {
            foreach ($points as $point) {
                $x = intval($point[0] * $imageWidth / 100);
                $y = intval($point[1] * $imageHeight / 100);
                $x = max(0, min($x, $imageWidth));
                $y = max(0, min($y, $imageHeight));
                $absPoints[] = [$x, $y];
            }
        }

        return $absPoints;
    }

    public static function percentCoordsToAbs($coords, $imageWidth, $imageHeight)
    {
        $validCropData = self::isValidCropData($coords) && $imageWidth && $imageHeight;
        if ($validCropData) {
            $x1 = intval($coords['x1'] * $imageWidth / 100);
            $y1 = intval($coords['y1'] * $imageHeight / 100);
            $x2 = intval($coords['x2'] * $imageWidth / 100);
            $y2 = intval($coords['y2'] * $imageHeight / 100);
            $w = max(0, $x2 - $x1);
            $h = max(0, $y2 - $y1);
            $w = min($w, $imageWidth);
            $h = min($h, $imageHeight);

            return [
                'x1'     => $x1,
                'y1'     => $y1,
                'x2'     => $x2,
                'y2'     => $y2,
                'iy1'    => max(0, $imageHeight - $y1),
                'iy2'    => max(0, $imageHeight - $y2),
                'width'  => $w,
                'height' => $h,
                'w'      => $x1,
                'n'      => $y1,
                'e'      => max(0, $imageWidth - $x2),
                's'      => max(0, $imageHeight - $y2),
            ];

        } else {
            return null;
        }
    }
}
