<?php namespace Modules\AiApp\Convert;

class ConvertService
{
    use ConvertServiceImageTrait, ConvertServiceCropTrait, ConvertServiceTextTrait, ConvertServicePdfTrait,
        ConvertServiceMovieTrait;
    //
    const A4_72_DPI  = '595x841';
    const A4_150_DPI = '1240x1753';
    const A4_200_DPI = '1653x2338';
    const A4_300_DPI = '2480x3507';

    //------------------------------------------------------------------------------------------------------------------

    public static $imageMimes = [
        'image/jpeg',
        'image/jpg',
        'image/pjpeg',
        'image/png',
        'image/tiff',
        'image/gif',
        'image/miff',
    ];

    public static $movieMimes = [
        'video/mp4',
        'video/x-flv',
        'video/MP2T',
        'video/mp2t',
        'video/3gpp',
        'video/quicktime',
        'video/x-msvideo',
        'video/x-ms-wmv',
        'video/x-ms-asf',
        'video/x-matroska',
        'video/webm',
        'video/mpeg',
        'application/x-shockwave-flash',
    ];

    public static $audioMimes = [
        'audio/basic',
        'audio/L24',
        'audio/mp4',
        'audio/aac',
        'audio/mpeg',
        'audio/ogg',
        'audio/vorbis',
        'audio/x-ms-wma',
        'audio/x-ms-wax',
        'audio/x-aiff',
        'audio/x-m4a',
        'audio/x-hx-aac-adts',
        'audio/x-unknown',
        'audio/x-wav',
        'audio/vnd.rn-realaudio',
        'audio/wav',
        'audio/vnd.wave',
        'audio/webm',
        'audio/AMR',
        'audio/AMR-WB',
        'audio/3gpp',
        'audio/vnd.dolby.dd-raw',
        'audio/midi',
        'application/vnd.rn-realmedia',
    ];

    public static $docMimes = [
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/msword',
        'application/vnd.oasis.opendocument.text',
        'application/vnd.oasis.opendocument.text-web',
        'application/excel',
        'application/vnd.ms-excel',
        'application/x-excel',
        'application/x-msexcel',
        'application/rtf',
        'application/x-rtf',
        'application/xhtml+xml',
        'text/plain',
        'text/html',
        'text/rtf',
        'text/richtext',
    ];

    public static $pdfMimes = [
        'application/pdf',
    ];

    //------------------------------------------------------------------------------------------------------------------

    public static function isImageMime($mime)
    {
        return in_array($mime, self::$imageMimes);
    }

    public static function isMovieMime($mime)
    {
        return in_array($mime, self::$movieMimes);
    }

    public static function isDocumentMime($mime)
    {
        return in_array($mime, self::$docMimes);
    }

    public static function isAudioMime($mime)
    {
        return in_array($mime, self::$audioMimes);
    }

    public static function isPdfMime($mime)
    {
        return in_array($mime, self::$pdfMimes);
    }

    public static function isAudioOrMovieMime($mime)
    {
        return (self::isAudioMime($mime) || self::isMovieMime($mime));
    }

    //------------------------------------------------------------------------------------------------------------------

    public static function isImage($filePath)
    {
        $mime = self::getMime($filePath);

        return self::isImageMime($mime);
    }

    public static function isMovie($filePath)
    {
        $mime = self::getMime($filePath);

        return self::isMovieMime($mime);
    }

    public static function isDocument($filePath)
    {
        $mime = self::getMime($filePath);

        return self::isDocumentMime($mime);
    }

    public static function isAudio($filePath)
    {
        $mime = self::getMime($filePath);

        return self::isAudioMime($mime);
    }

    public static function isPdf($filePath)
    {
        $mime = self::getMime($filePath);

        return self::isPdfMime($mime);
    }

    //------------------------------------------------------------------------------------------------------------------

    public static function getMime($filePath)
    {
        $mime = mime_content_type($filePath);

        if ($mime == 'application/octet-stream') {
            $filename = escapeshellcmd($filePath);
            $command = "/usr/bin/mimetype -b {$filename}";
            $mime = trim(shell_exec($command));
        }

        return $mime;
    }

    public static function detectType($mime)
    {
        $type = '';
        if (self::isImageMime($mime)) {
            $type = 'image';
        } elseif (self::isMovieMime($mime)) {
            $type = 'video';
        } elseif (self::isDocumentMime($mime)) {
            $type = 'doc';
        } elseif (self::isAudioMime($mime)) {
            $type = 'audio';
        } elseif (self::isPdfMime($mime)) {
            $type = 'pdf';
        }

        return $type;
    }

    public static function extractMeta($filePath): array
    {
        if (is_link($filePath)) {
            $filePath = readlink($filePath);
        }

        if (!is_readable($filePath)) {
            throw new \Exception('FilePath is not readable : '.$filePath);
        }

        $mime = self::getMime($filePath);
        $parts = explode('/', $mime);
        $type = $parts[0] ?? '';

        $meta = [
            'mime'           => $mime,
            'type'           => $type,
            'duration'       => '',
            'duration_human' => '',
            'width'          => '',
            'height'         => '',
        ];

        if (in_array($type, ['audio', 'video'])) {
            /**
             * Duration
             */
            try {
                $media = \FFMpeg::fromDisk('root')->open($filePath);
                $duration = round($media->getDurationInMiliseconds() / 1000, 4);
                $meta['duration'] = $duration;
                $meta['duration_human'] = gmdate("H:i:s", intval($duration));
            } catch (\Exception $e) {
            } catch (\Throwable $e) {
            }
        }

        if ($type == 'video') {
            try {
                /** @var \Pbmedia\LaravelFFMpeg\Media $media */
                $media = \FFMpeg::fromDisk('root')->open($filePath);
                $fs = $media->getFirstStream();
                $meta['width'] = $fs->get('width');
                $meta['height'] = $fs->get('height');
            } catch (\Exception $e) {

            } catch (\Throwable $e) {

            }
        }

        if ($type == 'image') {
            /**
             * Dimensions WxH
             */
            try {
                list($width, $height, $type, $attr) = getimagesize($filePath);
                $meta['width'] = $width;
                $meta['height'] = $height;
            } catch (\Exception $e) {

            }
        }

        return $meta;
    }

    public static function buildOpts(array $opts)
    {
        $res = [];
        foreach ($opts as $k => $v) {
            if (!is_null($v)) {
                if (empty($k) || is_numeric($k)) {
                    $res[] = strval($v);
                } else {
                    $res[] = strval($k).' '.$v;
                }
            }
        }

        return join(' ', $res);
    }

}
