<?php namespace Modules\AiApp\Convert;

use File;

trait ConvertServicePdfTrait
{
    public static function splitPdf($filePath)
    {
        $ok = 0;
        $cmdTpl = '/usr/bin/pdftk %s burst output "%s.page_%%03d.pdf" 2>&1';
        $outputFnPrefix = preg_replace('/.pdf$/ui', '', $filePath);
        $cmd = sprintf($cmdTpl, escapeshellarg($filePath), $outputFnPrefix);
        exec($cmd, $output, $ret);
        $tfile = dirname($filePath).'/doc_data.txt';
        if (file_exists($tfile)) {
            @unlink($tfile);
        }
        $ok = (0 == $ret);

        return $ok;
    }

    public static function imageToPDF($filePath)
    {
        $pdfPath = '';
        if (self::isImage($filePath)) {
            $outPdf = $filePath.'.pdf';
            @unlink($outPdf);

            $dpi = 200;
            $x = $dpi * 827 / 100;
            $y = $dpi * 1169 / 100;
            $x2 = $x;
            $y2 = $y - 100;
            //$cmdTpl = "/usr/bin/convert %s -compress jpeg -quality 70 -density {$dpi} -units PixelsPerInch -resize {$x}x{$y} -repage {$x}x{$y2} -trim +repage %s";
            $cmdTpl = "/usr/bin/convert %s -compress jpeg -quality 70 -density {$dpi} -units PixelsPerInch -resize {$x}x{$y}".
                " -bordercolor white -border 20x0 -repage {$x2}x{$y2} -background white -gravity south -splice 0x50 +gravity %s";

            //-gravity south -background white -splice 0x20

            $cmd = sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outPdf));
            exec($cmd, $output, $ret);

            if (file_exists($outPdf)) {
                $pdfPath = $outPdf;
            }
        } elseif (self::isPdf($filePath)) {
            $pdfPath = $filePath;
        }

        return $pdfPath;
    }

    public static function pdfToImage($filePath, $outputPath, $density = 150, $size = '2480x3508', $quality = 100)
    {
        $ret = $filePath;
        if (self::isPdf($filePath)) {
            $cmdTpl = '/usr/bin/convert -density %s -depth 8 %s -background white -alpha remove -resize %s -quality %s %s';
            $cmd = sprintf($cmdTpl, intval($density), escapeshellarg($filePath), $size, intval($quality), escapeshellarg($outputPath));
            exec($cmd);
            if (file_exists($outputPath)) {
                $ret = $outputPath;
            }
        }

        return $ret;
    }

    public static function pdfToImage2($filePath, $outputPath, $density = 150, $size = 2480)
    {
        $ret = $filePath;
        if (self::isPdf($filePath)) {
            $density = (int)$density;
            $size = (int)$size;
            $tmpPrefix = dirname($filePath).'/converted';
            $cmdTpl = "/usr/bin/pdftoppm -f 1 -l 1 -r $density -tiff -scale-to-x $size -scale-to-y -1 %s $tmpPrefix";
            $cmd = sprintf($cmdTpl, escapeshellarg($filePath));
            exec($cmd);
            $tmpOutFile = $tmpPrefix.'-1.tif';
            if (file_exists($tmpOutFile)) {
                if (preg_match('/\.tiff?$/i', $outputPath)) {
                    @rename($tmpOutFile, $outputPath);
                } else {
                    $cmdTpl = "/usr/bin/convert %s %s";
                    $cmd = sprintf($cmdTpl, escapeshellarg($tmpOutFile), escapeshellarg($outputPath));
                    exec($cmd);
                }
                if (file_exists($outputPath)) {
                    $ret = $outputPath;
                }
            }
        }

        return $ret;
    }

    public static function joinPDFs($filePaths, $outputPath)
    {
        if (count($filePaths) && preg_match('/\.pdf$/i', $outputPath)) {
            $input = [];
            foreach ($filePaths as $filePath) {
                if (file_exists($filePath)) {
                    $input[] = escapeshellarg($filePath);
                }
            }
            @unlink($outputPath);

            $cmdTpl = '/usr/bin/pdfunite %s %s';
            $cmd = sprintf($cmdTpl, join(' ', $input), escapeshellarg($outputPath));

//            $cmdTpl = '/usr/bin/gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=%s -dBATCH %s';
//            $cmd = sprintf($cmdTpl, escapeshellarg($outputPath), join(' ', $input));

            exec($cmd, $output, $ret);
        }

        return file_exists($outputPath);
    }

    /**
     * @param $filePath
     * @param $pdfFilePath
     */
    public static function docToPdf($filePath, $pdfFilePath)
    {
        $lockFilePath = storage_path('locks/unoconv.lock');
        $lockFileHandler = fopen($lockFilePath, 'w+');

        $maxAttempts = 200;

        $attempts = 0;
        do {
            $attempts++;
            $tryLock = flock($lockFileHandler, LOCK_EX | LOCK_NB);
            if (empty($tryLock)) {
                sleep(1);
            }
        } while (($attempts < $maxAttempts) && empty($tryLock));

        /**
         * kill frozen process:
         */
        if ($attempts == $maxAttempts) {
            $cmd = '/usr/bin/killall soffice.bin';
            exec($cmd);
        }

        /**
         * Converting:
         */
        $orig = getenv('HOME');

        $tf = '/tmp/libre/'.time();
        File::makeDirectory($tf, 0777, true, true);

        putenv('HOME='.$tf);
        putenv('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin');
        //
        try {
            $cmdTpl = '/usr/bin/unoconv -vvvv -f pdf -o %s %s 2>&1';
            $cmd = sprintf($cmdTpl, escapeshellarg($pdfFilePath), escapeshellarg($filePath));
            exec($cmd, $output, $ret);

            /**
             * [HOME]/.config/libreoffice/
             * @see https://github.com/dagwieers/unoconv/issues/241
             */
            if (false !== stristr(join('', $output), 'Failed to connect to /usr/lib/libreoffice/program/soffice.bin')) {
                exec($cmd, $output, $ret);
            }
        } catch (\Exception $e) {
        } finally {
            putenv('HOME='.$orig);
            File::deleteDirectory($tf);
        }
        flock($lockFileHandler, LOCK_UN);
    }
}
