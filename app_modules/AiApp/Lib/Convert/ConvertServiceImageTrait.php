<?php namespace Modules\AiApp\Convert;

use Image;

trait ConvertServiceImageTrait
{
    public static function autoOrient($imagePath)
    {
        return Image::make($imagePath)->orientate()->save($imagePath);
    }

    public static function binarization($filePath, $outputPath)
    {
        $ret = '';
        $cmdTpl = base_path('bin').'/my_bin.sh %s %s';
        $res = exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)), $o, $r);
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function getImageData($filePath)
    {
        //$cmdTpl = '/usr/bin/identify %s';
        $cmdTpl = '/usr/bin/convert %s -verbose info:';

        $cmd = sprintf($cmdTpl, escapeshellarg($filePath));
        exec($cmd, $output);

        return $output;
    }

    public static function fixPngGif($filePath, $outputPath = null)
    {
        $ret = $filePath;
        $mime = self::getMime($filePath);
        $outputPath = $filePath ? $filePath : $filePath.'.miff';
        if (in_array($mime, ['image/png', 'image/gif'])) {
            $cmdTpl = '/usr/bin/convert %s %s';
            exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)));
            if (file_exists($outputPath)) {
                $ret = $outputPath;
            }
        }

        return $ret;
    }

    /**
     * @param $filePath
     * @param $outputPath
     * @param string $size
     * @return string
     *
     */
    public static function resizeImage($filePath, $outputPath, $size = self::A4_300_DPI)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -units PixelsPerInch -density 300 -resize %s -quality 100 %s';
        exec(sprintf($cmdTpl, escapeshellarg($filePath), $size, escapeshellarg($outputPath)));
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    /**
     * Exact box dimensions WxH
     */
    public static function resizeImageBox($filePath, $outputPath, $size = self::A4_300_DPI)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -units PixelsPerInch -density 300 -resize %s -quality 100 -background transparent -gravity center -extent %s %s';
        exec(sprintf($cmdTpl, escapeshellarg($filePath), $size, $size, escapeshellarg($outputPath)));
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function grayscaleImage($filePath, $outputPath)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -depth 8 -colorspace gray -quality 100 %s';
        exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)));
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function addFrame($filePath, $outputPath, $percent = 10, $border = false)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -bordercolor white -border %s -quality 100 %s';
        exec(sprintf($cmdTpl, escapeshellarg($filePath), $percent.'%', escapeshellarg($outputPath)));

        if ($border) {
            $cmdTpl = '/usr/bin/convert %s -bordercolor green -border %s -quality 100 %s';
            exec(sprintf($cmdTpl, escapeshellarg($outputPath), '10', escapeshellarg($outputPath)));
        }

        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function removeTransparencyImage($filePath, $outputPath)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -background white -alpha remove -flatten +matte -quality 100 %s';
        exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)));
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function binImage($filePath, $outputPath): string
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -threshold 50%% %s';
        $res = exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)), $o);
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function isAnimatedGif($filePath)
    {
        $frames = 0;
        if (preg_match('|gif$|i', $filePath)) {
            $cmdTpl = '/usr/bin/identify -format %%n %s';
            $cmd = sprintf($cmdTpl, escapeshellarg($filePath));
            $res = exec($cmd);
            $frames = intval($res);
        }

        return $frames > 1;
    }

    /**
     * The color type of PNG image is stored at byte offset 25. Possible values of that 25'th byte is:
     *
     * 0 - greyscale
     * 2 - RGB
     * 3 - RGB with palette
     * 4 - greyscale + alpha
     * 6 - RGB + alpha
     *
     * @param $filePath
     * @return bool
     */
    public static function isTransparentPng($filePath)
    {
        $data = ord(@file_get_contents($filePath, false, null, 25, 1));
        $isAlpha = ($data == 4 || $data == 6);

        return $isAlpha;
    }
}
