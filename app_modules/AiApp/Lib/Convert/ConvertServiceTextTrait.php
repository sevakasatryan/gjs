<?php namespace Modules\AiApp\Convert;

trait ConvertServiceTextTrait
{
    public static function getParseCmd($inputFile, $output = 'stdout', $format = '', $mode = 'default')
    {
        $v3 = [
            'default' => 1,
            'block'   => 1,
            'line'    => 7,
            'max'     => 6,
        ];

        $v4 = [
            'default' => 1,
            'block'   => 1,
            'line'    => 7,
            'max'     => 11,
        ];

        if (is_local()) {
            // Compiled v4
            $psm = $v4[$mode];
            $tpl = 'taskset 0x1 /opt/tess40/bin/tesseract -l eng --oem 2 --psm '.$psm.' %s %s %s';
        } else {
            // v3
            $psm = $v3[$mode];
            $tpl = 'taskset 0x1 /usr/bin/tesseract -l eng -psm '.$psm.' %s %s %s';
        }

//        if ($output !== 'stdout' && $format) {
//            $output = preg_replace('/\.'.$format.'$/', '', $output);
//        }

        return sprintf($tpl, escapeshellarg($inputFile), escapeshellarg($output), $format);
    }

    public static function parseImageToText($filePath, $txtFilePath = null, $x1 = 0, $y1 = 0, $x2 = 100, $y2 = 100, $mode = 'block')
    {
        static $cnt;

        if (!in_array($mode, ['block', 'line'])) {
            throw new \Exception('Invalid mode');
        }

        $text = '';
        if (self::isImage($filePath) && $x2 && $y2) {
            $cnt++;
            $txtFilePath = $txtFilePath ? $txtFilePath : sprintf('%s.%s.txt', $filePath, $cnt);
            $filePath = self::cropImage($filePath, sprintf('%s.%s.tiff', $filePath, $cnt), compact('x1', 'y1', 'x2', 'y2'));

            $filePath = self::prepareForOCR($filePath, sprintf('%s.in.tiff', $filePath));
            $cmd = self::getParseCmd($filePath, preg_replace('/\.txt$/i', '', $txtFilePath), 'txt', $mode);

            exec($cmd);

            if (file_exists($txtFilePath)) {
                $text = file_get_contents($txtFilePath);
                $text = trim($text);
                $text = preg_replace('/(^[^\w]+|[^\w]+$)/u', '', $text);
            }
        }

        return $text;
    }

    public static function prepareForOCR($filePath, $outputPath)
    {
        $ret = '';
        $cmdTpl = '/usr/bin/convert %s -resize 2000x2000 -depth 8 -colorspace gray -antialias -quality 100 %s';
        $res = exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)), $o, $r);
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function pdfToText($pdfFilePath, $txtFilePath = null, $x1 = 0, $y1 = 0, $x2 = 100, $y2 = 100)
    {
        $text = '';

        if (self::isPdf($pdfFilePath) && $x2 && $y2) {
            $txtFilePath = $txtFilePath ? $txtFilePath : $pdfFilePath.'.txt';
            $dims = self::getPdfDimensions($pdfFilePath);

            if ($dims) {
                /**
                 * extract text
                 */
                if (empty($x1) && empty($y1)) {
                    $cmdTpl = '/usr/bin/pdftotext %s %s';
                    $cmd = sprintf($cmdTpl, escapeshellarg($pdfFilePath), escapeshellarg($txtFilePath));
                } else {
                    $pixX = intval($x1 / 100 * $dims['width']);
                    $pixY = intval($y1 / 100 * $dims['height']);
                    $pixW = intval($x2 / 100 * $dims['width']) - $pixX;
                    $pixH = intval($y2 / 100 * $dims['height']) - $pixY;

                    $cmdTpl = '/usr/bin/pdftotext -layout -x %s -y %s -W %s -H %s %s %s';
                    $cmd = sprintf($cmdTpl, $pixX, $pixY, $pixW, $pixH, escapeshellarg($pdfFilePath), escapeshellarg($txtFilePath));
                }

                if (is_local() && is_cli()) {
                    d($cmd);
                }

                exec($cmd);

                /**
                 * clean
                 */
                $cmdTpl = "sed -i 's/Â / /g' %s";
                $cmd = sprintf($cmdTpl, escapeshellarg($txtFilePath));
                exec($cmd);

                if (file_exists($txtFilePath)) {
                    $text = file_get_contents($txtFilePath);
                }
            }

            $text = trim($text);
            $text = preg_replace('/(^[^\w]+|[^\w]+$)/u', '', $text);
        }

        return $text;
    }

    public static function textCleaner($filePath, $outputPath, string $opts = ''): string
    {
        $ret = '';
        $cmdTpl = base_path('bin').'/textcleaner.sh %s %s %s';
        $res = exec(sprintf($cmdTpl, $opts, escapeshellarg($filePath), escapeshellarg($outputPath)), $o);
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }

    public static function textDeskew($filePath, $outputPath)
    {
        $ret = '';
        $cmdTpl = base_path('bin').'/textdeskew.sh %s %s';
        $res = exec(sprintf($cmdTpl, escapeshellarg($filePath), escapeshellarg($outputPath)), $o);
        if (file_exists($outputPath)) {
            $ret = $outputPath;
        }

        return $ret;
    }
}
