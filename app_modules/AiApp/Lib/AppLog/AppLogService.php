<?php namespace Modules\AiApp\AppLog;

use Modules\AiApp\Helpers\ActionResult;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Formatter\LineFormatter;
use Request;

class AppLogService
{
    protected $loggers = [];
    protected $formatter, $maxFiles;

    public function __construct()
    {
        $ip = Request::ip();
        $this->formatter = new LineFormatter("[%datetime%] [".$ip."] %message% \n");
        $this->maxFiles = config('app.log_max_files', 5);
    }

    public function __call($loggerName, $arguments)
    {
        $logger = $this->getLoggerByName($loggerName);
        $this->splitAndWrite($arguments[0], $logger);
    }

    protected function getLoggerByName($loggerName)
    {
        $loggerName = snake_case($loggerName);
        $loggerName = str_replace('_', '-', $loggerName);
        if (!array_key_exists($loggerName, $this->loggers)) {
            $logger = new Logger('logger-'.$loggerName);

            $logHandler = new RotatingFileHandler(
                storage_path().'/logs/'.$loggerName.'.log',
                $this->maxFiles,
                Logger::ERROR
            );

            $logHandler->setFormatter($this->formatter);
            $logger->pushHandler($logHandler);
            $this->loggers[$loggerName] = $logger;
        }

        return $this->loggers[$loggerName];
    }

    protected function splitAndWrite($message, Logger $logger)
    {
        $lines = $this->split($message);
        $this->writeBlock($lines, $logger);
    }

    protected function split($something)
    {
        if (in_array($something, ['_', '-', '='])) {
            $something = str_repeat($something, 80);
        }

        if ($something instanceof ActionResult) {
            $arr = [];
            foreach ((array)$something->getData('mylog') as $log) {
                $arr[] = sprintf('[%s] %s', strtoupper($log['type']), $log['message']);
            }
            $something = $arr;
        }

        if (($something instanceof \Exception) || ($something instanceof \Throwable)) {
            $something = [
                'CLASS'   => get_class($something),
                'FILE'    => $something->getFile(),
                'LINE'    => $something->getLine(),
                'MESSAGE' => $something->getMessage(),
                'TRACE'   => $something->getTraceAsString(),
            ];
        } elseif (is_object($something)) {
            //$something = get_object_vars($something);
            $something = explode(PHP_EOL, print_r($something, 1));
        }

        /*
         *
         */

        if (is_array($something)) {
            $lines = explode(PHP_EOL, print_r($something, 1));
        } else {
            $lines = [(string)$something];
        }

        if (count($lines) > 1) {
            $separator = str_repeat('-', 80);
            array_unshift($lines, $separator);
            array_push($lines, $separator);
        }

        return $lines;
    }

    protected function writeBlock(array $lines, Logger $logger)
    {
        foreach ($lines as $line) {
            $logger->error($line);
        }
    }

}