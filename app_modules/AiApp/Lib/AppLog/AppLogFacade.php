<?php namespace Modules\AiApp\AppLog;

use Illuminate\Support\Facades\Facade;

class AppLogFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'app.log';
    }
}