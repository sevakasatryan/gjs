<?php namespace Modules\AiApp\Media\ImageGenerators;

use Illuminate\Support\Collection;
use Spatie\MediaLibrary\Conversion\Conversion;
use Spatie\MediaLibrary\ImageGenerators\BaseGenerator;

class Audio extends BaseGenerator
{
    /**
     * This function should return a path to an image representation of the given file.
     */
    public function convert(string $file, Conversion $conversion = null): string
    {
        $pathToImageFile = pathinfo($file, PATHINFO_DIRNAME).'/'.pathinfo($file, PATHINFO_FILENAME).'.jpg';

        $waveform = new Waveform($file);
        try {
            $success = $waveform->getWaveform($pathToImageFile, 600, 400);
        } catch (\Exception $e) {

        }

        return $pathToImageFile;
    }

    public function requirementsAreInstalled(): bool
    {
        return !empty(shell_exec("which sox"));
    }

    public function supportedExtensions(): Collection
    {
        return collect(['mp3', 'wav', 'm4p', 'caf', 'aiff', 'aif']);
    }

    public function supportedMimeTypes(): Collection
    {
        return collect([
            'audio/mpeg',
        ]);
    }
}