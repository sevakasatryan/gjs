<?php namespace Modules\AiApp\Media;

use Modules\AiApp\FileSystem\Adapter\MyAdapterTrait;
use Spatie\MediaLibrary\UrlGenerator\UrlGenerator;
use Spatie\MediaLibrary\UrlGenerator\BaseUrlGenerator;
use Storage;

class CustomUrlGenerator extends BaseUrlGenerator implements UrlGenerator
{
    public function getUrl(): string
    {
        $driver = $this->media->getDiskDriverName();
        $disk = $this->media->disk;
        $storage = Storage::disk($disk);
        /** @var MyAdapterTrait $adapter */
        $adapter = $storage->getDriver()->getAdapter();
        $path = $this->getPathRelativeToRoot();

        if ($this->media->getCustomProperty('permanent')) {
            $url = $adapter->getPermanentUrl($path);
        } else {
            $url = $adapter->getTtlUrl($path);
        }

        return $url;
    }

    public function getPath(): string
    {
        return parent::getPathRelativeToRoot();
    }

//    public function getFullPath(): string
//    {
//        $storage = $this->media->getStorage();
//        $adapter = $storage->getDriver()->getAdapter();
//
//        return rtrim($adapter->getPathPrefix(), '/').'/'.$this->getPath();
//    }
}