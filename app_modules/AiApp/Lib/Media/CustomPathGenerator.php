<?php namespace Modules\AiApp\Media;

use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;
use Carbon\Carbon;

class CustomPathGenerator implements PathGenerator
{
    /*
     * Get the path for the given media, relative to the root storage path.
     */
    public function getPath(Media $media): string
    {
//        $suffix = substr($media->id, -2);
//        $suffix = str_pad($suffix, 2, '0', STR_PAD_LEFT);

        /** @var Carbon $createdAt */
        $createdAt = $media->created_at;
        $createdAt->setTimezone('UTC');
        $suffix = $createdAt->format('Y-m-d-H');
        $postfix = str_pad($media->id, 8, '0', STR_PAD_LEFT);

        return sprintf('%s/%s/', $suffix, $postfix);
    }

    /*
     * Get the path for conversions of the given media, relative to the root storage path.
     * @return string
     */
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media).'c/';
    }
}