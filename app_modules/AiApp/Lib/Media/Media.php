<?php namespace Modules\AiApp\Media;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Media as BaseMedia;
use Spatie\MediaLibrary\UrlGenerator\UrlGeneratorFactory;
use Illuminate\Support\Collection;
use Modules\AiApp\Media\ImageGenerators\Audio;

class Media extends BaseMedia
{
    protected $visible = ['file_name', 'mime_type', 'size', 'human_readable_size', 'url'];
    protected $appends = ['human_readable_size', 'url'];

    //------------------------------------------------------------------------------------------------------------------
    // Attributes

    public function getInfoAttribute()
    {
        return $this->getMediaInfo();
    }

    public function getUrlAttribute()
    {
        return $this->getFullUrl();
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getMediaInfo()
    {
        $type = $this->getCustomProperty('type');

        if (in_array($type, ['video', 'audio'])) {
            $info = $this->getCustomProperty('duration_human');
        } elseif (in_array($type, ['image'])) {
            $info = sprintf('%sx%s', $this->getCustomProperty('width'), $this->getCustomProperty('height'));
        } else {
            $info = sprintf('%s (%s)', $this->file_name, $this->human_readable_size);
        }

        return $info;
    }


    public function getProps()
    {
        return [
            'duration'       => floatval($this->getCustomProperty('duration')),
            'duration_human' => strval($this->getCustomProperty('duration_human')),
            'width'          => intval($this->getCustomProperty('width')),
            'height'         => intval($this->getCustomProperty('height')),
            'type'           => $this->getCustomProperty('type'),
            'info'           => $this->getMediaInfo(),
        ];
    }

    public function getStorage(): FilesystemAdapter
    {
        return Storage::disk($this->disk);
    }

    public function getContent()
    {
        return $this->getStorage()->get($this->getPath());
    }

    public function getFullPath(string $conversionName = ''): string
    {
        $storage = $this->getStorage();

        return $storage->path($this->getPath($conversionName));
    }

    /**
     * Collection of all ImageGenerator drivers.
     */
    public function getImageGenerators(): Collection
    {
        return parent::getImageGenerators()->push(Audio::class);
    }

}