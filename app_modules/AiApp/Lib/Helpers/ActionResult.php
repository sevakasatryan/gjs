<?php namespace Modules\AiApp\Helpers;

use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;

class ActionResult implements \ArrayAccess
{
    const CODE_HZ               = 0;
    const CODE_OK               = 200;
    const CODE_BAD_REQUEST      = 400;
    const CODE_UNAUTHORIZED     = 401;
    const CODE_FORBIDDEN        = 403;
    const CODE_NOT_FOUND        = 404;
    const CODE_NOT_ALLOWED      = 405;
    const CODE_VALIDATION_ERROR = 415;
    const CODE_ERROR            = 500;
    const CODE_DB               = 504;
    const CODE_PROCESSING_ERROR = 520;

    const TYPE_SUCCESS = 'success';
    const TYPE_ERROR   = 'error';

    public $success, $error, $type, $message, $validation, $code, $data;

    public static $messages = [
        0   => 'Undefined Error.',
        200 => 'The operation completed successfully.',
        401 => 'Unauthorized.',
        400 => 'Invalid incoming data.',
        403 => 'You do not have sufficient permissions to perform this action.',
        404 => 'Item not found.',
        405 => 'Method not allowed.',
        415 => 'Validation Error.',
        500 => 'An error occurred while processing the request.',
        504 => 'There was an error while processing a database query.',
        520 => 'Processing Error.',
    ];

    public function __construct()
    {
        $this->code = self::CODE_HZ;
        $this->message = '';
        $this->data = [];
        $this->validation = [];
    }

    public function answer($isSuccess, $message, $code = null)
    {
        if ($isSuccess) {
            $this->success($message, $code);
        } else {
            $this->error($message, $code);
        }

        return $this;
    }

    public function success($message, $code = null)
    {
        return $this->setSuccess($message, $code);
    }

    public function successCode($code = null)
    {
        $message = $this->getMessageByCode($code);

        return $this->setSuccess($message, $code);
    }

    public function forLogs($message = 'LOGS')
    {
        $this->success('/--- ACTION LOGS ---/ '.$message);

        return $this;
    }

    public function allOK()
    {
        return $this->successCode(self::CODE_OK);
    }

    public function errorDB()
    {
        return $this->errorCode(self::CODE_DB);
    }

    public function error404()
    {
        return $this->errorCode(self::CODE_NOT_FOUND);
    }

    public function errorResource404()
    {
        return $this->errorNotAllowed();
    }

    public function errorNotAllowed()
    {
        return $this->errorCode(self::CODE_NOT_ALLOWED);
    }

    public function errorPerms()
    {
        return $this->errorCode(self::CODE_FORBIDDEN);
    }

    public function errorRequest()
    {
        return $this->errorCode(self::CODE_ERROR);
    }

    public function errorIncomingData()
    {
        return $this->errorCode(self::CODE_BAD_REQUEST);
    }

    public function errorUnauthorized()
    {
        return $this->errorCode(self::CODE_UNAUTHORIZED);
    }

    public function errorProcessing()
    {
        return $this->errorCode(self::CODE_PROCESSING_ERROR);
    }

    public function errorValidation($messages = [])
    {
        $this->errorCode(self::CODE_VALIDATION_ERROR);
        $this->setValidation($messages);

        return $this;
    }

    public function error($message, $addValidationMessage = false, $code = null)
    {
        $this->setError($message, $addValidationMessage, $code);

        return $this;
    }

    public function errorCode($code)
    {
        $message = $this->getMessageByCode($code);
        $this->error($message, false, $code);

        return $this;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function errorCodeMessages($data)
    {
        foreach ($data as $key => $value) {
            $key = intval($key);
            self::$messages[$key] = strval($value);
        }
    }

    public function __toString()
    {
        return $this->toString();
    }

    public function getMessageByCode($code)
    {
        return self::$messages[intval($code)] ?? '';
    }

    public function isEmpty()
    {
        return ($this->code == self::CODE_HZ);
    }

    public function messagesToString()
    {
        $arr = [$this->message];
        if ($this->error) {
            foreach ($this->validation as $field => $errors) {
                foreach ($errors as $error) {
                    $arr[] = sprintf('[%s]: %s', $field, $error);
                }
            }
        }

        return join(' | ', $arr);
    }

    /**
     * @param string|array $key
     * @param null $val
     * @return $this
     *
     * setData(['a' => 1, 'b' => 2]);
     * setData('cc', 99);
     */
    public function setData($key, $val = null)
    {
        if (is_array($key)) {
            $this->data = $key;
        } else {
            $this->data[strval($key)] = $val;
        }

        return $this;
    }

    public function hasData($key = null)
    {
        return isset($this->data[$key]);
    }

    public function getData($key = null, $defaultVal = null)
    {
        if (null !== $key) {
            return isset($this->data[$key]) ? $this->data[$key] : $defaultVal;
        } else {
            return $this->data;
        }
    }

    public function setValidation($messages)
    {
        if (is_object($messages) && method_exists($messages, 'toArray')) {
            $this->validation = $messages->toArray();
        } else {
            $this->validation = (array)$messages;
        }

        return $this;
    }

    public function addValidation($message, $field = '')
    {
        $this->validation[$field][] = strval($message);

        return $this;
    }

    public function getValidation()
    {
        return $this->validation;
    }

    public function setPagination(Paginator $pg)
    {
        $pgData = $pg->toArray();
        unset($pgData['data']);
        $this->setData('items', $pg->items());
        $this->setData('pagination', $pgData);
        $this->allOK();

        return $this;
    }

    //------------------------------------------------------------------------------------------------------------------
    // MYLOG:
    protected function mylog($message, $type)
    {
        if (is_array($message)) {
            $message = trim(join(PHP_EOL, $message));
        }

        if (empty($message)) {
            $message = '<EMPTY_MYLOG_MESSAGE>';
            $type = 'error';
        }

        if (empty($this->data['mylog'])) {
            $this->data['mylog'] = [];
        }

        $this->data['mylog'][] = ['type' => $type, 'message' => $message, 'date' => Carbon::now()->toDateTimeString()];
    }

    public function mylogOk($message)
    {
        $this->mylog($message, 'info');
    }

    public function mylogError($message)
    {
        $this->mylog($message, 'error');
    }

    public function mylogMerge($merge)
    {
        if ($merge instanceof ActionResult) {
            $logs = $merge->getData('mylog', []);
        } else {
            $logs = (array)$merge;
        }

        if (empty($this->data['mylog'])) {
            $this->data['mylog'] = [];
        }
        $this->data['mylog'] = array_merge($this->data['mylog'], $logs);
    }

    //------------------------------------------------------------------------------------------------------------------

    protected function setSuccess($message, $code = null)
    {
        if (is_null($code)) {
            $code = self::CODE_OK;
        }
        $this->message = $message;
        $this->success = true;
        $this->error = !$this->success;
        $this->code = $code;
        $this->type = self::TYPE_SUCCESS;

        return $this;
    }

    protected function setError($message, $addValidationMessage, $code = null)
    {
        if ($message instanceof \Exception) {
            $message = $message->getMessage();
        }

        if (is_null($code)) {
            $code = self::CODE_ERROR;
        }

        $this->message = $message;
        $this->success = false;
        $this->error = !$this->success;
        $this->code = $code;
        $this->type = self::TYPE_ERROR;

        if ($addValidationMessage) {
            $this->addValidation($message);
        }

        return $this;
    }

    public function offsetSet($offset, $value)
    {
        if ($offset && property_exists($this, $offset)) {
            $this->{$offset} = $value;
        }
    }

    public function offsetExists($offset)
    {
        return property_exists($this, $offset);
    }

    public function offsetUnset($offset)
    {
    }

    public function offsetGet($offset)
    {
        return property_exists($this, $offset) ? $this->{$offset} : null;
    }

    public function toArray()
    {
        // we need only public properties
        $props = call_user_func('get_object_vars', $this);
        $props = array_arrvals($props);
        unset($props['data']['mylog']);

        return $props;
    }

    public function toString()
    {
        return json_encode($this->toArray());
    }

    public function __sleep()
    {
        return $this->toArray();
    }

    public function merge(ActionResult $result)
    {
        $fields = ['success', 'error', 'type', 'message', 'validation', 'code', 'data'];
        foreach ($fields as $field) {
            $this->{$field} = $result->{$field};
        }
    }

    //******************************************************************

    public static function getInstance()
    {
        return new self();
    }

    public static function createFrom(self $actionResult)
    {
        $new = self::getInstance();
        $new->merge($actionResult);

        return $new;
    }

    public static function fromArray(array $data)
    {
        $new = self::getInstance();
        $new->success = !empty($data['success'] ?? 0);
        $new->error = !empty($data['error'] ?? 0);
        $new->type = $data['type'] ?? 'error';
        $new->message = strval($data['message'] ?? '');
        $new->validation = (array)($data['validation'] ??[]);
        $new->code = intval($data['code'] ?? '');
        $new->data = (array)($data['data'] ?? []);

        return $new;
    }

}