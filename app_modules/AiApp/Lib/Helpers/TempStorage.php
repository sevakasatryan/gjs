<?php namespace Modules\AiApp\Helpers;

use Illuminate\Filesystem\FilesystemAdapter;

class TempStorage
{
    protected $path, $storage, $doNotRemove, $diskName;

    public function __construct($diskName, FilesystemAdapter $storage, $path = '')
    {
        $this->diskName = $diskName;
        $this->path = $path ? trim($path, '/') : time().'_'.md5(uniqid());
        $this->storage = $storage;
        $this->doNotRemove = false;
        //
        $this->storage->makeDirectory($path);
    }

    public function __destruct()
    {
        try {
            if (empty($this->doNotRemove)) {
                $this->remove();
            }
        } catch (\Exception $e) {

        }
    }

    public function doNotRemove()
    {
        $this->doNotRemove = true;
    }

    public function remove()
    {
        $this->storage->deleteDirectory($this->path);
    }

    public function __toString()
    {
        return $this->getFullPath();
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getFilePath($sourcePath = '')
    {
        return rtrim($this->path, '/').'/'.$sourcePath;
    }

    public function getFullPath($sourcePath = '')
    {
        $prefix = $this->getPrefix();
        $fullPath = $prefix.$this->getFilePath($sourcePath);

        return $fullPath;
    }

    public function exists($sourcePath = '')
    {
        $prefix = $this->getPrefix();
        $fullPath = $prefix.rtrim($this->path, '/').'/'.$sourcePath;

        return file_exists($fullPath);
    }

    public function sanitizeName($fn)
    {
        return preg_replace('/[^\w\-_\.]/i', '_', $fn);
    }

    public function put($path, $content, $options = [])
    {
        $storePath = $this->path.'/'.$path;
        $this->storage->put($storePath, $content, $options);

        return $storePath;
    }

    public function getPrefix()
    {
        return $this->storage->getDriver()->getAdapter()->getPathPrefix();
    }

    public function getDisk()
    {
        return $this->diskName;
    }
}
