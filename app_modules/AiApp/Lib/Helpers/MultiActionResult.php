<?php namespace Modules\AiApp\Helpers;

use Illuminate\Support\Collection;

class MultiActionResult extends Collection
{
    public function __get($name)
    {
        $methodName = 'get'.studly_case($name).'Property';
        if (method_exists($this, $methodName)) {
            return $this->{$methodName}();
        }

        return null;
    }

    protected function getSuccessProperty()
    {
        $hasError = $this->first(function (ActionResult $actionResult) {
            return empty($actionResult->success);
        });

        $success = $this->isNotEmpty() && empty($hasError);


        return $success;
    }

    protected function getErrorProperty()
    {
        $error = !($this->getSuccessProperty());

        return $error;
    }

    protected function getTypeProperty()
    {
        $success = $this->getSuccessProperty();
        $type = $success ? ActionResult::TYPE_SUCCESS : ActionResult::TYPE_ERROR;

        return $type;
    }

    protected function getMessageProperty()
    {
        $messages = $this->getMessages()->implode(' ');

        return $messages;
    }

    protected function getMessages()
    {
        $messages = $this->map(function (ActionResult $actionResult) {
            return $actionResult->message;
        })->unique()->values();

        return $messages;
    }

    /**
     * @todo check
     */
    public function getValidation(): array
    {
        $validation = [];
        $this->each(function (ActionResult $actionResult) use (&$validation) {
            $messages = $actionResult->getValidation();
            foreach ($messages as $field => $value) {
                if (!in_array($value, $validation[$field] ?? [])) {
                    $validation[$field][] = $value;
                }
            }
        })->unique()->values()->toArray();

        return $validation;
    }
}