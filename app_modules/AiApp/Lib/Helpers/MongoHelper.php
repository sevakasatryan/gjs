<?php namespace Modules\AiApp\Helpers;

use Illuminate\Support\Collection;
use MongoDB\Driver\Cursor;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\UTCDatetime;
use MongoDB\Model\BSONArray;
use MongoDB\Model\BSONDocument;
use Carbon\Carbon;

abstract class MongoHelper
{
    public static function mongoToArray($mixed)
    {
        /**
         * Convert dates to Carbon
         */
        if ($mixed instanceof UTCDatetime) {
            try {
                $dt = $mixed->toDateTime();
                $mixed = new Carbon($dt->format('Y-m-d H:i:s'), $dt->getTimeZone());
            } catch (\Exception $e) {
            }
        }

        if ($mixed instanceof Cursor || $mixed instanceof BSONArray || $mixed instanceof BSONDocument) {
            $mixed = iterator_to_array($mixed);
        }

        if ($mixed instanceof ObjectID) {
            $mixed = strval($mixed);
        } elseif (is_array($mixed)) {
            foreach ($mixed as $key => &$value) {
                $value = self::mongoToArray($value);
            }
        }

        return $mixed;
    }

    public static function mongoToCollection($mixed): Collection
    {
        $arr = self::mongoToArray($mixed);

        return Collection::make($arr);
    }
}
