<?php namespace Modules\AiApp\Helpers;

/**
 * @see http://jsonapi.org/format/#fetching-pagination
 */
class PaginationParams
{
    const USE_DEFAULT = 'default';
    const USE_OFFSET  = 'offset';
    const USE_PAGE    = 'page';

    public $requestParams, $default, $maxLimit, $use, $offset, $limit, $number, $size;

    public function __construct($requestParams, $default = 20, $maxLimit = 1000)
    {
        $this->requestParams = $requestParams;
        $this->default = $default;
        $this->maxLimit = $maxLimit;
        //
        $this->use = self::USE_DEFAULT;
        $this->offset = 0;
        $this->limit = $default;
        $this->number = 1;
        $this->size = $default;
        //
        $this->update();
    }

    public static function create($offset = 0, $limit = 10)
    {
        return new self(['offset' => $offset, 'limit' => $limit]);
    }

    public function update()
    {
        if (isset($this->requestParams['offset']) || isset($this->requestParams['limit'])) {
            $offset = intval($this->requestParams['offset'] ?? 0);
            $offset = max(0, $offset);
            $limit = intval($this->requestParams['limit'] ?? $this->default);
            $limit = min($limit, $this->maxLimit);
            //
            $this->offset = $offset;
            $this->limit = $limit;
            $this->number = ceil($offset / $limit) + 1;
            $this->size = $limit;
            $this->use = self::USE_OFFSET;
        }

        if (isset($this->requestParams['number']) || isset($this->requestParams['size'])) {
            $number = intval($this->requestParams['number'] ?? 1);
            $number = max(1, $number);
            $size = intval($this->requestParams['size'] ?? $this->default);
            $size = min($size, $this->maxLimit);
            //
            $this->offset = ($number - 1) * $size;
            $this->limit = $size;
            $this->number = $number;
            $this->size = $size;
            $this->use = self::USE_PAGE;
        }

    }
}