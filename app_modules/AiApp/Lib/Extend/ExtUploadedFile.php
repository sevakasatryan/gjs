<?php namespace Modules\AiApp\Extend;

use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\MimeType\FileBinaryMimeTypeGuesser;
use Modules\AiApp\Convert\ConvertService;

class ExtUploadedFile extends UploadedFile
{
    public $preserveOriginal = false;

    public function getMimeType()
    {
        $guesser = MimeTypeGuesser::getInstance();
        $guesser->register(new FileBinaryMimeTypeGuesser());  // run FileBinaryMimeTypeGuesser first
        $mime = $guesser->guess($this->getPathname());

        $clientMime = $this->getClientMimeType();
        $clientExt = $this->getClientOriginalExtension();

        //ddd($mime, $clientMime);

        if ($clientMime == 'audio/x-ms-wma' && $mime == 'video/x-ms-asf' && $clientExt == 'wma') {
            $mime = 'audio/x-ms-wma';
        }

        if ($clientMime == 'audio/vnd.rn-realaudio' && $mime == 'application/vnd.rn-realmedia' && $clientExt == 'ra') {
            $mime = 'audio/vnd.rn-realaudio';
        }

        if ($clientMime == 'audio/x-matroska' && $mime == 'video/x-matroska') {
            $mime = 'audio/x-matroska';
        }

        if ($clientMime == 'audio/webm' && $mime == 'video/webm') {
            $mime = 'audio/webm';
        }

        if (in_array($mime, ['audio/mpeg3', 'audio/mpeg']) ||
            in_array($clientMime, ['audio/mpeg3', 'audio/mpeg', 'audio/mp3'])
        ) {
            // additional check
            $info = $this->getFirstBytes(3);
            if ('ID3' == $info) {
                $mime = 'audio/mp3';
            }
        }

        if ($mime == 'application/octet-stream') {
            if ('caf' == $clientExt || $clientMime == 'audio/x-caf') {
                // additional check
                $info = $this->getFirstBytes(4);
                if ('caff' == $info) {
                    $mime = 'audio/x-caf';
                }
            }

            if ('pdf' == $clientExt) {
                // additional check
                $info = $this->getFirstBytes(3, 1);
                if ('PDF' == $info) {
                    $mime = 'application/pdf';
                }
            }

            if ('amr' == $clientExt) {
                // additional check
                $info = $this->getFirstBytes(3, 2);
                if ('AMR' == $info) {
                    $mime = 'audio/AMR';
                }
            }

        }

        if (in_array($clientExt, ['3ga', '3gp', '3gpp']) || in_array($clientMime, ['audio/3gpp', 'video/3gpp'])) {
            // additional check
            $info = $this->getFirstBytes(6, 4);

            if (in_array($info, ['ftyp3g', 'ftypmp'])) {
                $vcodec = $this->getVideoCodec();
                if (is_null($vcodec) || $vcodec) {
                    $mime = 'video/3gpp';  // not audio/3gpp, @see guesser
                } else {
                    $mime = 'audio/3gpp';
                }
            } else {
                $info = $this->getFirstBytes(3, 2);
                if ('AMR' == $info) {
                    $mime = 'audio/3ga';
                }
            }
        }

        if ($mime == 'audio/x-unknown') {
            if ('voc' == $clientExt) {
                // additional check
                $info = $this->getFirstBytes(19);
                if ('Creative Voice File' == $info) {
                    $mime = 'audio/voc';
                }
            }
        }

        return $mime;
    }

    public function guessExtension()
    {
        $mime = $this->getMimeType();

        if (in_array($mime, ['audio/mp3', 'audio/mpeg', 'audio/mpeg3'])) {
            $ext = 'mp3';
        } elseif (in_array($mime, ['audio/x-hx-aac-adts'])) {
            $ext = 'aac';
        } elseif (in_array($mime, ['audio/vnd.dolby.dd-raw'])) {
            $ext = 'ac3';
        } elseif (in_array($mime, ['audio/AMR'])) {
            $ext = 'amr';
        } elseif (in_array($mime, ['audio/x-m4a'])) {
            $ext = 'm4a';
        } elseif (in_array($mime, ['audio/ogg'])) {
            $ext = 'ogg';
        } elseif (in_array($mime, ['audio/voc'])) {
            $ext = 'voc';
        } elseif (in_array($mime, ['audio/vnd.rn-realaudio'])) {
            $ext = 'ra';
        } else {
            $ext = parent::guessExtension();
        }

        //ddd($mime, $ext);

        return $ext;
    }

    public function hashName($path = null)
    {
        return md5(uniqid($path)).'.'.$this->guessExtension();
    }

    public function getFirstBytes($length, $offset = 0)
    {
        return get_first_bytes($this->path(), $length, $offset);
    }

    public function getVideoCodec()
    {
        $vcodec = null;
        if (class_exists(ConvertService::class)) {
            $vcodec = (string)ConvertService::getVideoCodec($this->getPathname());
        }

        return $vcodec;
    }

    public function detectFileType()
    {
        $mimeType = $this->getMimeType();

        $parts = explode('/', $mimeType);
        $code = $parts[0] ?? '';

        if ($code == 'application') {
            //application/vnd.adobe.flash.movie
            if (preg_match('/movie|shockwave-flash/', $mimeType)) {
                $code = 'video';
            }
        }

        return $code;
    }

    public function isFileType($type)
    {
        $fileType = $this->detectFileType();

        return ($type === $fileType);
    }
}