/**
 *
 * Notifications
 *
 */
window.notifications = {
    toast: {
        text: '',
        type: 'info',
        heading: false,
        showHideTransition: 'slide',       // It can be plain, fade or slide
        bgColor: null,                     // Background color for toast
        textColor: '#fff',                 // text color
        allowToastClose: true,             // Show the close button or not
        hideAfter: 5000,                   // `false` to make it sticky or time in miliseconds to hide after
        stack: 5,                          // `fakse` to show one stack at a time count showing the number of toasts that can be shown at once
        textAlign: 'left',                 // Alignment of text i.e. left, right, center
        position: 'top-right',             // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values to position the toast on page
    }
};

window.showOnlyErrorNotification = function (message, type) {
    if (type === 'success') {
        cons(message, 'NOTIFICATION [' + type + ']');
    } else {
        showNotification(message, type);
    }
};

window.showActionResultMessages = function (actionResult, showSuccess) {
    if (actionResult.error || showSuccess) {
        showNotification(actionResult.message, actionResult.type);
    }
    if (actionResult.error) {
        showValidationMessages(actionResult.validation)
    }
};

window.showNotification = function (message, type) {
    if (typeof type === 'undefined') {
        type = 'info';
    }
    var headings = {
        info: 'Information',
        success: 'Success',
        error: 'Error',
        warning: 'Warning!'
    };

    var conf = $.extend({}, window.notifications.toast, {
        text: message,
        heading: false, //headings[type],
        icon: type
    });

    return $.toast(conf);
};

window.showValidationMessages = function (resultValidation) {
    var messages = [];
    $.each(resultValidation, function (i, el) {
        $.each(el, function (k, elmsg) {
            if (elmsg) {
                messages.push(elmsg);
            }
        });
    });
    if (messages.length > 0) {
        showNotification(messages.join(', '), 'error');
    }
};