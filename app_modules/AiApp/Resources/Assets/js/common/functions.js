window.QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

window.cons = function (subj, title) {
    if (appConfig.js_debug) {
        if (typeof title == 'undefined') {
            title = 'DBG';
        } else {
            title = title.toUpperCase();
        }

        if ($.isArray(subj)) {
            console.log(title + ' (array) ⤵');
            console.log(subj);
        } else if (typeof subj === 'object') {
            console.log(title + ' (object) ⤵');
            console.log(subj);
        } else {
            console.log(title + ' (string): ' + subj);
        }
    }
};

window.secondsToTimeString = function (seconds, format) {
    format = format || "HH:MM:SS.XXX";

    var pad = function pad(n, width) {
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(0) + n;
    };

    var h = Math.floor(seconds / 3600);
    var m = Math.floor((seconds * 1000 / (1000 * 60)) % 60);
    var mmm = (h * 60 + m);
    var s = Math.floor(seconds % 60);
    var ms = Math.floor((seconds * 1000) % 1000);

    format = format.replace(/HH/, pad(h, 2));
    format = format.replace(/MMM/, pad(mmm, 3));
    format = format.replace(/MM/, pad(m, 2));
    format = format.replace(/SS/, pad(s, 2));
    format = format.replace(/XXX/, pad(ms.toString().slice(0, 3), 3));
    format = format.replace(/XX/, pad(ms.toString().slice(0, 2), 2));
    format = format.replace(/X/, pad(ms.toString().slice(0, 1), 1));

    return format;
};

window.removeFromArray = function (arr, what) {
    for (var i = arr.length - 1; i >= 0; i--) {
        if (arr[i] === what) {
            arr.splice(i, 1);
        }
    }
};

window.clearjQueryCache = function () {
    for (var x in jQuery.cache) {
        delete jQuery.cache[x];
    }
};

window.sleep = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

window.ucfirst = function (str) {
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1, str.length - 1);
};

window.imagePath = function (im) {
    return window.appConfig.images_path + '/' + im;
};