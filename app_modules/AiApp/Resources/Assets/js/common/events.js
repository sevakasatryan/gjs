(function (w) {
    /**
     * Base Event:
     */
    w.baseEvent = {
        type: 'base',
        params: {},
        getParam: function (key) {
            if (typeof(this.params[key]) != 'undefined') {
                return this.params[key];
            }
            return '';
        }
    };

    //------------------------------------------------------------------------------------------------------------------

    w.events = {
        myEventComponentReady: $.extend({}, baseEvent, {type: 'component_ready'}),
        myEventPageResized: $.extend({}, baseEvent, {type: 'page_resized'}),
        myEventTotalScroll: $.extend({}, baseEvent, {type: 'total_scroll'}),
        myEventAjaxPageLoaded: $.extend({}, baseEvent, {type: 'ajax_page_loaded'}),
    };

    //------------------------------------------------------------------------------------------------------------------

    $.fn.mytrigger = function (event) {
        cons(event, 'MyTrigger');
        return this.trigger(event);
    };

    (function () {
        var pageResizeTimeoutId = 0;
        $(w).resize(function () {
            clearTimeout(pageResizeTimeoutId);
            pageResizeTimeoutId = setTimeout(function () {
                $(document).mytrigger(w.events.myEventPageResized);
            }, 100);
        });
    })();

})(window);