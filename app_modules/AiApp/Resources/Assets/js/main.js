//----------------------------------------------------------------------------------------------------------------------
window.$ = window.jQuery = require('jquery');

import './../vendor/jquery-toast-plugin/dist/jquery.toast.min';
import './../vendor/spinner/jquery.babypaunch.spinner';
import './../vendor/spinner/spinner';
import './common/functions';
import './common/events';
import './common/notifications';
//----------------------------------------------------------------------------------------------------------------------