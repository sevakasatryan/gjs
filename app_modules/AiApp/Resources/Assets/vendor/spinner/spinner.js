$(document).ready(function () {
    $("#spin").spinner();

    window.showLoading = function () {
        $("#spin").show();
    };

    window.hideLoading = function () {
        $("#spin").hide();
    };
});
