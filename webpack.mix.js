let mix = require('laravel-mix');
const Clean = require('clean-webpack-plugin');
const webpack = require('webpack');

function isExternal(module) {
    // This prevents stylesheet resources with the .css or .scss extension
    // from being moved from their original chunk to the vendor chunk
    if (module.resource && (/^.*\.(css|scss)$/).test(module.resource)) {
        return false;
    }
    return module.context && (
            module.context.indexOf("node_modules") !== -1 ||
            module.context.indexOf("bootstrap") !== -1 ||
            module.context.indexOf("vendor") !== -1 ||
            module.context.indexOf("jquery") !== -1
        );
}

let outputFolder = 'public/build';

mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            moment: "moment"
        }),
        new Clean([
            outputFolder + '/*.js',
            outputFolder + '/*.css'
        ], {verbose: true, watch: false}),
        //
        new webpack.optimize.CommonsChunkPlugin({
            name: '/build/vendor',
            minChunks: function (module) {
                return isExternal(module);
            },
            chunks: ["/build/app"]
        }),
    ],
});

mix.options(
    {
        processCssUrls: true,
        sourcemaps: false,
        fileLoaderDirs: {
            fonts: 'build/fonts',
            images: 'build/images',
        },
    })
//    .setPublicPath(outputFolder)
    .js('resources/assets/js/app.js', outputFolder)
    .sass('resources/assets/sass/app.scss', outputFolder)
    .version();
