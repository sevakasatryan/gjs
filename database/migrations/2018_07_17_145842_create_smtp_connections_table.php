<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmtpConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smtp_connections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('host');
            $table->string('port');
            $table->string('username');
            $table->string('password');
            $table->string('from_name');
            $table->string('from_email');
            $table->string('reply_to_email');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smtp_connections');
    }
}
