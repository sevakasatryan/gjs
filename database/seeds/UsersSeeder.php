<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = DB::table('users');
        $tbl->truncate();
        $tbl->insert([
            [
                'role'       => 'user',
                'first_name' => 'John',
                'last_name'  => 'Doe',
                'email'      => 'test@gmail.com',
                'password'   => bcrypt('qwklbjh3i5buSd'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'role'       => 'user',
                'first_name' => 'Admin',
                'last_name'  => 'Dev',
                'email'      => 'tmp3tmp@gmail.com',
                'password'   => bcrypt('jb4rb3pHKGB048g324j'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
