## GoJeo Send Application

### Build JS/CSS (LOCAL DEV)

1. `npm install`
1. `npm run dev|watch|prod`

### Setup

1. clone repository
1. `php composer.phar install`
1. `php artisan module:optimize`
1. `php composer.phar update --lock`
1. `php artisan migrate:fresh --seed`
1. setup CRON `crontab -e`:

    `* * * * * php /var/www/SITE.COM/artisan schedule:run >> /dev/null 2>&1`
1. command to update sources from REPO: `./bin/sh/update.php [dev|prod]`